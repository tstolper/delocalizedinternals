
# -*- coding: utf-8 -*-

from itertools import izip
import logging
import time
import scipy as np
import GeomOpt.optimize as opt
import GeomOpt.Structure as mol
import GeomOpt.Read as Read
import Interface.Molpro as molpro
from Interface.Orca import OrcaRun
import Interface.Clusters as cluster
import Interface.Numerical as numer
import Analysis.frequency as freq
import Potentials as pots


class CMD_Log(object):

    def __init__(self, options = None):
        assert options != None

        self.opts       = options['LOG'].key_vals

    def run(self):
        logging.addLevelName(1, 'VERYVERBOSE')
        setattr(logging, 'VERYVERBOSE', 1)
        if self.opts['debug']:
            logging.basicConfig(filename = self.opts['main_inp'] + '.debug', filemode = 'w', format = '%(message)s', level = logging.VERYVERBOSE)
            formatter   = logging.Formatter('%(message)s')
            mainhandler = logging.FileHandler(self.opts['main_inp'] + '.log')
            mainhandler.setLevel(logging.DEBUG)
            mainhandler.setFormatter(formatter)
            logging.getLogger().addHandler(mainhandler)
            debughandler = logging.FileHandler(self.opts['main_inp'] + '.out')
            debughandler.setLevel(logging.INFO)
            debughandler.setFormatter(formatter)
            logging.getLogger().addHandler(debughandler)
        else:
            logging.basicConfig(filename = self.opts['main_inp'] + '.out', filemode = 'w', format = '%(message)s', level = logging.INFO)

class CMD_Read(object):

    def __init__(self, inline = None, options = None, **kwargs):
        assert inline != None
        assert options != None

        it              = iter(inline)
        self.iopts      = dict(izip(it, it))

    def run(self):
        if 'type' in self.iopts:
            if self.iopts['type'].upper() == 'GRAD':
                if 'folder' not in self.iopts:
                    raise KeyError('*** ERROR: Please supply a folder.')
                # assume Molpro as program right now.
                qmprog = molpro.MolproRun(norun = True)
                Read.read_numer_EnDiff(self.iopts['folder'], qmprog, order = 'GRAD')
            elif self.iopts['type'].upper() == 'HESS':
                if 'folder' not in self.iopts:
                    raise KeyError('*** ERROR: Please supply a folder.')
                qmprog = molpro.MolproRun(norun = True)
                Read.read_numer_EnDiff(self.iopts['folder'], qmprog, order = 'HESS')
        else:
            logging.error('*** ERROR: You need to supply a "type" to read. Valid options: GRAD')


class CMD_Pot(object):
    def __init__(self, inline = None, options = None, **kwargs):
        assert inline is not None
        # assert options is not None

        it              = iter(inline)
        self.iopts      = dict(izip(it, it))

    def run(self, inline=None):
        inlopts = self.iopts
        retpot  = None
        if inline is not None: inlopts = inline
        if 'type' in inlopts:
            pottyp = inlopts['type'].upper()
            if pottyp == 'HARMONIC2DIFF':
                for opt in ('k', 'r0', 'uatoms', 'vatoms'):
                    if opt not in inlopts:
                        raise ValueError('ERROR: Please supply k, r0, uatoms and vatoms for harmonic potentials.')
                k, r0 = float(inlopts['k']), float(inlopts['r0'])
                uatoms= [int(u.strip('"\'')) for u in inlopts['uatoms'].split(',')]
                vatoms= [int(v.strip('"\'')) for v in inlopts['vatoms'].split(',')]
                if pots.optimization is None:
                    pots.optimization = []
                retpot  = pots.Harmonic2Diff(k=k, r0=r0, uatoms=uatoms, vatoms=vatoms)
                pots.optimization.append(retpot)
        else:
            raise ValueError('ERROR: You need to supply the type of the potential.')
        return retpot


class CMD_Opt(object):

    def __init__(self, inline = None, options = None, **kwargs):
        assert options != None
        assert inline != None

        it              = iter(inline)
        self.iopts      = dict(izip(it, it))
        self.opts       = options['OPT'].key_vals
        self.start_mol  = None
        self.inpname    = options['LOG'].key_vals['main_inp']
        self.read_grad  = False
        self.read_hess  = False
        if 'mol' in self.iopts:
            self.start_mol  = self.iopts['mol']
        if 'grad_start_id' in self.iopts:
            self.read_grad = True
        if 'hess_start_id' in self.iopts:
            self.read_hess = True

    def get_calc_Energy(self, molec = None, prog = None):
        assert molec != None
        assert prog != None

        energy  = 1.0
        wtime   = self.opts['en_wtime']
        en_str  = self.opts['en_match']
        # TODO Check for existing file, compare and rename if necessary!
        prog.run_calc(molec.get_String(units = 'ANG'))
        if self.opts['on_cluster']:
            while energy >= 0.0:
                time.sleep(wtime)
                try:
                    energy = prog.get_Energy(match_str = en_str)
                except ValueError:
                    logging.debug('No energy for calculation {0} yet!'.format(prog.work_dir + '/' + prog.file_basename + '.inp'))
        else:
            energy = prog.get_Energy(match_str = en_str)
        return energy

    def get_numer_Grad(self, molec = None, prog = None):
        assert molec != None
        assert prog != None

        wtime       = self.opts['grad_wtime']
        en_str      = self.opts['en_match']
        gbatch      = self.opts['cluster_batch']
        read_id     = None
        if self.read_grad:
            read_id         = int(self.iopts['grad_start_id'])
            self.read_grad  = False # only do this at the beginning
        if self.opts['on_cluster']:
            clustr = cluster.GWDG(job = prog, batch = gbatch)
        else:
            clustr = None
        return numer.calc_numer_Grad(prog, clustr, molec, wtime = wtime, match_str = en_str, read = read_id)

    def get_calc_Hessian(self, prog = None):
        assert prog is not None
        lowertriag_mat = prog.get_Hessian().toarray()
        symm_hess = lowertriag_mat + lowertriag_mat.transpose() - np.diag(np.diag(lowertriag_mat))
        return symm_hess

    def run(self, struct_file=None):
        # first get all the options
        en_tl       = self.opts['en_template']
        grad_tl     = self.opts['grad_template']
        en_opts     = self.opts['en_calcopts']
        grad_opts   = self.opts['grad_calcopts']
        maxit       = self.opts['maxit']
        coords      = self.opts['coords']
        method      = self.opts['method']
        hess        = self.opts['hessian']
        update      = self.opts['update']
        upd_steps   = self.opts['update_steps']
        exact_steps = self.opts['exact_steps']
        executable  = self.opts['exe']
        name        = self.opts['basename']
        suite       = self.opts['outtype'].upper()
        # then prepare the run
        start_mol   = mol.Molecule()
        if struct_file is not None:
            start_mol.load_File(struct_file)
        else:
            start_mol.load_File(self.start_mol)
        if suite == 'MOLPRO':
            qmsuite   = molpro.MolproRun
        elif suite == 'ORCA':
            qmsuite   = OrcaRun
        else:
            raise NotImplementedError('No suite of that name yet.')
        en_qmprog   = qmsuite(name, en_tl, options = en_opts, prog = executable)
        # and the needed callbacks
        get_en      = lambda m: self.get_calc_Energy(m, en_qmprog)
        if self.opts['numer_grad']:
            grad_qmprog = qmsuite(name, grad_tl, options = grad_opts, prog = executable)
            get_grad    = lambda m: self.get_numer_Grad(m, grad_qmprog)
        else:
            get_grad    = lambda m: en_qmprog.get_Gradient(match_str = self.opts['grad_match']).flatten()
        if hess.upper() != 'EXACT':
            get_hess    = None
        else:
            get_hess    = lambda cstep, coords: (self.get_calc_Hessian(en_qmprog), False)
        # last but not least run it
        o = opt.Optimizer(molfile = start_mol, method = method, hess = hess, hessupd = update, coordsys = coords,
                          getEner = get_en, getGrad = get_grad, getHess = get_hess, update_steps = upd_steps, exact_steps = exact_steps,
                          opt_options=self.opts)
        self.opts['opt_object']   = o
        steps, is_conv, final_mol = o.minimize(maxsteps = maxit)
        self.opts['opt_isconv'] = is_conv
        if self.opts['write_final_xyz']:
            final_mol.write_Struct(outfile = self.inpname + '.xyz', wtype = 'XYZ', units = 'ANG')
        if self.opts['write_statistics']:
            import scipy as np
            with open('optimization_statistics.dat', 'w') as statfile:
                statfile.write('{:8s} {:20s} {:20s} {:20s}\n'.format('Step','Energy/Eh','Max Grad','Grad Norm'))
                for i,s in enumerate(o.optstep_hist):
                    statfile.write('{:8d} {:20.10f} {:20.10f} {:20.10f}\n'.format(i,s.energy_real,np.amax(s.grad_real),np.std(s.grad_real)))

class CMD_PotRSS(CMD_Opt):

    def __init__(self, *args, **kwargs):
        super(CMD_PotRSS, self).__init__(*args, **kwargs)
        self.read_grad  = False
        self.read_hess  = False

    def run(self):
        import os, os.path, shutil
        inlopts     = self.iopts
        opt_opts    = self.opts
        rss_start   = float(inlopts.pop('rss_start', None).strip('"'))
        rss_end     = float(inlopts.pop('rss_end', None).strip('"'))
        rss_steps   = int(inlopts.pop('rss_steps', None).strip('"'))
        inlopts.update({'r0': rss_start})
        potential   = CMD_Pot(inline=(1,2)).run(inline=inlopts)
        cwd         = os.getcwd()
        start_mol   = self.start_mol
        if start_mol[0] != '/':
            start_mol = os.path.join(cwd, start_mol)
        opt_opts['en_template'] = os.path.join(cwd, opt_opts['en_template'])
        if opt_opts['grad_template'] is not None:
            opt_opts['grad_template'] = os.path.join(cwd, opt_opts['grad_template'])

        with open('rss_results.dat', 'w') as resfile:
            resfile.write('#{:5} {:5} {:12} {:12} {:12} {:12} {:5} {:5}\n'.format('r0', 'r', 'Epot', 'max(Gpot)', 'Ereal', 'max(Greal)', 'Steps', 'Conv'))
            for pot_r0 in np.linspace(rss_start, rss_end, num=rss_steps+1, endpoint=True):
                cdir = os.path.join(cwd, 'rss_step_{:.3f}'.format(pot_r0))
                if os.path.isdir(cdir):
                    raise ValueError('ERROR: Path exists already. Please clean.')
                else:
                    os.makedirs(cdir, 0755)
                    os.chdir(cdir)
                potential.settings['r0'] = pot_r0
                super(CMD_PotRSS, self).run(struct_file=start_mol)
                start_mol = os.path.join(cdir, self.inpname+'.xyz')
                os.chdir(cwd)
                # Assembling some results
                last_step   = self.opts['opt_object'].optstep_hist[-1]
                last_ccart  = last_step.struct.get_flat_Array(units='BOHR')
                _R, _dV, _dG= potential.get_All(flatcoords=last_ccart)
                resfile.write(' {:5.2f} {:5.2f}'.format(pot_r0, _R))
                resfile.write(' {:12.8f} {:12.8f} {:12.8f} {:12.8f}'.format(last_step.energy, np.absolute(last_step.grad_cart).max(),
                                                                            last_step.energy_real, np.absolute(last_step.grad_real).max()))
                resfile.write(' {:5d} {!r:5}\n'.format(len(self.opts['opt_object'].optstep_hist)-1, self.opts['opt_isconv']))
                resfile.flush()


class CMD_Freq(CMD_Opt):

    def __init__(self, inline = None, options = None):
        super(CMD_Freq, self).__init__(inline, options)
        self.freqopts   = options['FREQ'].key_vals

    def run(self):
        logging.info('************** Calculating Frequencies ***************')
        step        = self.freqopts['hess_stepsize']
        en_tl       = self.opts['en_template']
        en_opts     = self.opts['en_calcopts']
        grad_tl     = self.opts['grad_template']
        grad_opts   = self.opts['grad_calcopts']
        executable  = self.opts['exe']
        name        = self.opts['basename']
        clustr      = None
        read_id     = None
        jobbatch    = self.opts['cluster_batch']
        # first get the reference right
        en_qmprog   = molpro.MolproRun(name, en_tl, options = en_opts, prog = executable)
        grad_qmprog = molpro.MolproRun(name, grad_tl, options = grad_opts, prog = executable)
        if 'opt_object' in self.opts:
            refmol  = self.opts['opt_object'].optstep_hist[-1].struct
            refen   = self.opts['opt_object'].optstep_hist[-1].energy
        else:
            refmol  = mol.Molecule()
            refmol.load_File(self.start_mol)
            refen   = self.get_calc_Energy(refmol, en_qmprog)
        if self.opts['on_cluster']:
            clustr  = cluster.GWDG(job = grad_qmprog, batch = jobbatch)
        if self.read_hess:
            read_id         = int(self.iopts['hess_start_id'])
            self.read_hess  = False
        hess = numer.calc_Hess_cart(qmsuite = grad_qmprog, mngr = clustr, mol = refmol, ref_energy = refen, wtime = self.freqopts['hess_wtime'], displ = step, read = read_id)
        frq  = freq.Frequency(molecule = refmol)
        frq.load_cart_Hess(hess)
        frq.get_Frequencies()
        if self.freqopts['log_modes']:
            frq.log_Normalmodes()
        if self.freqopts['write_vibs']:
            frq.write_Vibrations(basename = self.inpname)


class CMD_Parse(object):

    def __init__(self, options = None):
        assert options is not None

        self.opt_list = options
        self.cmd_list = [CMD_Log(options)]

    def append_cmd(self, cmdstr = None, optlist = None):
        assert cmdstr is not None
        assert optlist is not None

        CMD = cmdstr.upper()
        if cmdstr.upper() == 'OPT':
            newcmd = CMD_Opt(inline = optlist, options = self.opt_list)
        elif cmdstr.upper() == 'FREQ':
            newcmd = CMD_Freq(inline = optlist, options = self.opt_list)
        elif cmdstr.upper() == 'READ':
            newcmd = CMD_Read(inline = optlist, options = self.opt_list)
        elif CMD == 'POT':
            newcmd = CMD_Pot(inline = optlist, options = self.opt_list)
        elif CMD == 'POTRSS':
            newcmd = CMD_PotRSS(inline = optlist, options = self.opt_list)
        else:
            raise NotImplementedError('Command {0} not known!'.format(cmdstr))
        self.cmd_list.append(newcmd)

    def run(self):
        for cmd in self.cmd_list:
            cmd.run()

