
# -*- coding: utf-8 -*-

import logging

conv2int    = ('en_wtime', 'grad_wtime', 'grad_skip','maxit', 'update_steps', 'exact_steps', 'hess_wtime', 'cluster_batch', 'reset_coords_steps')
conv2float  = ('hess_stepsize', 'smax')
conv2bool   = ('debug', 'extrared', 'numer_grad', 'on_cluster', 'write_final_xyz', 'log_modes', 'dobakken',
               'trust_update', 'reset_coords', 'write_vibs', 'write_trj', 'write_statistics', 'total_connection')

def convval(opt, val):
  retval = None
  if opt in conv2int:
    retval = int(val)
  elif opt in conv2float:
    retval = float(val)
  elif opt in conv2bool:
    retval = bool(val.upper() == 'TRUE')
  else: # assume string should be kept
    retval = val
  return retval

class Opt(object):

    def __init__(self, **kwargs):
        self.key_vals = {'en_match':        '!RHF STATE 1.1 Energy',
                         'en_wtime':        180,
                         'en_template':     None,
                         'en_calcopts':     None,
                         'grad_match':      'Atom dE/dx dE/dy dE/dz',
                         'grad_wtime':      3600,
                         'grad_skip':       1,
                         'grad_template':   None,
                         'grad_calcopts':   None,
                         'basename':        'bubbeldidu',
                         'outtype':         'Molpro', # TODO: Automatic conversion to object
                         'exe':             'molpro',
                         'maxit':           100,
                         'coords':          'REDU',
                         'total_connection': False,
                         'linbends':        'CART',
                         'dobakken':        False,
                         'method':          'RF',
                         'hessian':         'LINDH',
                         'update':          'BFGS',
                         'update_steps':    10,
                         'trust_update':    False,
                         'exact_steps':     0,
                         'numer_grad':      False,
                         'on_cluster':      False,
                         'smax':            0.3,
                         'extrared':        False,
                         'cluster_batch':   0,
                         'reset_coords':    False,
                         'reset_coords_steps':  5,
                         'write_final_xyz': False,
                         'write_trj': True,
                         'write_statistics': True
                        }

    def parse_kvs(self, optlist = None):
        assert optlist != None
        for i in range(0, len(optlist), 2):
            key = optlist[i].lower()
            val = optlist[i+1].strip('"\'')
            if key in self.key_vals:
                self.key_vals[key] = convval(key, val)
            else:
                raise NotImplementedError('Key {0} not implemented yet.'.format(key))

class Freq(Opt):

  def __init__(self, **kwargs):
    self.key_vals = {'hess_stepsize':         0.01,
                     'hess_wtime':            180,
                     'log_modes':           False,
                     'write_vibs':          False}


class Log(Opt):

  def __init__(self, **kwargs):
      self.key_vals = {'main_inp':    kwargs['cfile'],
                       'level':       logging.INFO,
                       'debug':       False
                      }
