
# -*- encoding: utf-8 -*-

############################
#
# Program control for PyOpt
#
# Syntax similar to Orca.
#
############################

from shlex import shlex
import inspect, sys
import Commands as pocmds
import Options as poopts

exec_list           = None
list_of_options     = {}

def parse_Control(cfile = None):
    assert cfile != None

    # initialize the options with default values
    global list_of_options
    for name, optcls in inspect.getmembers(sys.modules['PyOpt.Control.Options'], inspect.isclass):
        list_of_options[name.upper()] = optcls(cfile = cfile)
    exec_list       = Commands.CMD_Parse(options = list_of_options)
    with open(cfile, 'r') as inp:
        itext = inp.read()
        lexer = shlex(itext)
        lexer.wordchars += '.'
        is_block    = False
        block_type  = None
        block_kvs   = None
        is_cmd      = False
        cmd_type    = None
        cmd_kvs     = None
        for token in lexer:
            if token == '%': # create a block/option
                is_block = True
                block_type  = next(lexer)
                block_kvs   = []
            elif token == '!':
                    is_cmd      = True
                    cmd_type    = next(lexer)
                    cmd_kvs     = []
            elif token == 'END':
                if not (is_block or is_cmd):
                    raise ValueError('END directive without starting block?')
                elif is_block:
                    is_block = False
                    if block_type in list_of_options:
                        list_of_options[block_type].parse_kvs(block_kvs)
                    else:
                        raise ValueError('The block "{0}" does not exist!'.format(block_type))
                    block_type  = None
                    block_kvs   = None
                elif is_cmd:
                    exec_list.append_cmd(cmdstr = cmd_type, optlist = cmd_kvs)
                    is_cmd      = False
                    cmd_type    = None
                    cmd_kvs     = None
            else:
                if is_block and token not in '%:=!':
                    block_kvs.append(token)
                elif is_block and token == '!':
                    raise ValueError('No Commands in Blocks please!')
                elif is_cmd and token not in '%:=!':
                    cmd_kvs.append(token.strip('"'))
                elif token not in '%:=!':
                    raise ValueError('There is no such thing as {0}!'.format(token))
        # start the program
        exec_list.run()
