#!/usr/bin/python
# --* encoding: utf-8 *--

import argparse
import os
import scipy as np
import copy
import time

from GeomOpt import Structure, coordinates, optimize, intcoord_funcs
from Interface import Molpro, Numerical
from Analysis.frequency import Frequency

# def transform_back_Hess(bmat = None, kmat = None, hess_int = None):
#     assert bmat != None
#     assert kmat != None
#     assert hess_int != None

#     converged   = False
#     cartHess_new    = 
#         last_rms    = None
#         curr_rms    = None

#         prim_target = prim_old + prim_step
#         ddeloc_new  = np.dot(umat.transpose(), prim_step)
#         deloc_guess = deloc_old + ddeloc_new
#         deloc_new  += ddeloc_new
#         prim_guess  = np.dot(umat, deloc_guess)
#         dprim_new   = prim_target - prim_guess
#         for i in range(max_iter):
#             last_rms    = np.sqrt(np.mean(np.power(ddeloc_new, 2)))
#             ddeloc_new  = np.dot(umat.transpose(), dprim_new)
#             curr_rms    = np.sqrt(np.mean(np.power(ddeloc_new, 2)))
#             deloc_new  += ddeloc_new
#             prim_new    = np.dot(umat, deloc_new)
#             dprim_new   = prim_target - prim_new
#             print 'Umat back: lastrms={0}, currrms={1}, it={2}'.format(last_rms, curr_rms, i)
#             if curr_rms < 1.0e-6 or np.absolute(curr_rms - last_rms) < 1.0e-12:
#                 converged = True
#                 deloc_final = deloc_new
#                 prim_final  = prim_new
#                 break
#         else:
#             if not allow_noconv:
#                 raise Exception('No convergence in {0} steps of the back-transformation.'.format(max_iter))
#             #else:
#                 #print 'Warning: No convergence in {0} steps of the back-transformation.'.format(max_iter)
#             deloc_final = deloc_guess
#             prim_final  = prim_guess

def en_callback(prog, mol, **kwargs): # TODO: adapt to cluster.
    energy = 1.0
    prog.run_calc(mol.get_String(units = 'ANG'))
    try:
        energy = prog.get_Energy(**kwargs)
        if energy >= 0.0:
            raise ValueError
    except ValueError:
        print 'No energy yet!'
    return energy

def get_deloc_Hess(calc_name = None, mol_path = None, en_templ = None, hess_templ = None, cpus = '1', exe = 'molpro', **kwargs):
    assert calc_name != None
    assert mol_path != None

    en_match    = kwargs['en_match_str']

    start_mol = Structure.Molecule()
    start_mol.load_File(mol_path)
    start_cart = start_mol.get_flat_Array(units = 'BOHR')

    en_qmprog      = Molpro.MolproRun(calc_name, en_templ, options = '--no-xml-output -n '+cpus, prog = exe)
    hess_qmprog    = Molpro.MolproRun(calc_name, hess_templ, options = '--no-xml-output -n '+cpus, prog = exe)
    get_en      = lambda mol: en_callback(en_qmprog, mol, match_str = en_match)
    get_hess    = lambda coords, mol, refen: Numerical.calc_Hess_deloc(coords, hess_qmprog, mngr = None, mol = mol, ref_energy = refen,
                                                                       wtime = 180, rdispl = 0.005, match_str = en_match)
    get_hess_cart = lambda coords, mol, refen: Numerical.calc_Hess_cart(coords, hess_qmprog, mngr = None, mol = mol, ref_energy = refen,
                                                                       wtime = 180, rdispl = 0.01, match_str = en_match)

    coords = coordinates.DelocalizedInternals(optref = None, dotest = True, startcart = start_cart, atomlabels = start_mol.get_Elem_List(), getextrared = False)
    # reference energy
    ref_en      = get_en(start_mol)
    deloc_hess  = get_hess(coords, start_mol, ref_en)
    return
    # # following could be nicer, I know
    # opt         = optimize.Optimizer(molfile = start_mol, getEner = get_en)
    # gcart,gdelo = opt.get_NumGrad(coords_cart = start_cart)
    # tmpbmat     = intcoord_funcs.calcBmat(start_cart, coords.coords)
    # kmat_deloc  = intcoord_funcs._kmat_numderiv(flatcoords = start_cart, coords = coords.coords, int_grad = gdelo, umat = coords.Umat)
    # cart_hess   = np.dot(coords.Bmat.transpose(), np.dot(deloc_hess, coords.Bmat)) + kmat_deloc
    # freqs = Frequency(molecule = start_mol)
    # freqs.load_cart_Hess(cart_hess)
    # freqs.print_Frequencies()
    # print 'From cartesian:'
    # chess = get_hess_cart(coords, start_mol, ref_en)
    # freqs.load_cart_Hess(chess)
    # freqs.print_Frequencies()
    # return deloc_hess, cart_hess

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'A very simple script to make use of an also simple interface to use Kumo\'s python optimizer with Molpro.')
    parser.add_argument('--enstr', dest = 'enstr', default = '!RHF STATE 1.1 Energy')
    parser.add_argument('-n', '--nprocs', dest = 'nprocs', default = '1')
    parser.add_argument('en_template', help = 'Template file. This is a molpro input with the structure substituted by {structure} and als curly braces written twice.')
    parser.add_argument('hess_template', help = 'Template file. As for the energy, but needs different save or restart statements to be on the save side.')
    parser.add_argument('name', help = 'Basename of the actual written molpro input/ouput. This also determines the "working directory".')
    parser.add_argument('structure', help = 'Path to the starting structure.')
    args = parser.parse_args()

    print '-------------- Numerical Hessian Calculation Test (locally) --------------'
    tmphess_deloc, tmphess_cart = get_deloc_Hess(args.name, args.structure, args.en_template, args.hess_template, args.nprocs, en_match_str = args.enstr)
    print 'Numerical Hessian in delocalized internal coordinates:'
    for i in range(tmphess_deloc.shape[0]):
        pstr = ''
        for j in range(5):
            pstr += '{0:15.8f}'.format(tmphess_deloc[i,j])
        print pstr
    # # and transform back to cartesians. Try without the K matrix for now.
    print 'Numerical Hessian in cartesian coordinates:'
    for i in range(tmphess_cart.shape[0]):
        pstr = ''
        for j in range(5):
            pstr += '{0:15.8f}'.format(tmphess_cart[i,j])
        print pstr