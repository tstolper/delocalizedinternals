
# --* coding: utf-8 *--

from __future__ import print_function
import unittest
import subprocess
import os
import shutil
import sys
import inspect
import scipy as np

from do_molpro_pyopt import runOpt

class TestSet(object):
    basedir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    maindir = os.path.join(basedir, 'molprocomp')
    sdir    = os.path.join(maindir, 'testset/')

    def setUp(self):
        #To set things up, we need to get a list of structures,
        #the working directories, etc.
        self.structfiles    = [ struct for struct in os.listdir(self.sdir) if (struct[-4:] == '.xyz' and os.path.isfile(self.sdir+struct))]
        #Also load the reference results
        self.molpro_ref    = {} #dictionaries are awesome. :)

        with open(os.path.join(self.maindir, 'molpro_ref.dat'), 'r') as reffile:
            for line in reffile:
                lcols = line.split()
                if lcols[0][0] != '#' and len(lcols) >= 3:
                    self.molpro_ref[lcols[0]+'.xyz'] = (int(lcols[1]), float(lcols[2]))
            reffile.close()
        self.num_calcs = len(self.molpro_ref)
        if self.num_calcs < 1:
            print('ERROR: Didn\'t load any Molpro cartesian references.', file = sys.stderr)
            sys.exit(1)

    def doit(self, path = None, **kwargs):
        assert path != None
        resdict         = {}
        stepsum         = 0
        same_stepsum    = 0
        worse_stepsum   = 0
        better_stepsum  = 0
        same_count      = 0
        worse_count     = 0
        better_count    = 0

        # First create a main working directory. If existant, try to resume.
        testpath = os.path.join(self.maindir, path)
        if os.path.isdir(testpath):
            tmpstructs = []
            #shutil.rmtree(testpath)
            for struct in self.structfiles:
                tmppath = os.path.join(testpath, struct[:-4])
                isdone  = False
                if os.path.isdir(tmppath):
                    # only use the convergence.txt for now to check if it finished ...
                    tmpfile = os.path.join(tmppath, 'convergence.txt')
                    if os.path.isfile(tmpfile):
                        with open(tmpfile, 'r') as convfile:
                            tmplist = [line for line in convfile] # inefficient but simple
                            try:
                                tmpsplit    = tmplist[-1].split()
                                tmpsteps    = int(tmpsplit[0])
                                tmpen       = float(tmpsplit[1])
                                tmpconv     = bool(tmpsplit[-1] == 'True')
                                if tmpen > self.molpro_ref[struct][1] + 2.0e-5:
                                    tmpmin = -1
                                elif tmpen < self.molpro_ref[struct][1] - 2.0e-5:
                                    tmpmin = +1
                                else:
                                    tmpmin = 0
                                if tmpconv or tmpsteps >= 100:
                                    print('INFO: Structure {0} already done. Not repeating the optimization!'.format(struct))
                                    isdone = True
                                    resdict[struct] = (tmpsteps, tmpmin)
                                    stepsum += tmpsteps - self.molpro_ref[struct][0]
                                    if tmpmin == 0:
                                        same_stepsum += tmpsteps - self.molpro_ref[struct][0]
                                        same_count   += 1
                                    elif tmpmin == +1:
                                        better_stepsum += tmpsteps - self.molpro_ref[struct][0]
                                        better_count   += 1
                                    elif tmpmin == -1:
                                        worse_stepsum += tmpsteps - self.molpro_ref[struct][0]
                                        worse_count   += 1
                            except Exception, e:
                                pass # expect an error so just redo it
                            finally:
                                convfile.close()
                    if not isdone: shutil.rmtree(tmppath)
                if not isdone: tmpstructs.append(struct)
            self.structfiles = tmpstructs
        else:
            os.mkdir(testpath)
        self.structfiles.sort()

        if len(self.structfiles) > 0:
            print('\n{0:50s} {1:10s} {2:10s} {3:10s}'.format('Structure', 'Its', 'Its diff', 'Correct?'))
            #Now 'just' loop over every structure and do the corresponding calculation.
            for struct in self.structfiles:
                calcpath = os.path.join(testpath, struct[:-4])
                os.mkdir(calcpath)
                os.chdir(calcpath)
                print('   Doing {0:50s} '.format(struct), end = "")
                sys.stdout.flush()
                #compare directly.
                steps, final_en, __ = runOpt(calc_name = os.path.join(calcpath, struct[:-4]+'_opt'), mol_path = os.path.join(self.sdir, struct), exe = "/home/kumo/Software/molpro_dev_own_parallel/bin/molpro.sh", **kwargs)
                #save the results for later inspection
                stepsum += steps - self.molpro_ref[struct][0]
                # check the energy against baker's minima
                if final_en > self.molpro_ref[struct][1] + 2.0e-5:
                    correct_minimum = -1
                elif final_en < self.molpro_ref[struct][1] - 2.0e-5:
                    correct_minimum = +1
                else:
                    correct_minimum = 0
                resdict[struct] = (steps, correct_minimum)
                print('{0:10} {1:10} {2:10}'.format(steps, steps-self.molpro_ref[struct][0], correct_minimum))

                if correct_minimum == 0:
                    same_stepsum += steps - self.molpro_ref[struct][0]
                    same_count   += 1
                elif correct_minimum == +1:
                    better_stepsum += steps - self.molpro_ref[struct][0]
                    better_count   += 1
                elif correct_minimum == -1:
                    worse_stepsum += steps - self.molpro_ref[struct][0]
                    worse_count   += 1
        print(' Difference of summed steps to Molpro: {0}'.format(stepsum))
        print(' Difference of summed steps to Molpro for {1} same minima: {0}'.format(same_stepsum, same_count))
        print(' Difference of summed steps to Molpro for {1} worse minima: {0}'.format(worse_stepsum, worse_count))
        print(' Difference of summed steps to Molpro for {1} better minima: {0}'.format(better_stepsum, better_count))
        os.chdir(self.maindir)
        return resdict, stepsum


class TestRedundants(TestSet, unittest.TestCase):
    template_str    = "memory,200,m\nnosym\ngdirect\ngeomtyp=xyz\ngeometry={structure}\nbasis=svp\n\ndfunc,pbe\nks\nforces\n"
    en_match        = '!RKS STATE 1.1 Energy'
    grad_match      = 'Atom          dE/dx               dE/dy               dE/dz'
    grad_skip       = 1

    hess_guess = lambda ref,optstep,pclist: (np.identity(optstep.grad_cart.shape[0]), False)
    @unittest.skip('No diags.')
    def test_redundant_diaghess_BFGS_03norm(self):

        result_red, stepsum = self.doit('own_red_diag_bfgs_03norm', templ = self.template_str, maxit = 100, coords = 'REDU', hess = 'DIAG', update = 'BFGS',
                                        smax = 0.3, usenorm = True,
                                        usteps = 1000, esteps = 0, cpus = '8', en_match_str = self.en_match, grad_match_str = self.grad_match, grad_match_skip = self.grad_skip)
        #At the end of the test: Save statistics/differences
        with open('pyopt_redu_diag_bfgs_03norm_steps.dat', 'w') as ofile:
            for struct in result_red:
                print('{0:30s} {1:5} {2:5}'.format(struct, result_red[struct][0], result_red[struct][1]), file = ofile)
            ofile.close()
        self.assertLessEqual(stepsum, 0, msg = 'Warning: Redundant internals with a diagonal Hessian updated with BFGS' +
                                               'optimize longer than Molpro (in average)! stepsum = {0}'.format(stepsum))

    @unittest.skip('No diags.')
    def test_redundant_diaghess_BFGS_03nonorm(self):

        result_red, stepsum = self.doit('own_red_diag_bfgs_03nonorm', templ = self.template_str, maxit = 100, coords = 'REDU', hess = 'DIAG', update = 'BFGS',
                                        smax = 0.3, usenorm = False,
                                        usteps = 1000, esteps = 0, cpus = '8', en_match_str = self.en_match, grad_match_str = self.grad_match, grad_match_skip = self.grad_skip)
        #At the end of the test: Save statistics/differences
        with open('pyopt_redu_diag_bfgs_03norm_steps.dat', 'w') as ofile:
            for struct in result_red:
                print('{0:30s} {1:5} {2:5}'.format(struct, result_red[struct][0], result_red[struct][1]), file = ofile)
            ofile.close()
        self.assertLessEqual(stepsum, 0, msg = 'Warning: Redundant internals with a diagonal Hessian updated with BFGS' +
                                               'optimize longer than Molpro (in average)! stepsum = {0}'.format(stepsum))


    def test_redundant_unithess_BFGS_03norm(self):

        result_red, stepsum = self.doit('own_red_unit_bfgs_03norm', templ = self.template_str, maxit = 100, coords = 'REDU', hess = 'EXACT', update = 'BFGS',
                                        smax = 0.3, usenorm = True, own_hess = self.hess_guess,
                                        usteps = 1000, esteps = 0, cpus = '8', en_match_str = self.en_match, grad_match_str = self.grad_match, grad_match_skip = self.grad_skip)
        #At the end of the test: Save statistics/differences
        with open('pyopt_redu_unit_bfgs_03norm_steps.dat', 'w') as ofile:
            for struct in result_red:
                print('{0:30s} {1:5} {2:5}'.format(struct, result_red[struct][0], result_red[struct][1]), file = ofile)
            ofile.close()
        self.assertLessEqual(stepsum, 0, msg = 'Warning: Redundant internals with a unit Hessian updated with BFGS' +
                                               'optimize longer than Molpro (in average)! stepsum = {0}'.format(stepsum))

    def test_redundant_unithess_BFGS_03nonorm(self):

        result_red, stepsum = self.doit('own_red_unit_bfgs_03nonorm', templ = self.template_str, maxit = 100, coords = 'REDU', hess = 'EXACT', update = 'BFGS',
                                        smax = 0.3, usenorm = False, own_hess = self.hess_guess,
                                        usteps = 1000, esteps = 0, cpus = '8', en_match_str = self.en_match, grad_match_str = self.grad_match, grad_match_skip = self.grad_skip)
        #At the end of the test: Save statistics/differences
        with open('pyopt_redu_unit_bfgs_03nonorm_steps.dat', 'w') as ofile:
            for struct in result_red:
                print('{0:30s} {1:5} {2:5}'.format(struct, result_red[struct][0], result_red[struct][1]), file = ofile)
            ofile.close()
        self.assertLessEqual(stepsum, 0, msg = 'Warning: Redundant internals with a unit Hessian updated with BFGS' +
                                               'optimize longer than Molpro (in average)! stepsum = {0}'.format(stepsum))

    def test_delocalized_unithess_BFGS_03norm(self):

        result_red, stepsum = self.doit('own_delo_unit_bfgs_03norm', templ = self.template_str, maxit = 100, coords = 'DELO', hess = 'EXACT', update = 'BFGS',
                                        smax = 0.3, usenorm = True, own_hess = self.hess_guess,
                                        usteps = 1000, esteps = 0, cpus = '8', en_match_str = self.en_match, grad_match_str = self.grad_match, grad_match_skip = self.grad_skip)
        #At the end of the test: Save statistics/differences
        with open('pyopt_delo_unit_bfgs_03norm_steps.dat', 'w') as ofile:
            for struct in result_red:
                print('{0:30s} {1:5} {2:5}'.format(struct, result_red[struct][0], result_red[struct][1]), file = ofile)
            ofile.close()
        self.assertLessEqual(stepsum, 0, msg = 'Warning: Delocalized internals with a unit Hessian updated with BFGS' +
                                               'optimize longer than Molpro (in average)! stepsum = {0}'.format(stepsum))

    @unittest.skip('No diags.')
    def test_delocalized_diaghess_BFGS_03norm(self):

        result_red, stepsum = self.doit('own_delo_diag_bfgs_03norm', templ = self.template_str, maxit = 100, coords = 'DELO', hess = 'DIAG', update = 'BFGS',
                                        smax = 0.3, usenorm = True, own_hess = self.hess_guess,
                                        usteps = 1000, esteps = 0, cpus = '8', en_match_str = self.en_match, grad_match_str = self.grad_match, grad_match_skip = self.grad_skip)
        #At the end of the test: Save statistics/differences
        with open('pyopt_delo_diag_bfgs_03norm_steps.dat', 'w') as ofile:
            for struct in result_red:
                print('{0:30s} {1:5} {2:5}'.format(struct, result_red[struct][0], result_red[struct][1]), file = ofile)
            ofile.close()
        self.assertLessEqual(stepsum, 0, msg = 'Warning: Delocalized internals with a diagonal Hessian updated with BFGS' +
                                               'optimize longer than Molpro (in average)! stepsum = {0}'.format(stepsum))



if __name__ == '__main__':
    unittest.main(verbosity=2, failfast=False, buffer=False)
