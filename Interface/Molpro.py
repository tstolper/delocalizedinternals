# --* encoding: utf-8 *--

import scipy as np
import scipy.sparse as sparse
import subprocess as sproc
import os
import time
import logging
from Utils import load_Template
from PyOpt.GeomOpt.utility_funcs import log_Vector, log_Matrix
from PyOpt.GeomOpt.Structure import Molecule

class MolproRun(object):

    def __init__(self, basepath = None, template = None, norun = False, *args, **kwargs):
        if not norun:
            self.set_defaults()
            if 'copyfrom' not in kwargs:
                assert basepath is not None
                assert template is not None

                tmppth              = os.path.realpath(basepath)
                self.currdir        = os.getcwd()
                self.work_dir       = os.path.dirname(tmppth)
                self.file_basename  = os.path.basename(tmppth)
                # self.program        = 'molpro'
                # self.options        = '--no-xml-output -d /scr/' + os.environ['USER']

                if os.path.isfile(os.path.join(self.currdir, template)):
                    self.input_template = load_Template(template)
                else: # assume 'template' to contain a string with the input itself.
                    self.input_template = template
            else:
                self.currdir        = kwargs['copyfrom'].currdir
                self.work_dir       = kwargs['copyfrom'].work_dir
                self.file_basename  = kwargs['copyfrom'].file_basename
                self.program        = kwargs['copyfrom'].program
                self.options        = kwargs['copyfrom'].options
                self.input_template = kwargs['copyfrom'].input_template

        self.norun      = norun
        self.ran        = False
        self.success    = False

        if len(args) >= 1:
            self.program = args[0]
        if len(args) >= 2:
            self.options = args[1]
        if 'prog' in kwargs:
            self.program = kwargs['prog']
        if 'options' in kwargs and kwargs['options'] is not None:
            self.options = kwargs['options']

        # Parsing options
        if 'en_match' in kwargs: self.en_match = kwargs['en_match']
        if 'grad_match' in kwargs and 'grad_stop' in kwargs:
            if 'grad_skip' in kwargs: self.grad_skip = kwargs['grad_skip']
            self.grad_match = kwargs['grad_match']
            self.grad_stop  = kwargs['grad_stop']
        if 'hess_match' in kwargs: self.hess_match = kwargs['hess_match']

    def set_defaults(self):
        self.program        = 'molpro'
        self.options        = '--no-xml-output -d /scr/' + os.environ['USER']
        self.write_stdout   = False
        self.en_match       = '!RHF STATE 1.1 Energy'
        self.grad_skip      = 1
        self.grad_match     = 'Atom dE/dx dE/dy dE/dz'
        self.grad_stop      = 'Nuclear force contribution to virial'
        self.hess_match     = 'Force Constants (Second Derivatives of the Energy) in [a.u.]'
        # the following may better be class variables?
        self.xyzininp       = '{{\n{xyz}\n}}'
        self.grad_pos       = (1,2,3)
        self.errors_pos     = ('error', '?')
        self.errors_neg     = ('?WARNING: GRADIENT NORM NOT ZERO!',)

    def change_basename(self, basename = None):
        tmppth              = os.path.realpath(basename)
        self.currdir        = os.getcwd()
        self.work_dir       = os.path.dirname(tmppth)
        self.file_basename  = os.path.basename(tmppth)

    def run_calc(self, struct = None):
        if not os.path.isdir(self.work_dir):
            if os.pardir in self.work_dir:
                raise OSError('Please refrain from using .. in the path specification.')
            os.makedirs(self.work_dir, 0755)
        os.chdir(self.work_dir)
        self.success    = False
        ifile           = self.file_basename + '.inp'
        if os.path.isfile(ifile[:-4] + '.out'):
            os.rename(ifile[:-4] + '.out', ifile[:-4] + '.out_' + str(int(time.time())))
        #logging.log(logging.VERYVERBOSE, 'MOLPRO: Running file {0}'.format(ifile))
        with open(ifile, 'w') as inpfile:
            if '.xyz' in struct:
                print >> inpfile, self.input_template.format(structure = struct)
            else:
                print >> inpfile, self.input_template.format(structure = self.xyzininp.format(xyz=struct))
            inpfile.close()
        #print [self.program] + self.options.split() + [ifile]
        if self.write_stdout:
            output_file = open(ifile[:-4] + '.out', 'w')
        else:
            output_file = None
        run_error = sproc.call([self.program] + self.options.split() + [ifile], stdout=output_file)
        if output_file is not None:
            output_file.close()
        if run_error:
            raise RuntimeError('Molpro complained!')
        self.ran = True
        os.chdir(self.currdir)

    def check_success(self, match_str = None, opath = None):
        """ The way to success:
        + Check if the output exists.
        + If it does, check if it contains the match string.
        - Also check if there are lines in which the first character is a question mark.
        - Check if 'error' (case insesitive) is anywhere in the file.
        """
        assert match_str != None
        assert opath != None
        success = False
        if self.ran or self.norun:
            ofile = opath
            if os.path.isfile(ofile):
                with open(ofile, 'r') as output:
                    for line in output:
                        if match_str in line:
                            success = True
                            #logging.log(logging.VERYVERBOSE, 'Calculation {0} finished!'.format(opath))
                        elif True in [err in line for err in self.errors_pos] and True not in [noterr in line for noterr in self.errors_neg]:
                            success = False
                            #logging.log(logging.VERYVERBOSE, 'Error in Calculation {0}'.format(opath))
                            break
                    output.close()
        else:
            raise ValueError('You did not run the calculation yet! Path: {0}'.format(ofile))
        if not self.norun:
            self.success = success
        return success

    def get_start_Mol(self, **kwargs):
        if 'opath' in kwargs:
            opath   = kwargs['opath']
        else:
            opath   = os.path.join(self.work_dir, self.file_basename) + '.out'

        # don't need to succeed for the input structure.
        structure_str = None
        with open(opath, 'r') as ofile:
            for line in ofile:
                lsplit = line.lstrip()
                if len(lsplit) > 0:
                    if lsplit.lower()[:8] == 'geometry':
                        structure_str = ''
                    elif lsplit[0] == '}':
                        break
                    elif structure_str != None:
                        structure_str += lsplit
        mol = Molecule()
        mol.load_from_String(sstring = structure_str, stype = 'XYZ')
        return mol

    def get_next_Energy(self, match_str = None, **kwargs):

        if 'opath' in kwargs:
            ran     = True
            opath   = kwargs['opath']
        else:
            ran     = self.ran
            opath   = os.path.join(self.work_dir, self.file_basename) + '.out'

        if match_str != None:
            match = match_str
        else:
            match = self.en_match

        if self.success or self.check_success(match_str = match, opath = opath):
            with open(opath, 'r') as ofile:
                for line in ofile:
                    lsplit = line.split()
                    if match in line:
                        yield float(lsplit[-1])
                ofile.close()
        else:
            raise ValueError('You did not run the calculation yet! Searching for "{0}".'.format(match_str))

    def get_next_Gradient(self, match_str = None, stop_str = None, **kwargs):
        tmpgrad = None

        if 'opath' in kwargs:
            ran     = True
            opath   = kwargs['opath']
        else:
            ran     = self.ran
            opath   = os.path.join(self.work_dir, self.file_basename) + '.out'

        if 'match_skip' in kwargs:
            pm_skip = kwargs['match_skip']
        else:
            pm_skip = self.grad_skip

        if match_str != None:
            match = match_str
        else:
            match = self.grad_match

        if stop_str != None:
            stop = stop_str
        else:
            stop = self.grad_stop

        if self.success or self.check_success(match_str = match, opath = opath):
            tmpgrad = []
            match_split = match.split()
            stop_split  = stop.split()
            mlen    = len(match_split)
            slen    = len(stop_split)
            with open(opath, 'r') as ofile:
                for line in ofile:
                    if match_split == line.split()[:mlen]:
                        for __ in range(pm_skip): next(ofile)
                        gline   = next(ofile)
                        glsplit = gline.split()
                        while len(glsplit) >= 4:
                            tmpgrad.append([float(glsplit[pos]) for pos in self.grad_pos])
                            gline   = next(ofile)
                            glsplit = gline.split()
                            while len(glsplit) == 0:
                                gline   = next(ofile)
                                glsplit = gline.split()
                            if stop_split == glsplit[:slen]: break
                        yield np.array(tmpgrad)
                        tmpgrad = []
                ofile.close()
        else:
            raise ValueError('You did not run the calculation yet!')

    def get_next_Hessian(self, match_str = None, **kwargs):
        tmphess = None

        if 'file' in kwargs:
            ran     = True
            opath   = kwargs['file']
        else:
            ran     = self.ran
            opath   = os.path.join(self.work_dir, self.file_basename) + '.out'

        if match_str != None:
            match = match_str
        else:
            match = self.hess_match

        if self.success or self.check_success(match_str = match, opath = opath):
            with open(opath, 'r') as ofile:
                for line in ofile :
                    if match in line:
                        tmphess = []
                        next(ofile) # skip header
                        hline   = next(ofile)
                        hlsplit = hline.split()
                        while len(hlsplit) > 1: # no empty lines
                            if hlsplit[0][1] == 'X':
                                coord_idx = 0
                            elif hlsplit[0][1] == 'Y':
                                coord_idx = 1
                            elif hlsplit[0][1] == 'Z':
                                coord_idx = 2
                            row_idx = (int(hlsplit[0][2:]) - 1)*3 + coord_idx

                            if len(tmphess) <= row_idx: tmphess.append([])

                            for c2deriv in hlsplit[1:]:
                                tmphess[row_idx].append(float(c2deriv))
                            hlsplit = next(ofile).split()
                            if len(hlsplit) > 0:
                                if hlsplit[-1][0] == 'G': hlsplit = next(ofile).split()

                        data = []
                        idxs = []
                        cols = [0]
                        for row in tmphess:
                            data.extend(row)
                            idxs.extend(range(len(row)))
                            cols.append(len(row) + cols[-1])

                        yield sparse.csr_matrix((np.array(data),np.array(idxs),np.array(cols)), shape = (len(tmphess), len(tmphess)))

    def get_Energy(self, idx = 1, **kwargs):
        tmpen   = 0.0
        en_gen  = self.get_next_Energy(**kwargs)
        for i in range(idx):
            tmpen = next(en_gen)
        return tmpen

    def get_Gradient(self, idx = 1, **kwargs):
        tmpgrad   = 0
        grad_gen  = self.get_next_Gradient(**kwargs)
        for i in range(idx):
            tmpgrad = next(grad_gen)
        log_Matrix('Read gradient:', tmpgrad)
        return tmpgrad

    def get_Hessian(self, idx = 1, **kwargs):
        tmphess   = 0
        hess_gen  = self.get_next_Hessian(**kwargs)
        for i in range(idx):
            tmphess = next(hess_gen)
        return tmphess


