
# -*- coding: utf-8 -*-

import os
import time
import logging

from Interface.Molpro import MolproRun
import GeomOpt.Read as Read

class GWDG(object):

    def __init__(self, job = None, batch = 0):
        assert job != None

        self.refjob         = job
        self.joblist        = None
        self.jobs2go        = None # list of indices of total jobs which stille need to be run/finish
        self.jobs_running   = None
        self.batching       = batch
        self.structures     = None

    def spawn_Jobs(self, structs = None, dirprefix = 'tmp', read_path = None):
        """ FIXME: a bunch of jobs should be submitted, instead of structures. Or, alternatively, at least
        also a list of job names.

        Spawn a bunch of similar jobs.
        1.) Assemble a list of QMSuite classes (jobs) corresponding to the list of gradient index, structure
            tuples as supplied with the 'structs' option.
        2.) Change the basename of all the jobs and start (maybe submit) them.
        """
        assert structs != None

        if read_path != None and len(read_path) > 0:
            tmpdir = read_path
        else:
            tmpdir = '{0}_{1}'.format(dirprefix, int(time.time()))
        joblist = []
        jobrun  = []
        for idx, s in structs:
            joblist.append((idx, MolproRun(copyfrom = self.refjob)))
            joblist[-1][1].change_basename(os.path.join(tmpdir, 'tmpcalc_{0:05d}'.format(idx)))
        for jdx, jb in enumerate(joblist):
            jb[1].run_calc(struct = structs[jdx][1].get_String())
            jobrun.append(jdx)
            if self.batching > 0 and jdx+1 >= self.batching:
                break

        self.joblist = joblist
        self.jobs2go = range(len(joblist))
        self.jobs_running   = jobrun
        self.structures     = [ s for i, s in structs ]

        return joblist


    def spawn_Jobs_and_wait(self, structs = None, max_time = -1, wait_iter = 1800):
        """ FIXME -> structs = [(idx, MolproRun), ...]
        Spawn jobs and return after they're done.

        structs:        list of Structure classes
        max_time:       maximum time to wait before returning in seconds.
                        default is -1, i.e. infinite.
        """
        assert structs != None

        joblist     = self.spawn_Jobs(structs)
        start_time  = time.time()
        jobcount    = len(joblist)

        curr_time   = time.time()
        while (curr_time - start_time < max_time) or max_time < 0:
            done_count = 0
            for j in joblist:
                if j.get_Energy() != 0:
                    done_count += 1
            if done_count == jobcount:
                break
            else:
                time.sleep(wait_iter)
                curr_time = time.time()
        return joblist

    def wait_and_get_missing_Energies(self, wait_time = 300, **kwargs):
        """ Takes a list (filled with 'None's) that has the same length as the list of jobs,
        waits 'wait_time' seconds and then checks the jobs for new results ('None' elements).
        The return value gives the number of still missing values and a list of tuples of
        element indices with their value.

        This function can then be called again to try to get more results.
        """
        new_vals    = []
        topop       = []
        time.sleep(wait_time)
        for i in self.jobs_running:
            try:
                cener = self.joblist[i][1].get_Energy(**kwargs)
                if cener < 0.0:
                    new_vals.append((self.joblist[i][0], cener))
                    topop.append(i)
            except ValueError:
                logging.log(logging.VERYVERBOSE, 'Job {0:4d} did not finish yet.'.format(i))
        # clean done jobs out of todo list
        for i in topop:
            self.jobs2go.pop(self.jobs2go.index(i))
            self.jobs_running.pop(self.jobs_running.index(i))
        # start new jobs in case of batching
        if self.batching > 0 and len(self.jobs2go) > 0:
            jobs_to_start = list(set(self.jobs2go) - set(self.jobs_running))
            free_slots = self.batching - len(self.jobs_running)
            if free_slots > len(jobs_to_start):
                free_slots = len(jobs_to_start)
            logging.log(logging.VERYVERBOSE, 'CLUSTER: free slots = {0:5d}'.format(free_slots))
            for i in range(free_slots):
                jidx, job = self.joblist[jobs_to_start[i]]
                job.run_calc(struct = self.structures[jobs_to_start[i]].get_String())
                self.jobs_running.append(jobs_to_start[i])
                logging.log(logging.VERYVERBOSE, 'CLUSTER: Starting job {0}'.format(job.file_basename))

        return len(self.jobs2go), new_vals

    def assemble(self, structs = None): pass


