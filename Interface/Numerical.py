
# -*- coding: utf-8 -*-

import copy
import time
import logging
import scipy as np

from operator import itemgetter
from GeomOpt.Structure import Molecule
from GeomOpt.atomprops import Bohr2Ang
from Interface import Molpro
from Utils import get_Kwarg
from GeomOpt.utility_funcs import log_Vector, log_Matrix
from GeomOpt import Storage

def get_Structures(dim = 'GRAD', refstruct = None, refmol = None, displ = 0.01, skip_idxs = None):
    assert refstruct != None
    assert refmol != None

    los_tuples = []
    if dim == 'GRAD':
        tmpshift = np.zeros(refstruct.shape)
        for idx in range(0, refstruct.shape[0]):
            if skip_idxs == None or idx*2 not in skip_idxs:
                tmpshift[idx] = displ
                los_tuples.append((idx*2, Molecule(copyfrom = refmol)))
                los_tuples[-1][1].set_coords((refstruct+tmpshift).reshape(refmol.natoms, 3), units = 'BOHR')
            if skip_idxs == None or idx*2+1 not in skip_idxs:
                tmpshift[idx] = -displ
                los_tuples.append((idx*2+1, Molecule(copyfrom = refmol)))
                los_tuples[-1][1].set_coords((refstruct+tmpshift).reshape(refmol.natoms, 3), units = 'BOHR')
            tmpshift[idx] = 0.0
    elif dim == 'HESS':
        tmp_displ   = np.zeros(refstruct.shape)
        tmpit       = 0
        for i in range(len(skip_idxs)):
            logging.debug('    skip_idxs[{0}] = {1}'.format(i, skip_idxs[i]))
        for i in range(refstruct.shape[0]):
            for j in range(i, refstruct.shape[0]):
                logging.debug('    i={0}, j={1}, in skip_idxs: {2}'.format(i, j, tmpit in skip_idxs))
                if skip_idxs == None or tmpit not in skip_idxs:
                    if i == j: # the diagonal
                        tmp_displ[j] = displ
                    else:
                        tmp_displ[i] = displ
                        tmp_displ[j] = -displ
                    los_tuples.append((tmpit, Molecule(copyfrom = refmol)))
                    los_tuples[-1][1].set_coords(np.add(refstruct, tmp_displ).reshape(refmol.natoms, 3), units = 'BOHR')
                if skip_idxs == None or tmpit+1 not in skip_idxs:
                    if i == j:
                        tmp_displ[j]  = -displ
                    else:
                        tmp_displ[i]  = -displ
                        tmp_displ[j]  = displ
                    los_tuples.append((tmpit+1, Molecule(copyfrom = refmol)))
                    los_tuples[-1][1].set_coords(np.add(refstruct, tmp_displ).reshape(refmol.natoms, 3), units = 'BOHR')

                tmp_displ[i]    = 0.0
                tmp_displ[j]    = 0.0
                tmpit += 2
    return los_tuples

def calc_numer_Cart(**kwargs): pass

def calc_numer_Grad(qmsuite = None, mngr = None, mol = None, displ = 0.01, **kwargs):
    """ Calculate a numerical gradient, of course. However, right now I can imagine two scenarios to do that
    (at my workplace), if we're restricted to a certain walltime which is going to be exceeded.

    1.) The gradient is calculated numerically, but the main job is on a compute note and thus also subject to
        a walltime. Anyway, this requires us to save the intermediate results and to restart if necessary.
    2.) The main job may run as long as it takes but the SP calculations may not. Therefore one could make use
        of a large cluster by submitting the expensive jobs as single calculations and just wait for the results
        to finish.

    Of course one could think of scenarios in between, but I don't see the reason to explicitly handle that.
    Actually maybe it is even advantageous to ALWAYS save to a restart file, right when an energy calculation finished.

    Version 2 is right now done by just starting everything and polling once in a while. Spawning threads would
    be another option.

    ALSO: The use of the correct script/executable is the user's responsibility! That means one should use
        a submit script for every calculation if a resource manager is used and set the QMSuite to poll the
        output. Otherwise just wait for the program to finish (standard).
    """
    logging.debug('*   Numerical Gradient Calculation')
    # configuration
    if 'wtime' in kwargs:
        wtime = kwargs['wtime']
    else:
        wtime = 1800
    if 'read' in kwargs:
        read    = kwargs['read']
    else:
        read    = None
    numgrad     = None
    tmpmol      = Molecule(copyfrom = mol)
    refstruct   = tmpmol.get_flat_Array(units = 'BOHR')

    # make a list of structures
    idx_done        = [] # indices of the energy calculation, i.e. 2*(3N) max.
    val_done        = []
    skip            = None
    rpath           = None
    if read != None:
        skip = Storage.grad_idx_done[read]
        idx_done.extend(skip)
        val_done.extend(Storage.grad_val_done[read])
        rpath = Storage.grad_paths[read]
    list_of_structs = get_Structures(dim = 'GRAD', refstruct = refstruct, refmol = tmpmol, displ = displ, skip_idxs = skip)

    logging.info('Doing {0:5d} single points for numerical gradient.'.format(len(list_of_structs)))

    if mngr == None:
        numgrad = np.zeros((mol.natoms*3,))
        # This means we don't want to submit the jobs to a queueing system, but just start it locally.
        for i, s in list_of_structs:
            qmsuite.run_calc(struct = s.get_String())
            idx_done.append(i)
            val_done.append(qmsuite.get_Energy(**kwargs))
    else:
        # submit the jobs and get the energies. Read only needed to use the folder of the read gradient.
        mngr.spawn_Jobs(structs = list_of_structs, dirprefix = 'tmpgrad', read_path = rpath)
        still_missing   = len(list_of_structs)
        while still_missing != 0:
            still_missing, new_vals = mngr.wait_and_get_missing_Energies(wait_time = wtime, **kwargs)
            if (still_missing == 0 and len(new_vals) == 0): # things that shouldn't happen
                raise ValueError('ERROR: still_missing={0}, new_vals={1}'.format(still_missing, new_vals))
            if len(new_vals) > 0:
                idxs, vals = map(list, zip(*new_vals))
                idx_done.extend(idxs)
                val_done.extend(vals)
                if still_missing%100 == 0:
                    logging.info('Did {0:5d} single points.'.format(len(list_of_structs)-still_missing))
    # merge the saved and calculated results
    allres  = sorted(zip(idx_done, val_done), key = itemgetter(0))
    logging.debug('*** Raw gradient data')
    for i,v in allres:
        logging.debug('{0:5d} {1:15.8f}'.format(i,v))
    # reduce to gradient energies
    numgrad = np.zeros((mol.natoms*3,))
    for i in range(0, len(allres), 2):
        if allres[i][0] != i:
            raise ValueError('    ERROR: index of numerical gradient not sorted properly. i = {0}, allres[i] = {1}'.format(i, allres[i]))
        numgrad[i/2] = (allres[i][1] - allres[i+1][1])/(2.0*displ)
        logging.debug('    Doing gradient part. i={0}, displ={1}, numgrad[i/2]={2}'.format(i, displ, numgrad[i/2]))

    return numgrad

def calc_Hess_cart(qmsuite = None, mngr = None, mol = None, ref_energy = None, **kwargs):
    """ Calculates the Hessian numerically by using cartesian coordinates.
        TODO: Adapt to read in values. """
    assert qmsuite != None
    assert mol != None
    assert ref_energy != None

    logging.info('*   Numerical Hessian calculation')

    # configuration
    wtime   = get_Kwarg(kwargs, 'wtime', 1800)
    displ   = get_Kwarg(kwargs, 'displ', 0.01)
    read    = get_Kwarg(kwargs, 'read', None)
    #adispl  = get_Kwarg(kwargs, 'adispl', 1.0*np.pi/180.0)

    numhess     = None
    tmpmol      = Molecule(copyfrom = mol)
    refstruct   = tmpmol.get_flat_Array(units = 'BOHR')
    coordnum    = refstruct.shape[0]

    idx_done    = []
    val_done    = []
    skip        = None
    rpath       = None
    if read != None:
        skip = Storage.hess_idx_done[read]
        idx_done.extend(skip)
        val_done.extend(Storage.hess_val_done[read])
        rpath = Storage.hess_paths[read]
        logging.info(    'Starting from read Hessian! Skipping {0} calculations.'.format(len(skip)))
    list_of_structs = get_Structures(dim = 'HESS', refstruct = refstruct, refmol = tmpmol, displ = displ, skip_idxs = skip)


    # just calculate the energies
    logging.info('    Doing {0} single points for the cartesian Hessian.'.format(len(list_of_structs)))
    tmp_en_array = np.zeros((len(list_of_structs)))
    if mngr == None:
        for i, s in list_of_structs:
            qmsuite.run_calc(struct = s.get_String())
            idx_done.append(i)
            val_done.append(qmsuite.get_Energy(**kwargs))
            if i%100 == 0:
                logging.info('    Calculations done: {0:10d}'.format(i))
    else:
        mngr.spawn_Jobs(structs = list_of_structs, dirprefix = 'tmphess', read_path = rpath)
        still_missing = len(list_of_structs)
        while still_missing != 0:
            still_missing, new_vals = mngr.wait_and_get_missing_Energies(wait_time = wtime, **kwargs)
            if len(new_vals) > 0:
                idxs, vals = map(list, zip(*new_vals))
                idx_done.extend(idxs)
                val_done.extend(vals)
                if still_missing%100 == 0:
                    logging.info('    Calculations missing: {0:10d}'.format(still_missing))
        else:
            logging.debug('    All energies for Hessian done!')
    # merge the saved and calculated results
    allres  = sorted(zip(idx_done, val_done), key = itemgetter(0))

    # and assemble
    numhess = np.zeros((coordnum, coordnum))
    # first the diagonal
    tmpit = 0
    for i in range(coordnum):
        if allres[i][0] != i:
            raise ValueError('    ERROR: Something is wrong! Sorted results do not fit for i={0}.'.format(i))
        numhess[i,i] = (allres[tmpit][1] + allres[tmpit+1][1] - 2.0*ref_energy) / (displ)**2
        tmpit += 2*(coordnum - i)
    tmpit = 0
    for i in range(coordnum):
        for j in range(i+1, coordnum):
            if allres[j][0] != j:
                raise ValueError('    ERROR: Something is wrong! Sorted results do not fit for j={0}.'.format(j))
            numhess[i,j] = displ*numhess[i,i]/(2.0*displ) + displ*numhess[j,j]/(2.0*displ)  - (allres[tmpit+2*(j-i)][1] + allres[tmpit+2*(j-i)+1][1] - 2.0*ref_energy) / (2.0*displ*displ)
            numhess[j,i] = numhess[i,j]
        tmpit += 2*(coordnum - i)

    return numhess

def calc_Hess_deloc(coordref = None, qmsuite = None, mngr = None, mol = None, ref_energy = None, **kwargs):
    """ Calculates the Hessian numerically by using delocalized internal coordinates. """
    assert coordref != None
    assert qmsuite != None
    assert mol != None
    assert ref_energy != None

    # configuration
    wtime   = get_Kwarg(kwargs, 'wtime', 1800)
    rdispl  = get_Kwarg(kwargs, 'rdispl', 0.01)
    #adispl  = get_Kwarg(kwargs, 'adispl', 1.0*np.pi/180.0)

    numhess     = None
    tmpmol      = Molecule(copyfrom = mol)
    refstruct   = tmpmol.get_flat_Array(units = 'BOHR')
    ref_deloc   = copy.deepcopy(coordref.current_int)
    tmp_displ   = np.zeros(ref_deloc.shape)
    coord_list  = coordref.coords.get_Coords()
    coordnum    = ref_deloc.shape[0]
    list_of_structs = []

    # This is just a quick check to validate if it'd make more sense to use the real displacement of the primitive internal coords
    deloc_displ = rdispl*np.ones((coordnum,))

    # first create the necessary structures see E. Miliordos, S. S. Xantheas, JPCA, 2013, 117,7019--7029.
    tmpit = 0
    for i in range(coordnum):
        for j in range(i, coordnum):

            if i == j: # the diagonal
                tmp_displ[j] = deloc_displ[j]
            else:
                tmp_displ[i] = deloc_displ[i]
                tmp_displ[j] = -deloc_displ[j]
            # get cartesian
            bkt_res_plus = coordref.transform_back(ref_deloc, refstruct, tmp_displ, coordref.Bmat)
            tmpmol.set_coords(bkt_res_plus[2].reshape(tmpmol.natoms, 3), units = 'BOHR')
            list_of_structs.append((tmpit, Molecule(copyfrom = tmpmol)))
            displ_plus  = np.sum(np.absolute(bkt_res_plus[3]-ref_deloc))

            if i == j:
                tmp_displ[j]  = -deloc_displ[j]
            else:
                tmp_displ[i]  = -deloc_displ[i]
                tmp_displ[j]  = deloc_displ[j]
            bkt_res_minus = coordref.transform_back(ref_deloc, refstruct, tmp_displ, coordref.Bmat)
            tmpmol.set_coords(bkt_res_minus[2].reshape(tmpmol.natoms, 3), units = 'BOHR')
            list_of_structs.append((tmpit+1, Molecule(copyfrom = tmpmol)))
            displ_minus     = np.sum(np.absolute(bkt_res_minus[3]-ref_deloc))

            tmp_displ[i]    = 0.0
            tmp_displ[j]    = 0.0
            if i == j:
                displdiff = displ_plus+displ_minus-2*deloc_displ[j]
            else:
                displdiff = displ_plus+displ_minus-2*deloc_displ[j]-2*deloc_displ[i]
            print 'Index = {0:4}. Conv pos = {1:6s}, Conv neg = {2:6s}. Displ diff = {3:12.8f}. CurrRMS = {4:12}'.format(tmpit, repr(bkt_res_plus[0]), repr(bkt_res_minus[0]), displdiff, bkt_res_plus[1])
            tmpit += 2

    print 'Calculating the numerical hessian using {0} single points.'.format(len(list_of_structs))

    # just calculate the energies
    # tmp_en_array = np.zeros((len(list_of_structs)))
    # if mngr == None:
    #     for i, s in list_of_structs:
    #         qmsuite.run_calc(struct = s.get_String())
    #         tmp_en_array[i] = qmsuite.get_Energy(**kwargs)
    # else:
    #     mngr.spawn_Jobs(structs = list_of_structs, dirprefix = 'tmphess')
    #     still_missing = len(list_of_structs)
    #     while still_missing != 0:
    #         still_missing, new_vals = mngr.wait_and_get_missing_Energies(wait_time = wtime, **kwargs)
    #         for i, en in new_vals:
    #             tmp_en_array[i] = en
    #     else:
    #         print 'All energies done!'

    # # and assemble
    # numhess = np.zeros((coordnum, coordnum))
    # # first the diagonal
    # tmpit = 0
    # for i in range(coordnum):
    #     numhess[i,i] = (tmp_en_array[tmpit] + tmp_en_array[tmpit+1] - 2.0*ref_energy) / (deloc_displ[i])**2
    #     print 'calc1 = {0}, calc2 = {1}, en1 = {2}, en2 = {3}, val = {4}'.format(tmpit, tmpit+1, tmp_en_array[tmpit], tmp_en_array[tmpit+1], numhess[i,i])
    #     tmpit += 2*(coordnum - i)
    # tmpit = 0
    # for i in range(coordnum):
    #     for j in range(i+1, coordnum):
    #         numhess[i,j] = deloc_displ[i]*numhess[i,i]/(2.0*deloc_displ[j]) + deloc_displ[j]*numhess[j,j]/(2.0*deloc_displ[i])  - (tmp_en_array[tmpit+2*(j-i)] + tmp_en_array[tmpit+2*(j-i)+1] - 2.0*ref_energy) / (2.0*deloc_displ[i]*deloc_displ[j])
    #         numhess[j,i] = numhess[i,j]
    #         print 'i={0}, j={1}'.format(i,j)
    #         print 'calc1 = {0}, calc2 = {1}, en1 = {2}, en2 = {3}, val = {4}'.format(tmpit+2*(j-i), tmpit+2*(j-i)+1, tmp_en_array[tmpit+2*(j-i)], tmp_en_array[tmpit+2*(j-i)+1], numhess[i,j])
    #     tmpit += 2*(coordnum - i)

    # print repr(numhess)

    return numhess
    
