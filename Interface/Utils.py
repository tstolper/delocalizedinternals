
#-- coding: utf-8 --

def load_Template(fpath = None):
    assert fpath != None
    tmpstr = ""

    with open(fpath, 'r') as template:
        tmpstr = ''.join(template.readlines())
        template.close()
    return tmpstr

def get_Kwarg(kwargdict = None, valstr = None, defval = None):
    retval = defval
    if kwargdict != None:
        if valstr in kwargdict:
            retval = kwargdict[valstr]
    return retval