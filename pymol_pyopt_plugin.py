#!/usr/bin/env python

import time
import threading
import logging
import ttk
from Tkinter import *
from pymol import cmd

import matplotlib.pyplot as plt
import matplotlib.backends.backend_tkagg as tkagg

from GeomOpt.Structure import Molecule
from GeomOpt.optimize import Optimizer
from Interface.Molpro import MolproRun

def __init__(self):
    self.menuBar.addmenuitem('Plugin', 'command', 'PyOpt', label='PyOpt', command=run_opt)


class PymolObject(object):
    def __init__(self, pm_name=None):
        assert None not in (pm_name,)
        self.name   = pm_name
        self.states = []
        self.natoms = None

        struct = []
        cmd.iterate_state(1, self.name, 'coords.append([name, x,y,z])', space={'coords': struct})
        self.states.append(struct)
        self.natoms = len(struct)

    def get_XYZString(self, state=1, units='ANG'):
        res = '{:4d}\n\n'.format(len(self.states[state-1]))
        for idx, line in enumerate(self.states[state-1]):
            res += '{:3s} {:12.6f} {:12.6f} {:12.6f}'.format(*line)
            if idx < self.natoms-1:
                res += '\n'
        return res

    def append_State(self, mol=None):
        # TODO:
        # - Assert the same order of the atoms
        assert mol is not None
        tmp = [ [atm.element, atm.X, atm.Y, atm.Z] for atm in mol.atoms ]
        self.states.append(tmp)
        cmd.create(self.name, self.name, 1, len(self.states))
        cmd.alter_state(len(self.states), self.name, 'x,y,z=coords[rank]', space={'coords': mol.get_flat_Array(units='ANG').reshape(mol.natoms, 3)})


loaded_objects = {}

def load_PyMol_Objects():
    res = []
    for mol in cmd.get_names_of_type("object:molecule"):
        loaded_objects[mol] = PymolObject(pm_name=mol)
        res.append(mol)
    return res

class Optimization(threading.Thread):
    def __init__(self, doforces=None, opts=None, prg=None, templ=None, em=None, gm=None, sele=None, plotfunc=None, rootwin=None):
        assert None not in (doforces, opts, prg, templ, em, gm, sele, plotfunc, rootwin)
        threading.Thread.__init__(self)
        self.mainwin        = rootwin
        self.use_forces     = doforces
        self.prog_options   = opts
        self.prog_exe       = prg
        self.update_plot    = plotfunc
        self.templ_en       = templ
        self.energy_match   = em
        self.grad_match     = gm
        self.selections     = sele
        self.convergence    = {'energy diff': 1.0e-6, 'max grads': 3.0e-4}

    def run(self):
        global opt_is_running
        if opt_is_running:
            print 'Error: Currently not more than one optimization allowed.'
        else:
            opt_is_running = True
            # Basic setup
            mol = Molecule()
            #mol.load_from_String(calc_structure, 'XYZ')
            
            templ = self.templ_en.get(1.0, END)
            if self.use_forces.get() > 0:
                templ += '\nforces'
            qmprog      = MolproRun('bla', templ , options=self.prog_options.get(), prog=self.prog_exe.get(), detach=False)
            get_en      = lambda m: self.get_calc_Energy(m, qmprog)
            get_grad    = lambda m: qmprog.get_Gradient(match_str=self.grad_match.get()).flatten()
            # Variable loading
            pm_obj  = loaded_objects[self.selections['Structure'][0].get()]
            struct  = pm_obj.get_XYZString(state=1)
            method  = self.selections['Opt Method'][0].get()
            hess    = self.selections['H guess'][0].get()
            update  = self.selections['Update'][0].get()
            coords  = self.selections['Coords'][0].get()
            upd_steps   = 10
            exact_steps = 0
            mol.load_from_String(struct, 'XYZ')
            stats   = {'energy diff': [], 'max grads': [], 'norm grads': [], 'rms steps': []}
            # Run it
            stepgen = Optimizer(molfile=mol, method=method, hess=hess, hessupd=update, coordsys=coords,
                              getEner=get_en, getGrad=get_grad, getHess=None, update_steps=upd_steps,
                              exact_steps=exact_steps, opt_options={'write_trj': True}).minimizer(enthresh=1.0e-6, gradthresh=3.0e-4, stepsthresh=3.0e-4, maxsteps=100)
            for step in stepgen:
                s_mol, s_en, s_de, s_rmsdx, s_maxg, s_normg, s_isconv = step
                stats['energy diff'].append(s_de)
                stats['max grads'].append(s_maxg)
                stats['norm grads'].append(s_normg)
                stats['rms steps'].append(s_rmsdx)
                pm_obj.append_State(mol=s_mol)
                self.update_plot(stats)
            opt_is_running = False

    def get_calc_Energy(self, molec = None, prog = None):
        assert molec is not None
        assert prog is not None

        energy  = 1.0
        wtime   = 10
        en_str  = self.energy_match.get()
        # TODO Check for existing file, compare and rename if necessary!
        prog.run_calc(molec.get_String(units = 'ANG'))
        energy = prog.get_Energy(match_str = en_str)
        if energy > 0.0:
            raise ValueError('Something is amiss, we do not want positive energies.')
        return energy

opt_is_running = False # unnecessary, since threads can only be started once. -> catch exception

class PlotFigure(object):
    def __init__(self, root=None):
        assert root is not None
        self.fig    = plt.Figure(figsize=(7,8))
        self.axes   = {'energy diff': self.fig.add_subplot(221), 'max grads': self.fig.add_subplot(222),
                        'norm grads': self.fig.add_subplot(223), 'rms steps': self.fig.add_subplot(224)}
        self.plots  = {'energy diff': self.axes['energy diff'].plot([0], [0])[0],
                        'max grads': self.axes['max grads'].plot([0], [0])[0],
                        'norm grads': self.axes['norm grads'].plot([0], [0])[0],
                        'rms steps': self.axes['rms steps'].plot([0], [0])[0]}
        self.canvas = tkagg.FigureCanvasTkAgg(self.fig, master=root)
        self.canvas.get_tk_widget().pack(fill='both')
        #self.canvas._tkcanvas.pack(side='top', fill='both', expand=1)
        self.toolbar= tkagg.NavigationToolbar2TkAgg(self.canvas, root)
        self.toolbar.update()
        self.toolbar.pack()
        for k in self.axes.keys():
            self.axes[k].set_title(k)
            self.axes[k].get_xaxis().set_major_locator(plt.MaxNLocator(integer=True)) # for integer steps
            # if k in lines:
            #     self.axes[k].axhline(y=lines[k], c='red')
        self.canvas.draw()

    def replot(self, values=None):
        assert None not in (values, )
        nsteps = len(values['energy diff'])
        X  = range(1, nsteps+1) # should be the same for every plot
        for k in values.keys():
            if k in self.plots:
                self.plots[k].set_xdata(X)
                self.plots[k].set_ydata(values[k])
                if nsteps > 10:
                    start = nsteps-10
                else:
                    start = 1
                self.axes[k].set_xlim(start, nsteps)
                self.axes[k].set_ylim(min(values[k][start-1:]), max(values[k][start-1:]))
            else:
                print 'WARNING: Passed values "{0}", but no axes defined.'.format(k)
        self.canvas.draw()

def build_selboxes(listofvals=None, col=0, root=None):
    assert listofvals is not None
    assert root is not None
    boxes = {}
    for r, b in enumerate(listofvals):
        Label(root, text=b[0]).grid(row=r, column=col)
        svar = StringVar()
        box  = ttk.Combobox(root, justify='left', height=1, state='readonly', textvariable=svar, values=b[1])
        box.grid(row=r, column=col+1)
        try:
            box.current(0)
        except TclError, e:
            print 'Error:', e
        boxes[b[0]] = (svar, box)
    return boxes

def run_opt():
    logging.addLevelName(1, 'VERYVERBOSE') # annoying hack bco my weird logging stuff.
    setattr(logging, 'VERYVERBOSE', 1)
    mainwin = Toplevel()
    mainwin.title('Simple PyOpt Pymol Interface')
    tabs    = ttk.Notebook(mainwin)
    opttab  = ttk.Frame(tabs)
    freqopts= ttk.Frame(tabs)
    results = ttk.Frame(tabs)
    mainopts= ttk.Frame(opttab)
    textinps= ttk.Frame(opttab)
    mainopts.pack(fill=X)
    textinps.pack(fill=X)

    tabs.add(opttab, text='Optmization')
    tabs.add(freqopts, text='Num Freqs')
    tabs.add(results, text='Results')
    tabs.pack()
    # The optimization tab - main options
    opt_base = build_selboxes((('Opt Method', ('RF', 'DIIS')),
        ('H guess', ('Diag', 'Lindh', 'Pre-Calc')),
        ('Update', ('BFGS', 'Farkas')),
        ('Coords', ('Redu', 'LNC')),
        ('Structure', load_PyMol_Objects())), col=0, root=mainopts)
    ttk.Separator(mainopts,orient='vertical').grid(column=2,rowspan=len(opt_base), row=0)
    calc_grad = IntVar()
    calc_grad.set(1)
    Checkbutton(mainopts, text='QM gradient', variable=calc_grad).grid(row=0, column=3)
    Label(mainopts, text='QM exe').grid(row=1, column=3)
    Label(mainopts, text='QM opts').grid(row=2, column=3)
    Label(mainopts, text='En match').grid(row=3, column=3)
    Label(mainopts, text='Grad match').grid(row=4, column=3)
    calc_opts = Entry(mainopts)
    calc_exe  = Entry(mainopts)
    calc_enmatch = Entry(mainopts)
    calc_gradmatch = Entry(mainopts)
    calc_exe.grid(row=1, column=4)
    calc_opts.grid(row=2, column=4)
    calc_enmatch.grid(row=3, column=4)
    calc_gradmatch.grid(row=4, column=4)
    calc_exe.insert(0, 'molpro')
    calc_opts.insert(0, '--no-xml-output -n 4')
    calc_enmatch.insert(0, '!RKS STATE 1.1 Energy')
    calc_gradmatch.insert(0, 'Atom dE/dx dE/dy dE/dz')
    # The optimization tab - input file text field
    inpfile = Text(textinps, width=40, height=10)
    inpscrl = Scrollbar(textinps)
    inpfile.pack(side=LEFT)
    inpscrl.pack(side=LEFT, fill=Y)
    inpfile.config(yscrollcommand=inpscrl.set)
    inpscrl.config(command=inpfile.yview)
    inpfile.insert(END, "memory,200,m;\ngeometry={structure}\nbasis=svp\n{{df-rks,pbe,grid=1e-7}}")
    # The input text field
    ttk.Separator(mainopts, orient='horizontal').grid(column=0,columnspan=4,row=5)
    # The results tab (matplotlib)
    plots = PlotFigure(root=results)
    optclass = Optimization(doforces=calc_grad, opts=calc_opts, prg=calc_exe, templ=inpfile, em=calc_enmatch, gm=calc_gradmatch,
                            sele=opt_base, plotfunc=plots.replot, rootwin=mainwin)
    Button(mainwin, text='Run Calc', command=optclass.start).pack(side='left', padx=10)
    Button(mainwin, text='Cancel', command=mainwin.destroy).pack(side='right', padx=10)
    # Run it
    mainwin.mainloop()

if __name__ == '__main__':
    run_opt()
