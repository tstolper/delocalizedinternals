##############################################
#                                            #
# Python Optimizer for Geometry Minimization #
#                                            #
##############################################

This is a project to test the implementation and use
of delocalized internal coordinates in geometry optimization.
Thus, the main focus is on the creation of those
coordinates, as defined by Baker et al.[1]

It uses SciPy/NumPy for the optimization algorithms
and linear algebra.

(I am trying to use a somewhat more strict 'casing' convention.
- Classes get camel cased variables with the first being capital.
- Functions are camel cased with the first character lower case.
- Lists, strings, or numbers should be all lower case.
Dunno if that's actually a decent choice :/. We'll see.)

[1] J. Baker, A. Kessi, B. Delley, J. Chem. Phys. 105, 1996, 192.
