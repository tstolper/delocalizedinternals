# -*- coding: utf-8 -*-

#
#
# This file contains some constants and properties which are needed during optimization
# and all those calculations. Those values are not really checked for sanity and were
# gathered from different source.
#
# E.g. atomic masses are from NIST.
# Covalent radii are from: Pyykkö, Atsumi, Chem. Eur. J. 2009, 15, 186--197.
# vdW radii are from: Alvarez, Dalton Trans., 2013, 42, 8617.
# Bohr radius is also from NIST: http://physics.nist.gov/cgi-bin/cuu/Value?bohrrada0|search_for=atomnuc!

from collections import namedtuple

Bohr2Ang        = 0.52917721092
Eh2cm           = 219474.6313705
Eh2kJmol        = 2625.49962
Eh2J            = 4.359744e-18
const_c         = 299792458.0
EhBohr22Jm2     = 1556.8928448170257
amu2kg          = 1.660538782E-27
ElementList     = namedtuple('ElementList', 'H He Li Be B C N O F Ne Na Mg Al Si P S Cl Ar Fe Ni Cu Ru')
ElementProp     = namedtuple('ElementProp', 'number mass cov_radius vdw_radius')
Elements        = ElementList(H = ElementProp(1, 1.00794,   32E-2/Bohr2Ang, 120E-2/Bohr2Ang),
                            He = ElementProp(2, 4.002602,   46E-2/Bohr2Ang, 143E-2/Bohr2Ang),
                            Li = ElementProp(3, 6.941,     133E-2/Bohr2Ang, 212E-2/Bohr2Ang),
                            Be = ElementProp(4, 9.012182,  102E-2/Bohr2Ang, 198E-2/Bohr2Ang),
                            B  = ElementProp(5, 10.811,     85E-2/Bohr2Ang, 191E-2/Bohr2Ang),
                            C  = ElementProp(6, 12.0107,    75E-2/Bohr2Ang, 177E-2/Bohr2Ang),
                            N  = ElementProp(7, 14.0067,    71E-2/Bohr2Ang, 166E-2/Bohr2Ang),
                            O  = ElementProp(8, 15.9994,    63E-2/Bohr2Ang, 150E-2/Bohr2Ang),
                            F  = ElementProp(9, 18.9984032, 64E-2/Bohr2Ang, 146E-2/Bohr2Ang),
                            Ne = ElementProp(10,20.1797,    67E-2/Bohr2Ang, 158E-2/Bohr2Ang),
                            Na = ElementProp(11,22.98976928,155E-2/Bohr2Ang, 250E-2/Bohr2Ang),
                            Mg = ElementProp(12,24.3050,    139E-2/Bohr2Ang, 251E-2/Bohr2Ang),
                            Al = ElementProp(13,26.9815386, 126E-2/Bohr2Ang, 225E-2/Bohr2Ang),
                            Si = ElementProp(14,28.0855,    116E-2/Bohr2Ang, 219E-2/Bohr2Ang),
                            P  = ElementProp(15,30.973762,  111E-2/Bohr2Ang, 190E-2/Bohr2Ang),
                            S  = ElementProp(16,32.065,     103E-2/Bohr2Ang, 189E-2/Bohr2Ang),
                            Cl = ElementProp(17,35.453,      99E-2/Bohr2Ang, 182E-2/Bohr2Ang),
                            Ar = ElementProp(18,39.948,      96E-2/Bohr2Ang, 183E-2/Bohr2Ang),
                            Fe = ElementProp(26,55.845,     116E-2/Bohr2Ang, 244E-2/Bohr2Ang),
                            Ni = ElementProp(28,58.6934,    110E-2/Bohr2Ang, 240E-2/Bohr2Ang),
                            Cu = ElementProp(29,63.546,     112E-2/Bohr2Ang, 238E-2/Bohr2Ang),
                            Ru = ElementProp(44,101.07,     125E-2/Bohr2Ang, 246E-2/Bohr2Ang))

def get_Period(elem = None):
       assert elem != None
       
       retval = 0
       massnum = getattr(Elements, elem).number
       if massnum <= 2:
              retval = 1
       elif massnum <= 10:
              retval = 2
       elif massnum <= 18:
              retval = 3
       elif massnum <= 36:
              retval = 4
       elif massnum <= 54:
              retval = 5
       elif massnum <= 86:
              retval = 6
       return retval
