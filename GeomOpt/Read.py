
# -*- coding: utf-8 -*-

import os
import logging
import glob, re
import scipy as np

import Storage
import Interface.Numerical as numer

from Structure import Molecule
from Interface.Molpro import MolproRun


def read_numer_EnDiff(folder = None, prog = None, order = 'GRAD'):
    assert folder != None
    assert prog != None

    # store in the right list
    if order.upper() == 'GRAD':
        path_storage    = Storage.grad_paths
        idx_storage     = Storage.grad_idx_done
        val_storage     = Storage.grad_val_done
    elif order.upper() == 'HESS':
        path_storage    = Storage.hess_paths
        idx_storage     = Storage.hess_idx_done
        val_storage     = Storage.hess_val_done
    else:
        raise ValueError('    ERROR: Sorry, we do not support reading {0}.'.format(order))

    abspath = os.path.realpath(folder)
    if os.path.isdir(abspath):
        path_storage.append(folder)
        idxs = []
        vals = []
        #calc        = MolproRun(norun = True)
        allfiles    = glob.glob(os.path.join(abspath, 'tmpcalc_*.out'))
        if order.upper() == 'GRAD':
            logging.info('*** Reading old gradient. Found following values:')
        elif order.upper() == 'HESS':
            logging.info('*** Reading old Hessian. Found following values:')
        for f in allfiles:
            fidx = int(re.search(r'\d+', os.path.basename(f)).group())
            logging.debug('-  Trying to read file {0} with index {1}'.format(f, fidx))
            if os.path.isfile(f):
                if prog.check_success(match_str = prog.en_match, opath = f):
                    #cmol    = prog.get_start_Mol(opath = f)
                    #cstruct = cmol.get_flat_Array(units = 'ANG')
                    ener    = prog.get_Energy(opath = f)
                    idxs.append(fidx)
                    vals.append(ener)
                    logging.debug('-- Calculation was successful! Energy = {0:15.8f}'.format(ener))
                else:
                    logging.debug('-- Calculation not done yet, needs to be redone!')
            else:
                logging.debug('-- Calculation file {0} does not exist.'.format(f))
        idx_storage.append(idxs)
        val_storage.append(vals)
    else:
        raise ValueError('Sorry dear, but the folder {0} does not seem to exist!'.format(abspath))

