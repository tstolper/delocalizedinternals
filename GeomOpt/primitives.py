
# -*- coding: utf-8 -*-

from operator import attrgetter
import scipy as np
import scipy.linalg as linalg
import logging

from utility_funcs import _norm_is_ok, _are_parallel, _calcAngle, _calc_Angle_of_Vects, logverbose, log_Vector, log_Vectors_stacked

tors_near_pi = np.pi / 2

class PrimitiveCoord(object):

    def __init__(self):
        self.type        = None
        self.atom_ids    = None


class Stretch(PrimitiveCoord):

    def __init__(self, atom1=None, atom2=None):
        self.type       = 'stretch'
        self.is_hbond   = False
        self.is_aux     = False
        self.hid        = None
        if atom1 < atom2:
            self.atom_ids    = (atom1, atom2)
        else:
            self.atom_ids    = (atom2, atom1)

    def get_Val(self, flatcoords = None, **kwargs):
        assert flatcoords is not None

        a1, a2  = kwargs.pop('atom_ids', self.atom_ids)
        dist    = linalg.norm(flatcoords[a2*3:a2*3+3]-flatcoords[a1*3:a1*3+3])
        return dist

    def is_same(self, atom1 = None, atom2 = None):
        retval = False
        if (atom1, atom2) == self.atom_ids or (atom2, atom1) == self.atom_ids:
            retval = True
        return retval

class Bend(PrimitiveCoord):

    def __init__(self, latom = None, catom = None, ratom = None):
        self.type       = 'bend'
        if latom < ratom:
            self.atom_ids   = (latom, catom, ratom)
        else:
            self.atom_ids   = (ratom, catom, latom)

    def get_Val(self, flatcoords = None, **kwargs):
        assert flatcoords is not None
        retval= None
        a, b, c = kwargs.pop('atom_ids', self.atom_ids)
        uvec = flatcoords[a*3:a*3+3] - flatcoords[b*3:b*3+3]
        vvec = flatcoords[c*3:c*3+3] - flatcoords[b*3:b*3+3]
        ulen, vlen = linalg.norm(uvec), linalg.norm(vvec)
        uvec, vvec = uvec/ulen, vvec/vlen
        retval = _calc_Angle_of_Vects(uvec, vvec)
        return retval

    def get_Deriv(self, flatcoords = None, **kwargs):
        assert flatcoords is not None
        retval = np.zeros((9,))
        a, b, c = kwargs.pop('atom_ids', self.atom_ids)
        uvec = flatcoords[a*3:a*3+3] - flatcoords[b*3:b*3+3]
        vvec = flatcoords[c*3:c*3+3] - flatcoords[b*3:b*3+3]
        ulen, vlen = linalg.norm(uvec), linalg.norm(vvec)
        f1 = -1.0/np.sqrt(1.0 - np.dot(uvec/ulen, vvec/vlen)**2)
        dpda = (vvec * vlen * ulen - np.dot(uvec, vvec) * uvec * vlen/ulen) / (ulen*vlen)**2
        dpdb = (-(vvec+uvec)*vlen*ulen - np.dot(uvec, vvec)*(-uvec*vlen/ulen - vvec*ulen/vlen)) / (ulen*vlen)**2
        dpdc = (uvec * vlen * ulen - np.dot(uvec, vvec) * vvec * ulen/vlen) / (ulen*vlen)**2
        retval[0:3], retval[3:6], retval[6:] = f1 * dpda, f1 * dpdb, f1 * dpdc
        return retval

class Bend_Bakken(Bend):
    testvec1 = np.array([1.0, -1.0, 1.0])
    testvec2 = np.array([-1.0, 1.0, 1.0])
    axes_fixed = False
    bisector = None
    complement = None

    def _get_axes(self, flatcoords = None):
        from numpy import cross, cos
        from numpy.linalg import norm
        a, b, c = self.atom_ids
        uvec = flatcoords[a*3:a*3+3] - flatcoords[b*3:b*3+3]
        vvec = flatcoords[c*3:c*3+3] - flatcoords[b*3:b*3+3]
        ulen, vlen = linalg.norm(uvec), linalg.norm(vvec)
        uvec, vvec = uvec/ulen, vvec/vlen
        if not _are_parallel(uvec, vvec):
            xvec = uvec + vvec
            xvec /= norm(xvec)
            wvec = np.cross(uvec, vvec)
            wvec /= norm(wvec)
            if self.type == 'LinBend' and self.linvers == 1:
                pass

    def get_Deriv(self, flatcoords = None, **kwargs):
        assert flatcoords is not None
        retval = np.zeros((9,))
        a, b, c = kwargs.pop('atom_ids', self.atom_ids)
        uvec = flatcoords[a*3:a*3+3] - flatcoords[b*3:b*3+3]
        vvec = flatcoords[c*3:c*3+3] - flatcoords[b*3:b*3+3]
        ulen, vlen = linalg.norm(uvec), linalg.norm(vvec)
        uvec, vvec = uvec/ulen, vvec/vlen
        if not _are_parallel(uvec, vvec):
            wvec = np.cross(uvec, vvec)
        else:
            if not _are_parallel(uvec, self.testvec1):
                wvec = np.cross(uvec, self.testvec1)
            else:
                wvec = np.cross(uvec, self.testvec2)
        wlen = linalg.norm(wvec)
        wvec = wvec/wlen
        uwcross = np.cross(uvec, wvec)
        wvcross = np.cross(wvec, vvec)
        retval[0:3] = uwcross/ulen
        retval[3:6] = -uwcross/ulen - wvcross/vlen
        retval[6:] = wvcross/vlen
        return retval

class LinearBend(Bend):

    def __init__(self, *args, **kwargs):
        super(LinearBend, self).__init__(*args)
        self.type = 'LinBend'
        self.linvers = kwargs.pop('vers', 0)

    def get_Val(self, flatcoords=None, **kwargs):
        """ Linear bend for which the u-vector is one basis vector.
            Only works for angles > 90° and < 270°!
            (Since we are using alpha = acos(u*w) + acos(w*v) with u,v being
            normalized bond vectors and w a normalized basis vector
            perpendicular to u. I just assumed acos(u*w) to always be 90°.
            Which it is, but if acos(u*v) < 90° the addition would be wrong.)
        """
        a, b, c = kwargs.pop('atom_ids', self.atom_ids)
        uvec = flatcoords[a*3:a*3+3] - flatcoords[b*3:b*3+3]
        vvec = flatcoords[c*3:c*3+3] - flatcoords[b*3:b*3+3]
        ulen = linalg.norm(uvec)
        vlen = linalg.norm(vvec)
        if _are_parallel(uvec, np.array([0.0, 1.0, 0.0])):
            axis2 = np.cross(uvec, np.array([0.0, 0.0, 1.0]))
        else:
            axis2 = np.cross(uvec, np.array([0.0, 1.0, 0.0]))
        axis3 = np.cross(uvec, axis2)
        axis2len = linalg.norm(axis2)
        axis3len = linalg.norm(axis3)
        if self.linvers > 0:
            retval = np.pi/2.0 + np.arccos(np.dot(axis2, vvec)/(axis2len*vlen))
        else:
            retval = np.pi/2.0 + np.arccos(np.dot(axis3, vvec)/(axis3len*vlen))
        return retval

    def get_Deriv(self, flatcoords=None, **kwargs):
        retval = np.zeros((9,))
        a, b, c = kwargs.pop('atom_ids', self.atom_ids)
        uvec = flatcoords[a*3:a*3+3] - flatcoords[b*3:b*3+3]
        vvec = flatcoords[c*3:c*3+3] - flatcoords[b*3:b*3+3]
        tvec = np.array([0.0, 1.0, 0.0])
        if _are_parallel(uvec, tvec): # w0
            tvec  = np.array([0.0, 0.0, 1.0])
            axis2 = np.cross(uvec, tvec)
            mvec  = np.array([1.0, 1.0, 0.0])
            daxis3da = np.array([[2, -1, 0], [-1, 2, 1], [0, 1, -1]])
            daxis3da_mult = np.array([[1.0, 0.0, -2.0], [0.0, 1.0, -2.0], [1.0, 1.0, 0.0]])
            daxis3lenda_mult = np.array([[2.0, 2.0, 1.0], [2.0, 2.0, 2.0], [1.0, 1.0, 0.0]])
        else: # w1
            axis2 = np.cross(uvec, tvec)
            mvec  = np.array([1.0, 0.0, 1.0])
            # matrix which contains the ids of the u-vector to build the derivative
            # of the third new axis. -1 is just a placeholder which is later changed
            # to a 0 value by multiplying with daxis3da_mult.
            daxis3da = np.array([[1, 0, -1], [0, -1, 2], [-1, 2, 1]])
            daxis3da_mult = np.array([[1.0, -2.0, 0.0], [1.0, 0.0, 1.0], [0.0, -2.0, 1.0]])
            daxis3lenda_mult = np.array([[2.0, 1.0, 2.0], [1.0, 0.0, 1.0], [2.0, 1.0, 2.0]])
        axis3 = np.cross(uvec, axis2)
        ulen, vlen = linalg.norm(uvec), linalg.norm(vvec)
        axis2len, axis3len = linalg.norm(axis2), linalg.norm(axis3)
        if self.linvers > 0:
            f1 = -1.0/np.sqrt(1.0-(np.dot(axis2, vvec)/(axis2len*vlen)))
            vwlen2 = (vlen*axis2len)**2
            retval[:3] = f1 * (np.cross(tvec, vvec)*vlen*axis2len - np.dot(axis2, vvec)*vlen*np.multiply(uvec, mvec)/axis2len)/vwlen2
            retval[3:6] = f1 * (axis2*vlen*axis2len - np.dot(axis2, vvec)*axis2len*vvec/vlen)/vwlen2
            retval[6:] = f1 * (np.cross(tvec, uvec-vvec)*axis2len*vlen + np.dot(axis2, vvec)*(vlen*np.multiply(uvec, mvec)/axis2len + axis2len*vvec/vlen))/vwlen2
        else:
            f1 = -1.0/np.sqrt(1.0-(np.dot(axis3, vvec)/(axis3len*vlen)))
            dvdc    = np.identity(3)    # dv/dc_i   = e_i
            dvlendc = vvec/vlen         # d|v|/dc_i = (c_i-b_i)/|v|
            vzlen2  = (vlen*axis3len)**2
            daxis3lenda = np.multiply(uvec, np.dot(daxis3lenda_mult, np.multiply(uvec, uvec)))/axis3len # dz/da = u_i*[m*u²_1 + n*u²_2 + o*u²_3]/|z|
            retval[0] = f1 * (np.dot(vvec, np.multiply(uvec[daxis3da[0]], daxis3da_mult[0]))*vlen*axis3len
                                - vlen*daxis3lenda[0]*np.dot(vvec, axis3))/vzlen2
            retval[1] = f1 * (np.dot(vvec, np.multiply(uvec[daxis3da[1]], daxis3da_mult[1]))*vlen*axis3len
                                - vlen*daxis3lenda[1]*np.dot(vvec, axis3))/vzlen2
            retval[2] = f1 * (np.dot(vvec, np.multiply(uvec[daxis3da[2]], daxis3da_mult[2]))*vlen*axis3len
                                - vlen*daxis3lenda[2]*np.dot(vvec, axis3))/vzlen2
            retval[3] = f1 * ((np.dot(vvec, -np.multiply(uvec[daxis3da[0]], daxis3da_mult[0])) - axis3[0])*vlen*axis3len
                                - (vlen*(-daxis3lenda[0]) + axis3len*(-dvlendc[0]))*np.dot(vvec, axis3))/vzlen2
            retval[4] = f1 * ((np.dot(vvec, -np.multiply(uvec[daxis3da[1]], daxis3da_mult[1])) - axis3[1])*vlen*axis3len
                                - (vlen*(-daxis3lenda[1]) + axis3len*(-dvlendc[1]))*np.dot(vvec, axis3))/vzlen2
            retval[3] = f1 * ((np.dot(vvec, -np.multiply(uvec[daxis3da[2]], daxis3da_mult[2])) - axis3[2])*vlen*axis3len
                                - (vlen*(-daxis3lenda[2]) + axis3len*(-dvlendc[2]))*np.dot(vvec, axis3))/vzlen2
            retval[6:]  = f1 * (np.dot(dvdc, axis3)*vlen*axis3len - axis3len*dvlendc*np.dot(vvec, axis3))/vzlen2
        return retval

class LinBendCart(Bend):

    def __init__(self, *args, **kwargs):
        super(LinBendCart, self).__init__(*args)
        self.type = 'LinBendCart'
        self.linvers = kwargs.pop('vers', 0)
        self.cids = None

    def _get_Cart_ID(self, flatcoords=None, **kwargs):
        from operator import itemgetter
        assert flatcoords is not None
        # choose the cartesian directions of b most orthogonal
        # to the line connecting atom a and c.
        a, b, c = kwargs.pop('atom_ids', self.atom_ids)
        uvec = flatcoords[a*3:a*3+3] - flatcoords[b*3:b*3+3]
        vvec = flatcoords[c*3:c*3+3] - flatcoords[b*3:b*3+3]
        ulen = linalg.norm(uvec)
        vlen = linalg.norm(vvec)
        acvec= uvec-vvec
        aclen= linalg.norm(acvec)
        maxorth = sorted(enumerate(np.absolute(acvec/aclen)), key=itemgetter(1))
        log_Vector('   acvec: ', acvec)
        return [ i for i, v in maxorth[:2] ]

    def get_Val(self, flatcoords=None, **kwargs):
        assert flatcoords is not None
        retval = None
        b = kwargs.pop('atom_ids', self.atom_ids)[1]
        ccids = self._get_Cart_ID(flatcoords)
        if self.cids is None:
            self.cids = ccids
        elif self.cids[0] not in ccids or self.cids[1] not in ccids:
            logging.debug('WARNING: Preferred cartesian changes! That is inconsistant! ccids='+repr(ccids)+', cids='+repr(self.cids))
        if self.linvers < 1:
            retval = flatcoords[b*3+self.cids[0]]
        else:
            retval = flatcoords[b*3+self.cids[1]]
        return retval

    def get_Deriv(self, flatcoords=None, **kwargs):
        self.get_Val(flatcoords)
        retval = np.zeros((9,))
        if self.linvers < 1:
            retval[3+self.cids[0]] = 1.0
        else:
            retval[3+self.cids[1]] = 1.0
        return retval

class Twist(PrimitiveCoord):

    def __init__(self, matom = None, oatom = None, patom = None, natom = None):
        self.type           = 'twist'
        self.is_near_180    = False
        self.state          = 0
        if matom < natom:
            self.atom_ids   = (matom, oatom, patom, natom)
        else:
            self.atom_ids   = (natom, patom, oatom, matom)

    def is_same(self, matom = None, oatom = None, patom = None, natom = None):
        retval = False
        if self.atom_ids == (matom, oatom, patom, natom) or self.atom_ids == (natom, patom, oatom, matom):
            retval = True
        return retval

    def get_Val(self, flatcoords = None, **kwargs):
        assert flatcoords is not None
        matom, oatom, patom, natom = kwargs.pop('atom_ids', self.atom_ids)
        uvec    = flatcoords[oatom*3:oatom*3+3] - flatcoords[matom*3:matom*3+3] # b1
        wvec    = flatcoords[patom*3:patom*3+3] - flatcoords[oatom*3:oatom*3+3] # b2
        vvec    = flatcoords[natom*3:natom*3+3] - flatcoords[patom*3:patom*3+3] # b3
        ulen    = linalg.norm(uvec)
        wlen    = linalg.norm(wvec)
        vlen    = linalg.norm(vvec)
        # check norms of the vectors
        if not _norm_is_ok(uvec, wvec, vvec):
            raise ValueError('A norm for dihedral {0} is either too large or small: ulen = {1}, wlen = {2}, vlen = {3}'\
                             .format(repr((matom, oatom, patom, natom)), ulen, wlen, vlen))
        ucross  = np.cross(uvec/ulen, wvec/wlen) # n1
        vcross  = np.cross(wvec/wlen, vvec/vlen) # n2
        phiu_sin = linalg.norm(ucross)
        phiv_sin = linalg.norm(vcross)
        dihed_rad = np.dot(ucross, vcross)/(phiu_sin * phiv_sin)
        if dihed_rad >= 1.0 - 1.0e-10:
          dihed_rad = 0.0
        elif dihed_rad <= -1.0 + 1.0e-10:
          dihed_rad = np.pi
        else:
          dihed_rad = np.arccos(dihed_rad)
        # adjust the sign
        if np.dot(uvec/ulen, vcross) < 0: dihed_rad = -dihed_rad
        # always fix if state != 0
        if self.state == -1 and dihed_rad > tors_near_pi:
            dihed_rad -= 2.0*np.pi
        elif self.state == 1 and dihed_rad < -tors_near_pi:
            dihed_rad += 2.0*np.pi
        return dihed_rad

class Fragment(object):

    def __init__(self, atoms = None, stretches = None, *args, **kwargs):
        """ Initializes the lists it contains with supplied variables. Does not check if it's the right type!
        Expects lists.
        """
        if atoms != None:
            self.list_of_atoms = atoms
        else:
            self.list_of_atoms = []
        if stretches != None:
            self.list_of_stretches = stretches
        else:
            self.list_of_stretches = []

class Internals(object):

    def __init__(self, has_extrared_bonds = True):
        self.list_of_fragments   = []
        self.list_of_noncovstr   = [] # IF/aux IF bonds, H-bonds
        self.list_of_extrared    = None
        # maybe one could also divide the bends and twists into intra- and intermolecular
        self.list_of_bends       = []
        self.list_of_twists      = []
        self.has_extrared        = has_extrared_bonds
        self.has_linearbend      = False
        self.coord_count         = 0

    def add_Stretch(self, ids=None, frag_id=0):
        fragms = self.list_of_fragments
        if len(fragms) == 0:
            fragms.append(Fragment())
        assert frag_id < len(fragms)
        fragms[frag_id].list_of_stretches.append(Stretch(*ids))
        if ids[0] not in fragms[frag_id].list_of_atoms: fragms[frag_id].list_of_atoms.append(ids[0])
        if ids[1] not in fragms[frag_id].list_of_atoms: fragms[frag_id].list_of_atoms.append(ids[1])

    def add_IF_Stretch(self, stretch_list = None):
        if self.list_of_noncovstr == None:
            self.list_of_noncovstr = []
        self.list_of_noncovstr.extend(stretch_list)

    def add_HB_Stretch(self, stretch_list = None, hid_list = None):
        assert stretch_list != None
        assert hid_list != None
        startidx = len(self.list_of_noncovstr)
        self.add_IF_stretch(stretch_list)
        # make it an H-bond
        for hb in range(len(stretch_list)):
            self.list_of_noncovstr[hb+startidx].is_hbond = True
            self.list_of_noncovstr[hb+startidx].hid      = hid_list[hb]

    def add_extra_Stretch(self, stretch_list = None):
        if not self.has_extrared:
            self.has_extrared = True
        if self.list_of_extrared == None:
            self.list_of_extrared = []
        self.list_of_extrared.extend(stretch_list)

    def _delete_Coord(self, ids=None, prim_list=None):
        assert ids is not None
        assert prim_list is not None
        to_remove = []
        for i, c in enumerate(prim_list):
            correct_ids = [(atmid in c.atom_ids) for atmid in ids]
            if False not in correct_ids:
                to_remove.append(i)
        for cid in reversed(to_remove.sort()):
            self.list_of_bends.pop(cid)
        self.coord_count -= len(to_remove)
        return len(to_remove)

    def add_Bend(self, btype='normal', ids=None):
        assert ids is not None
        assert len(ids) == 3
        if btype == 'normal':
            self.list_of_bends.append(Bend(*ids))
            self.coord_count += 1
        elif btype == 'linbendset':
            self.list_of_bends.append(LinBend(*ids, vers=0))
            self.list_of_bends.append(LinBend(*ids, vers=1))
            self.coord_count += 2
        elif btype == 'linbendsetcart':
            self.list_of_bends.append(LinBendCart(*ids, vers=0))
            self.list_of_bends.append(LinBendCart(*ids, vers=1))
            self.coord_count += 2

    def delete_Bend(self, ids=None):
        """ Deletes bends from the list of internal coordinates.
        Expects tuples or any other iterable. If that iterable
        contains less ids than 3 (needed for a bend), all bends
        are deleted containing the given atom ids.

        Returns: The number of removed bends.
        """
        return self._delete_Coord(ids=ids, prim_list=self.list_of_bends)

    def add_Torsion(self, ids=None):
        assert ids is not None
        assert len(ids) == 4
        self.list_of_twists.append(Twist(*ids))
        self.coord_count += 1

    def delete_Torsion(self, ids=None):
        """ Deletes Torsions. Works the same as delete_Bend.
        """
        return self._delete_Coord(ids=ids, prim_list=self.list_of_twists)

    def get_Fragment_Atoms(self, fragment_id = None):
        result = None
        if fragment_id == None:
            result = []
            for frag in self.list_of_fragments:
                result.append(frag.list_of_atoms)
        else:
            result = self.list_of_fragments[fragment_id].list_of_atoms
        return result

    def contains_Stretch(self, atom1 = None, atom2 = None, only = None):
        assert atom1 != None
        assert atom2 != None
        isincoord   = False
        coordref    = None
        tmpstretches = self.get_Stretches(only = only)
        for s in tmpstretches:
            if s.is_same(atom1, atom2):
                isincoord   = True
                coordref    = s
                break
        return (isincoord, coordref)

    def get_Stretches(self, atom_id=None, only=None):
        tmplist = []
        if only == None or only in ('COV', 'REGULAR'):
            for fragm in self.list_of_fragments:
                if atom_id is None:
                    tmplist.extend(fragm.list_of_stretches)
                else:
                    tmplist.extend([ tstretch for tstretch in fragm.list_of_stretches if atom_id in tstretch.atom_ids ])
        if only == None or only == 'NONCOV':
            if atom_id is None:
                tmplist.extend(self.list_of_noncovstr)
            else:
                tmplist.extend([ tstretch for tstretch in self.list_of_noncovstr if atom_id is None or atom_id in tstretch.atom_ids ])
        elif only == 'REGULAR':
            if atom_id is None:
                tmplist.extend([ __ for __ in self.list_of_noncovstr if not __.is_aux ])
            else:
                tmplist.extend([ __ for __ in self.list_of_noncovstr if not __.is_aux and atom_id in __.atom_ids])
        if (only == None or only == 'EXTRA') and self.list_of_extrared is not None:
            if atom_id is None:
                tmplist.extend(self.list_of_extrared)
            else:
                tmplist.extend([ tstretch for tstretch in self.list_of_extrared if atom_id is None or atom_id in tstretch.atom_ids ])
        return sorted(tmplist, key = attrgetter('atom_ids'))

    def contains_Bend(self, latom = None, catom = None, ratom = None):
        """ A-B-C and C-B-A are the same, but other permutations not."""
        isincoord   = False
        coordref    = None
        if self.list_of_bends != None:
            for bend in self.list_of_bends:
                if bend.atom_ids == (latom, catom, ratom) or bend.atom_ids == (ratom, catom, latom):
                    if not bend.is_pseudo:
                        isincoord   = True
                        coordref    = bend
                        break
        return (isincoord, coordref)

    def get_Bends(self, only = None):
        tmplist = []
        if only != None and only == 'NOPSEUDO':
            tmplist.extend([ __ for __ in self.list_of_bends if not __.is_pseudo ])
        else:
            tmplist.extend(self.list_of_bends)
        return tmplist

    def contains_Twist(self, matom = None, oatom = None, patom = None, natom = None):
        isincoord   = False
        coordref    = None
        for twist in self.list_of_twists:
            if twist.is_same(matom, oatom, patom, natom):
                isincoord   = True
                coordref    = twist
                break
        return (isincoord, coordref)

    def get_Twists(self, only = None):
        tmplist = []
        tmplist.extend(self.list_of_twists)
        return tmplist

    def fix_tors_180(self, flatcoords = None):
        assert flatcoords is not None
        for tors in self.list_of_twists:
            tval = tors.get_Val(flatcoords)
            if tval > tors_near_pi:
                tors.state = 1
            elif tval < -tors_near_pi:
                tors.state = -1
            else:
                tors.state = 0

    def get_TwistStates(self):
        ret = {}
        for tors in self.list_of_twists:
            ret[tors.atom_ids] = tors.state
        return ret

    def set_TwistStates(self, states=None):
        assert states is not None
        logging.debug('    Setting states for twists.')
        for tors in self.list_of_twists:
            if tors.atom_ids in states:
                logging.debug('      Setting state {0:2d} for twist {1}'.format(states[tors.atom_ids], repr(tors.atom_ids)))
                tors.state = states[tors.atom_ids]
        return None

    # def fix_bend_ref(self, flatcoords = None, coords_int_start = None, coords_int_final = None):
    #     """ Fix the "reference frame" for the bend, i.e. choose a vector w which is not parallel to any
    #         bond vector and from which the angle is derived as follows: a = arccos(u*w) + arccos(v*w)
    #         This hopefully is enough to make the back-transformation stable for near-linear structures.
    #         The w-vector is chosen as follows:
    #         1.) If any of the starting or final structure is non-near-linear use that structures
    #             bisecting vector for w. (Should be valid also for linear structures, except if cos(w*v) = 0.)
    #         2.) Otherwise just find a perpendicular vector.
    #     """
    #     assert flatcoords is not None
    #     assert coords_int_start is not None
    #     coord_list = self.get_Coords()
    #     assert len(coords_int_start) == len(coord_list)
    #     if coords_int_final is not None:
    #         assert len(coords_int_start) == len(coords_int_final)
    #         for cis, cif, coord in zip(coords_int_start, coords_int_final, coord_list):
    #             # choose the smaller angle of the set (start, final).
    #             if np.absolute(cis-np.pi) > 0.087266: # ~5° from 180°
    #                 _a, _b, _c = coord.atom_ids
    #                 wvec = 0.5*(flatcoords[_a*3:_a*3+3] + flatcoords[_c*3:_c*3+3]) - flatcoords[_b*3:_b*3+3]
    #             elif np.absolute(cif-np.pi) > 0.087266:

    #             (__ids, __acos, __arad), (lvec, rvec, ldist, rdist) = _calcAngle(flatcoords, *b.atom_ids)
    #             if __arad  > np.pi - 2.0e-2: # nearly parallel
    #                 lvec /= ldist
    #                 rvec /= rdist
    #                 wvec = np.cross(lvec, rvec)
    #                 b.ref = wvec / linalg.norm(wvec)
    #             else:
    #                 b.ref = None

    def get_Coord_Vals(self, flatcoords = None):
        cintcoords  = np.zeros((self.coord_count,))
        ccoord      = 0
        tmpcoords   = self.get_Coords()
        for c in tmpcoords:
            cintcoords[ccoord] = c.get_Val(flatcoords)
            ccoord += 1
        if ccoord != self.coord_count:
            raise ValueError('Number of coord count and real coordinates doesn\'t match!\
                              coord_count = {0}, ccoord = {1}'.format(self.coord_count, ccoord))
        return cintcoords

    def get_Coord_Idxs(self):
        tmpidxs     = []
        tmpcoords   = self.get_Coords()
        for c in tmpcoords:
            if c.type == 'bend' and c.is_pseudo:
                    tmp1, tmp2, tmp3 = c.atom_ids
                    tmpidxs.append((-tmp1, -tmp2, -tmp3))
            else:
                tmpidxs.append(c.atom_ids)
        return tmpidxs

    def get_Coords(self, only = None):
        tmplist = []
        tmplist.extend(sorted(self.get_Stretches(), key = attrgetter('atom_ids')))
        tmplist.extend(self.get_Bends())
        tmplist.extend(self.get_Twists())
        return tmplist

    def sort_Coords(self):
        for frag in self.list_of_fragments:
            frag.list_of_stretches.sort(key = attrgetter('atom_ids'))
            frag.list_of_atoms.sort()
        self.list_of_noncovstr.sort(key = attrgetter('atom_ids'))
        self.list_of_bends.sort(key = attrgetter('atom_ids'))
        self.list_of_twists.sort(key = attrgetter('atom_ids'))
        if self.has_extrared:
            self.list_of_extrared.sort(key = attrgetter('atom_ids'))

    def print_Coords(self, func = logging.debug, flatcoords = None):
        assert flatcoords is not None
        for idx, c in enumerate(self.get_Coords()):
            if c.type == 'stretch':
                prefix  = 'R'
                props   = 'aux = {0}, hbond = {1}'.format(c.is_aux, c.is_hbond)
            elif c.type == 'bend':
                prefix  = 'B'
                props   = ''
            elif c.type == 'LinBend':
                prefix  = 'L'
                props   = 'vers = {:2d}'.format(c.linvers)
            elif c.type == 'LinBendCart':
                prefix  = 'L'
                props   = 'vers={:1d}, cart=True'.format(c.linvers)
            elif c.type == 'twist':
                prefix  = 'D'
                props   = 'near_180 = {0}'.format(c.is_near_180)
            func('{0:5d} {1}{2:20s} {3} value={4}'.format(idx, prefix, repr(c.atom_ids), props, c.get_Val(flatcoords)))
