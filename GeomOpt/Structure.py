# --* encoding: utf-8 *--

import re, os, sys
import scipy as np
import copy

from atomprops import Bohr2Ang, Elements

class Atom(object):
    def __init__(self, elem = None, values = None, group = None, iscart = True):
        self.element    = elem.capitalize()
        self.group      = group
        if iscart:
            self.X = float(values[0])
            self.Y = float(values[1])
            self.Z = float(values[2])
        else: # zmat. Expects tuples of (atomid, coord_val) as a list.
            self.zcoords = values

class Molecule(object):
    
    def __init__(self, copyfrom = None, **kwargs):
        self.atoms  = []
        self.natoms = None
        self.format = None
        self.comment = None
        self.units  = None
        print type(copyfrom), type(Molecule), type(copyfrom) == Molecule
        if copyfrom is not None:
            self.atoms  = copy.deepcopy(copyfrom.atoms)
            self.natoms = copyfrom.natoms
            self.format = copyfrom.format
            self.comment = copyfrom.comment
            self.units  = copyfrom.units

        if 'from_coords' in kwargs:
            pass

    def load_File(self, fpath = None, stype = None):
        assert fpath != None

        if stype == None: # determine from extension
            ext = fpath.split('.')[-1].upper()
            if ext == 'XYZ':
                self.format = 'XYZ'
            elif ext == 'ZMAT':
                self.format = 'ZMAT'
            else:
                raise Exception('File type not recognized! '+ext)
        else:
            self.format = stype

        if self.format == 'XYZ': # assume Ángstroms
            with open(fpath, 'r') as inpfile:
                next(inpfile)
                self.comment = next(inpfile).strip()
                for line in inpfile:
                    try:
                        fields      = line.split()
                        elem, idx   = re.findall(r'(\D+)(\d*)', fields[0])[0]
                        if idx != '':
                            idx     = int(idx)
                        else:
                            idx     = 0
                        vals        = fields[1:]
                        self.atoms.append(Atom(elem = elem, values = vals, group = idx, iscart = True))
                    except Exception, e:
                        print >> sys.stderr, 'ERROR: Could not parse structure input line: {0}'.format(line)
                        raise e
                inpfile.close()
                self.natoms = len(self.atoms)
                self.units  = 'ANG'
        elif self.format == 'ZMAT':
            raise NotImplementedError('No ZMAT yet, sorry.')

    def load_from_String(self, sstring = None, stype = None):
        assert sstring is not None
        assert stype is not None

        self.format = stype.upper()
        if self.format == 'XYZ':
            lines           = sstring.split('\n')
            self.comment    = lines[1]
            for line in lines[2:]:
                try:
                    fields      = line.split()
                    elem, idx   = re.findall(r'(\D+)(\d*)', fields[0])[0]
                    if idx != '':
                        idx     = int(idx)
                    else:
                        idx     = 0
                    vals        = fields[1:]
                    self.atoms.append(Atom(elem = elem, values = vals, group = idx, iscart = True))
                except Exception, e:
                    print >> sys.stderr, 'ERROR: Could not parse structure input line: {0}'.format(line)
                    raise e
            self.natoms = len(self.atoms)
            self.units  = 'ANG'
        elif self.format == 'ZMAT':
            raise NotImplementedError('No ZMAT yet, sorry.')

    def write_Struct(self, outfile = None, wtype = 'XYZ', units = 'ANG'):
        assert self.format is not None
        assert outfile is not None

        outstr = self.get_String(wtype=wtype, units=units, nogroup=True, noheader=False)
        with open(outfile, 'w') as ofile:
            ofile.write(outstr)
            ofile.close()

    def get_flat_Array(self, units = 'ANG'):
        retmat = None
        if self.format == 'XYZ':
            retmat = np.zeros((self.natoms*3,))
            for idx, atom in enumerate(self.atoms):
                if units == self.units:
                    retmat[idx*3]   = atom.X
                    retmat[idx*3+1] = atom.Y
                    retmat[idx*3+2] = atom.Z
                elif units == 'BOHR' and self.units == 'ANG':
                    retmat[idx*3]   = atom.X / Bohr2Ang
                    retmat[idx*3+1] = atom.Y / Bohr2Ang
                    retmat[idx*3+2] = atom.Z / Bohr2Ang
                elif units == 'ANG' and self.units == 'BOHR':
                    retmat[idx*3]   = atom.X * Bohr2Ang
                    retmat[idx*3+1] = atom.Y * Bohr2Ang
                    retmat[idx*3+2] = atom.Z * Bohr2Ang
                else:
                    raise NotImplementedError('Conversion from {0} to {1} not supported!'.format(self.units, units))
        elif self.format == 'ZMAT':
            raise NotImplementedError('As the error says.')
        return retmat

    def get_String(self, wtype = 'XYZ', units = 'ANG', nogroup = True, noheader=True):
        retstr = ''
        if self.format == 'XYZ':
            if wtype.upper() == 'XYZ':
                if not noheader:
                    retstr += '{0:d}\n'.format(self.natoms)
                    retstr += '  {0:s}\n'.format(self.comment)
                for atom in self.atoms:
                    if units == self.units:
                        x, y, z = atom.X, atom.Y, atom.Z
                    elif units == 'BOHR' and self.units == 'ANG':
                        x, y, z = atom.X/Bohr2Ang, atom.Y/Bohr2Ang, atom.Z/Bohr2Ang
                    elif units == 'ANG' and self.units == 'BOHR':
                        x, y, z = atom.X*Bohr2Ang, atom.Y*Bohr2Ang, atom.Z*Bohr2Ang
                    else:
                        raise NotImplementedError('Conversion from {0} to {1} not supported!'.format(self.units, units))
                    if nogroup:
                        retstr += '{:s} {:12.8f} {:12.8f} {:12.8f}\n'.format(atom.element, x.real, y.real, z.real)
                    else:
                        retstr += '{:s}{:<3d} {:12.8f} {:12.8f} {:12.8f}\n'.format(atom.element, atom.group, x.real, y.real, z.real)
            elif wtype.upper() == 'ZMAT':
                raise NotImplementedError('No conversion possible yet.')
        elif self.format == 'ZMAT':
            raise NotImplementedError('No ZMAT yet.')
        return retstr

    def get_Elem_List(self):
        retlist = []
        for a in self.atoms:
            retlist.append(a.element)
        return retlist

    def get_Mass(self):
        mass = 0.0
        for a in self.atoms:
            mass += getattr(Elements, a.elem).mass
        return mass

    def get_MassCenter(self):
        center = np.zeros((3,))
        for a in self.atoms:
            center[0] += a.X
            center[1] += a.Y
            center[2] += a.Z
        center /= self.get_Mass()
        return center

    def move(self, displ_vec = None, nosave = False):
        result = np.zeros((self.natoms*3,))
        for idx, a in enumerate(self.atoms):
            result[idx*3] = a.X
            result[idx*3+1] = a.Y
            result[idx*3+2] = a.Z
            if not nosave:
                a.X += displ_vec[0]
                a.Y += displ_vec[1]
                a.Z += displ_vec[2]
        return result

    def rotate(self, final_arr = None, nosave = False): pass

    def set_coords(self, coords = None, type = 'XYZ', units = 'ANG'):
        """ Sets the coordinates of the molecule. Useful if the structure was copied to create an already displaced structure.
        Requires an (n, 3) ndarray. Only handles XYZ for now.
        """
        assert coords is not None
        assert len(coords.shape) == 2
        assert coords.shape[1] == 3
        assert coords.shape[0] == self.natoms, repr((coords.shape, self.natoms))

        for idx, xyz in enumerate(coords):
            if units == self.units:
                self.atoms[idx].X = xyz[0]
                self.atoms[idx].Y = xyz[1]
                self.atoms[idx].Z = xyz[2]
            elif units == 'ANG' and self.units == 'BOHR':
                self.atoms[idx].X = xyz[0] / Bohr2Ang
                self.atoms[idx].Y = xyz[1] / Bohr2Ang
                self.atoms[idx].Z = xyz[2] / Bohr2Ang
            elif units == 'BOHR' and self.units == 'ANG':
                self.atoms[idx].X = xyz[0] * Bohr2Ang
                self.atoms[idx].Y = xyz[1] * Bohr2Ang
                self.atoms[idx].Z = xyz[2] * Bohr2Ang
            else:
                raise NotImplementedError('Conversion from {0} to {1} not supported!'.format(self.units, units))
