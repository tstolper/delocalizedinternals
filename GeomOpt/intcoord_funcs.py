
# --* coding: utf-8 *--
############################################
#
# Convenience module for the coordinate
# classes. Contains functions to do
# tasks like determining the bonds,
# fragments, angles and torsions.
# But also things like calculating
# the Wilson B matrix should be done
# here. Actually everything that
# doesn't need to be kept in classes.
#
# There are the getBondIDs, getAngleIDs and
# getTorsIDs functions which determine
# the whole set of internal coordinates.
#
# To just update the values use the
# recalcXXX functions.
#
# For flexibility I kept the calculation
# of the derivatives separately, e.g.
# calcBmat().
############################################

from __future__ import division

import sys
import logging
import time
import scipy as np
import scipy.linalg as linalg

from atomprops import Elements
from atomprops import Bohr2Ang
from primitives import Stretch, Bend, Bend_Bakken, LinearBend, LinBendCart, Twist, Fragment, Internals
from utility_funcs import _are_parallel, _norm_is_ok, _calcAngle, _calcDihedral, _calc_Angle_of_Vects, dij, zijk, log_Matrix, log_Vector, logverbose

acceptor_elements   = ('N', 'O', 'F', 'P', 'S', 'Cl')
donor_elements      = acceptor_elements

def get_total_Connection(flatcoords = None, atomsymbols = None):
    """ Mainly introduced for debugging, this is a simple primitive coordinate set.
    It just consists of all bonds possible."""
    assert len(flatcoords) == 3*len(atomsymbols)

    coords= Internals(has_extrared_bonds=False)
    bonds= []

    for idx1, elem1 in enumerate(atomsymbols):
        for idx2 in range(idx1+1,len(atomsymbols)):
            bonds.append(Stretch(idx1, idx2))
    coords.list_of_fragments.append(Fragment(atoms=range(len(atomsymbols)), stretches=bonds))
    coords.coord_count= len(bonds)
    return coords

def _getFragBonds(flatcoords = None, atomsymbols = None, atoms_done = None, atom_id = 0, getextrared = True, covbond_fact = 1.3,\
                hbond_fact = 0.9, auxinter_ang = 2.0, auxinter_fact = 1.3, auxbond_fact = 2.5):
    """
    This method starts at a certain atom and recursively traverses the bonds until it has found every
    atom in the fragment.

    fragatoms:      List of atom ids.
    bonds:          List of Stretch classes (primitives.py).
    auxbonds:       Same as bonds, just for the auxiliary bonds.
    """
    assert len(flatcoords) == 3*len(atomsymbols)
    assert atoms_done != None

    fragatoms   = []
    bonds       = [] # Nested list containing lists corresponding to the fragments
    auxbonds    = []

    # --- Regular bonds ---
    next_ids = [] # List of bonding partners
    for idx1, elem1 in enumerate(atomsymbols):
        if idx1 != atom_id and idx1 not in atoms_done:
            atm_dist    = linalg.norm(flatcoords[idx1*3 : idx1*3+3] - flatcoords[atom_id*3 : atom_id*3+3])
            cov_sum     = getattr(Elements, atomsymbols[idx1]).cov_radius + getattr(Elements, atomsymbols[atom_id]).cov_radius
            if atm_dist < covbond_fact*cov_sum:
                bonds.append(Stretch(atom_id, idx1))
                next_ids.append(idx1)
                if atomsymbols[atom_id] == 'H':
                    bonds[-1].hid = atom_id
                elif atomsymbols[idx1] == 'H':
                    bonds[-1].hid = idx1
            elif getextrared and atm_dist < auxbond_fact*cov_sum:
                logging.debug('Found extra redundant bond. ids = {0},{1} dist = {2} thresh = {3}'.format(atom_id, idx1, atm_dist, auxbond_fact*cov_sum))
                auxbonds.append(Stretch(atom_id, idx1))
                auxbonds[-1].is_aux = True
                if atomsymbols[atom_id] == 'H':
                    auxbonds[-1].hid = atom_id
                elif atomsymbols[idx1] == 'H':
                    auxbonds[-1].hid = idx1
    atoms_done.append(atom_id)
    fragatoms.append(atom_id)
    # now call this function for every bonding partner (bp)
    for bp in next_ids:
        logging.debug('Length of atoms_done = {0} start_atom = {1}'.format(len(atoms_done), bp))
        if bp not in atoms_done:
            bp_atoms, bp_bonds, bp_aux = _getFragBonds(flatcoords, atomsymbols, atoms_done, bp, getextrared, covbond_fact,
                                                      hbond_fact, auxinter_ang, auxinter_fact, auxbond_fact)
            fragatoms.extend(bp_atoms)
            bonds.extend(bp_bonds)
            auxbonds.extend(bp_aux)
    return fragatoms, bonds, auxbonds

def getBondIDs(flatcoords = None, atomsymbols = None, getextrared = True, covbond_fact = 1.3,
               hbond_fact = 0.9, auxinter_ang = 2.0, auxinter_fact = 1.3, auxbond_fact = 2.5, *args, **kwargs):
    """ getBondIDs() does just what it says. Well, actually even more, since it also calculates the distance
    while it is at it and saves it in a dictionary.

    The atom ids forming the bond are returned as tuples '(atom1, atom2)', where the bond vector is taken as
    pos(atom2) - pos(atom1).

    Returns:

    fragments           List of lists containing atom ids which correspond to one fragment.
    fragbonds           List of 'Stretch' classes. Contains all bonds except auxiliary.
    auxbonds            List of 'Stretch' classes, but just the auxiliary bonds.
    """
    assert len(flatcoords) == 3*len(atomsymbols)

    atoms_done  = []
    atom_num    = len(atomsymbols)
    atom_set    = set(range(atom_num))

    coords      = Internals(has_extrared_bonds = getextrared)
    tmp_extra   = []

    while len(atoms_done) < atom_num:
        start_atom = (atom_set-set(atoms_done)).pop()
        f_atoms, f_bonds, f_extra = _getFragBonds(flatcoords, atomsymbols, atoms_done, start_atom, getextrared, covbond_fact,
                                                        hbond_fact, auxinter_ang, auxinter_fact, auxbond_fact)
        coords.list_of_fragments.append(Fragment(atoms = f_atoms, stretches = f_bonds))
        tmp_extra.extend(f_extra)
        coords.coord_count += len(f_bonds)# + len(f_extra)
    # --- Determining Interfragment Bonds ---
    fragms = coords.get_Fragment_Atoms()
    for frag1 in range(len(fragms)):
        for frag2 in range(frag1+1,len(fragms)):
            ifbonds    = []
            for atom1 in fragms[frag1]:
                for atom2 in fragms[frag2]:
                    tmplen = linalg.norm(flatcoords[atom2*3:atom2*3+3] - flatcoords[atom1*3:atom1*3+3])
                    latom, ratom = atom1, atom2
                    ifbonds.append((tmplen, latom, ratom))
            ifbonds.sort()
            #print repr(ifbonds)
            coords.list_of_noncovstr.append(Stretch(ifbonds[0][1], ifbonds[0][2]))
            coords.coord_count += 1
            frag_lens = (len(fragms[frag1]), len(fragms[frag2]))
            if sum(frag_lens) > 2:
                ifbonds[:] = [__ib for __ib in ifbonds if not coords.contains_Stretch(__ib[1], __ib[2])[0]]
                # if 1 in frag_lens:
                #     coords.list_of_noncovstr.append(Stretch(ifbonds[1][1], ifbonds[1][2]))
                #     coords.coord_count += 1
                # else: # Both fragments have more than one atom
                #### WHAT WAS THIS FOR???? -> Not listed in Bakken's paper?
                # for ifb in range(1, min(2, len(ifbonds))):
                #     if not coords.contains_Stretch(ifbonds[ifb][1], ifbonds[ifb][2])[0]:
                #         coords.list_of_noncovstr.append(Stretch(ifbonds[ifb][1], ifbonds[ifb][2]))
                #         coords.coord_count += 1
                #         # print 'Adding IFB :', repr(ifbonds[ifb])
            # Now do the same for auxiliary IF bonds (bond smaller than 2 A or 1.3xIF bond)
            if getextrared:
                for atom1 in fragms[frag1]:
                    for atom2 in fragms[frag2]:
                        if not coords.contains_Stretch(atom1, atom2)[0]: # coords.list_of_noncovstr[-1].is_same(atom1, atom2):
                            tmplen = linalg.norm(flatcoords[atom2*3:atom2*3+3] - flatcoords[atom1*3:atom1*3+3])
                            if tmplen < 2.0 or tmplen < 1.3*ifbonds[0][0]: # ifbonds[0][0] contains length of smalles IF bond
                                coords.list_of_noncovstr.append(Stretch(atom1, atom2))
                                coords.list_of_noncovstr[-1].is_aux = True
                                coords.coord_count += 1
    # --- Determine REAL H-bonds ---
    fragbonds = coords.get_Stretches(only = 'COV')
    for bond in fragbonds:
        if bond.hid is not None: # any normal (covalent) bond should set the hid if there's a hydrogen in it
            heteroatoms = [i for i in bond.atom_ids if i in donor_elements]
            if len(heteroatoms) == 1:
                hid     = bond.hid # hydrogen atom
                hetid   = heteroatoms[0] # electronegative atom
                hpos    = flatcoords[hid*3:hid*3+3]
                hhetvec = flatcoords[hetid*3:hetid*3+3] - hpos
                hhetdist= np.linalg.norm(hhetvec)
                for acceptor in range(len(atomsymbols)):
                    if acceptor not in (hid, hetid) and atomsymbols[acceptor] in acceptor_elements:
                        covradsum   = getattr(Elements, 'H').cov_radius + getattr(Elements, atomsymbols[acceptor]).cov_radius
                        vdwradsum   = getattr(Elements, 'H').vdw_radius + getattr(Elements, atomsymbols[acceptor]).vdw_radius
                        haccvec     = flatcoords[acceptor*3:acceptor*3+3] - hpos
                        haccdist    = linalg.norm(haccvec)
                        if haccdist < vdwradsum and haccdist > covradsum:
                            hbangle = np.dot(haccvec, hhetvec)/(haccdist*hhetdist)
                            if hbangle <= 0.0:
                                coords.add_HB_Stretch((Stretch(hid, acceptor),), (hid,))
                                coords.coord_count += 1
    # In case there are auxiliary bonds left, add them.
    tmp_extra = [ bond for bond in tmp_extra if not coords.contains_Stretch(*bond.atom_ids)[0] ]
    coords.add_extra_Stretch(tmp_extra)
    coords.coord_count += len(tmp_extra)
    from numpy.linalg import matrix_rank
    print 'Rank of Bmat after Bonds: ', repr(matrix_rank(calcBmat(flatcoords, coords)))
    return coords

def getAngleIDs(flatcoords = None, atomsymbols = None, coords = None):
    """ Calculates the angles between all sets of atoms A-B-C where A and C are bonded
    to B. This is done by going through all bonds and calculating the angles with all
    bond vectors sharing a common (first) index.

    Option 1: loop through range(num_atoms); inner loop: all bonds -> O(n^2)
    Option 2:
    """
    assert None not in (flatcoords, atomsymbols, coords)
    from PyOpt.Control import list_of_options

    is_cart_linbend = (list_of_options['OPT'].key_vals['linbends'] == 'CART')
    do_bakken = list_of_options['OPT'].key_vals['dobakken']
    #logging.info('Note: Using cartesian linear bends is {:}'.format(is_cart_linbend))
    for atomB in range(len(atomsymbols)):
        for atomA in range(len(atomsymbols)):
            if atomB != atomA:
                is_stretch, __ = coords.contains_Stretch(atomB, atomA, only = 'REGULAR')
                if is_stretch:
                    for atomC in range(atomA+1,len(atomsymbols)):
                        # only angles with three different atoms and no reverse angles
                        is_stretch2, __ = coords.contains_Stretch(atomB, atomC, only = 'REGULAR')
                        is_bend, cbend  = coords.contains_Bend(atomA, atomB, atomC)
                        if atomC != atomA and is_stretch2 and not is_bend:
                            vals, refs = _calcAngle(flatcoords, atomA, atomB, atomC)
                            angl_ids = vals[0]
                            angl_rad = vals[2]
                            angl_cos = vals[1]
                            lvec    = refs[0]
                            ldist   = refs[2]
                            if angl_rad >= np.pi-0.2: # about 12°
                                if len(coords.get_Stretches(atom_id=atomB)) == 2:
                                    # stabilize with an additional, orthogonal angle
                                    if is_cart_linbend and not do_bakken:
                                        coords.list_of_bends.append(LinBendCart(atomA, atomB, atomC))
                                        coords.list_of_bends.append(LinBendCart(atomA, atomB, atomC, vers=1))
                                    else:
                                        coords.list_of_bends.append(LinearBend(atomA, atomB, atomC))
                                        coords.list_of_bends.append(LinearBend(atomA, atomB, atomC, vers=1))
                                    coords.coord_count += 2
                                else:
                                    logging.debug('    Skipping adding of linear bends, because central atom is not just 2-coordinate!')
                            elif angl_rad >= 0.1:
                                if not do_bakken:
                                    bend = Bend(atomA, atomB, atomC)
                                else:
                                    bend = Bend_Bakken(atomA, atomB, atomC)
                                coords.list_of_bends.append(bend)
                                coords.coord_count += 1
    return coords


def getTorsIDs(flatcoords = None, atomsymbols = None, coords = None):
    """ getTorsIDs(flatcoords, atomsymbols, coords)

    Function to create torsions out of overlapping angles. Thus, for a given angle a torsion is
    built if the central atom of the second angle is one of the outer atoms of the given angle
    and vice versa. However, only for angles not near to 180°.

    If no normal torsions were found by the said procedure, another run is attempted which
    adds all well defined permutations of the first torsion found using arbitrary combinations
    of four atoms.
    """
    assert flatcoords != None
    assert atomsymbols != None
    assert coords != None

    for atom1 in range(len(atomsymbols)):
        for atom2 in range(len(atomsymbols)):
            is_stretch1, stretch1 = coords.contains_Stretch(atom1, atom2)
            if is_stretch1 and not stretch1.is_aux:
                for atom3 in range(len(atomsymbols)):
                    is_stretch2, stretch2 = coords.contains_Stretch(atom2, atom3)
                    if is_stretch2 and not stretch2.is_aux and atom3 != atom1:
                        # don't use if atom1-atom2-atom3 is linear
                        __a = flatcoords[atom1*3:atom1*3+3] - flatcoords[atom2*3:atom2*3+3]
                        __b = flatcoords[atom3*3:atom3*3+3] - flatcoords[atom2*3:atom2*3+3]
                        if _calc_Angle_of_Vects(__a, __b) >= np.pi-0.2: continue # use this as an opportunity to build linear bends
                        for atom4 in range(atom1+1,len(atomsymbols)):
                            is_stretch3, stretch3 = coords.contains_Stretch(atom3, atom4)
                            if atom4 != atom2 and is_stretch3 and not stretch3.is_aux:
                                # linearity check for atom2-atom3-atom4
                                __a = flatcoords[atom2*3:atom2*3+3] - flatcoords[atom3*3:atom3*3+3]
                                __b = flatcoords[atom4*3:atom4*3+3] - flatcoords[atom3*3:atom3*3+3]
                                if _calc_Angle_of_Vects(__a, __b) >= np.pi-0.2: continue # again about 168°
                                if not coords.contains_Twist(atom1, atom2, atom3, atom4)[0]:
                                    coords.list_of_twists.append(Twist(atom1, atom2, atom3, atom4))
                                    coords.coord_count += 1
    # add torsions with collinear angles
    # 1. Search for collinear fragment
    for i in range(len(atomsymbols)):
        for j in range(len(atomsymbols)):
            is_stretch1, stretch1 = coords.contains_Stretch(i, j)
            if is_stretch1 and not stretch1.is_aux:
                for k in range(i+1, len(atomsymbols)):
                    if k == j: continue
                    vals, refs = _calcAngle(flatcoords, i, j, k)
                    is_stretch2, stretch2 = coords.contains_Stretch(j, k)
                    if is_stretch2 and not stretch2.is_aux and vals[2] == np.pi:
                        # get number of bonding partners
                        nbonds = 0
                        for jj in range(len(atomsymbols)):
                            if j != jj and coords.contains_Stretch(j, jj)[0]: nbonds += 1
                        if nbonds == 2:
                            atom2 = i
                            h = 0
                            while h < len(atomsymbols):
                                is_stretch3, stretch3 = coords.contains_Stretch(h, atom2)
                                if h != j and is_stretch3 and not stretch3.is_aux:
                                    vals, refs = _calcAngle(flatcoords, h, atom2, k)
                                    if vals[2] == 0.0:
                                        continue
                                    elif vals[2] == np.pi:
                                        atom2 = h
                                        h = 0
                                    else:
                                        atom1 = h
                                        atom3 = k
                                        for l in range(len(atomsymbols)):
                                            is_stretch4, stretch4 = coords.contains_Stretch(atom3, l)
                                            if l != j and is_stretch4 and not stretch4.is_aux:
                                                vals, refs = _calcAngle(flatcoords, l, atom3, atom2)
                                                if vals[2] == 0.0:
                                                    continue
                                                elif vals[2] == np.pi:
                                                    atom3 = l
                                                    continue # not necessary I guess
                                                else:
                                                    # hey, we finally got a well-defined dihedral!
                                                    atom4 = l
                                                    try:
                                                        vals, refs = _calcDihedral(flatcoords, atom1, atom2, atom3, atom4)
                                                        if not coords.contains_Twist(atom1, atom2, atom3, atom4)[0]:
                                                            coords.list_of_twists.append(Twist(atom1, atom2, atom3, atom4))
                                                            coords.coord_count += 1
                                                    except ValueError:
                                                        logging.debug('Calculation of Dihedral failed!')
                                h += 1
        return coords

def _bmat_numderiv(flatcoords, coords, diff = 1.0e-05):
    logverbose('--  Numerical Differentiation of primitive internal coordinates')
    if type(coords) == Internals:
        coordlist  = coords.get_Coords()
    else:
        coordlist  = coords
    tmpmat = np.zeros((len(coordlist),flatcoords.shape[0]))
    for idx, coord in enumerate(coordlist):
        # logverbose('--- Doing coordinate {:5d} {!r}'.format(idx,coord.atom_ids))
        for a in coord.atom_ids:
            for x in range(3):
                # logverbose('----Cartesian coordinate {:d}'.format(x))
                # two displacements
                bla = flatcoords.copy()

                bla[a*3+x] += diff
                pdispl      = coord.get_Val(bla)
                # andere Seite
                bla[a*3+x] -= 2.0*diff
                odispl      = coord.get_Val(bla)
                tmpmat[idx, a*3+x] += (pdispl-odispl)/(2*diff)
                # logverbose('    pdispl={:12.8e}, odispl={:12.8e}, deriv={:12.8e}'.format(pdispl,odispl,tmpmat[idx,a*3+x]))
                #print 'diff:', pdispl-odispl
        #print 'coord {0} row:\n'.format(idx), repr(tmpmat[idx])
    # log_Matrix('    New (numerical) B Matrix:', tmpmat)
    return tmpmat


def calcBmat(flatcoords = None, coords = None):
    """ calcBmat() calculates Wilson's B matrix from already determined primitive internal coordinates
    which should be assembled in the dictionary 'val_dict'. Since that is sorted the coordinates cannot
    be expected to have an order like '[bonds, angles, dihedrals]'.

    Returns a numpy 2D array (nintcoords, natoms*3) (C ordered array) with the derivatives and the
    corresponding numpy 1D array of primitive internal coordinates.
    """
    assert flatcoords != None
    assert coords != None

    if type(coords) == Internals:
        coord_list  = coords.get_Coords()
    else:
        coord_list  = coords
    pic_num     = len(coord_list)
    newBmat     = np.zeros((pic_num, flatcoords.shape[0]), dtype=np.float64)

    tmp_stretch = Stretch(0,1)
    tmp_bend    = Bend_Bakken(0,1,2)
    tmp_twist   = Twist(0,1,2,3)
    curr_coord  = 0
    #start_time = time.time()
    for c in coord_list:
        uvec    = None
        vvec    = None
        wvec    = None
        atom_ids = c if type(c) == tuple else c.atom_ids
        c_type  = ('stretch', 'bend', 'twist')[len(c)-2] if type(c) == tuple else c.type
        cnatoms = len(atom_ids)
        atom_a  = atom_ids[0]
        atom_b  = atom_ids[1]
        #logging.debug('calcBmat: Doing coordinate '+repr(c))
        if c_type == 'stretch':
            uvec = (flatcoords[atom_b*3:atom_b*3+3] - flatcoords[atom_a*3:atom_a*3+3])
            ulen = linalg.norm(uvec)
            uvec /= ulen
            newBmat[curr_coord, atom_a*3]   = -uvec[0]
            newBmat[curr_coord, atom_a*3+1] = -uvec[1]
            newBmat[curr_coord, atom_a*3+2] = -uvec[2]
            newBmat[curr_coord, atom_b*3]   = uvec[0]
            newBmat[curr_coord, atom_b*3+1] = uvec[1]
            newBmat[curr_coord, atom_b*3+2] = uvec[2]
        elif c_type in ('bend', 'LinBend', 'LinBendCart'):
            atom_c  = atom_ids[2]
            dpdx = tmp_bend.get_Deriv(flatcoords, atom_ids=atom_ids) if type(c) == tuple else c.get_Deriv(flatcoords)
            newBmat[curr_coord, atom_a*3:atom_a*3+3] = dpdx[0:3]
            newBmat[curr_coord, atom_b*3:atom_b*3+3] = dpdx[3:6]
            newBmat[curr_coord, atom_c*3:atom_c*3+3] = dpdx[6:9]
        elif c_type == 'twist':
            atom_c  = atom_ids[2] # p
            atom_d  = atom_ids[3] # n
            uvec    = flatcoords[atom_a*3:atom_a*3+3] - flatcoords[atom_b*3:atom_b*3+3]
            wvec    = flatcoords[atom_c*3:atom_c*3+3] - flatcoords[atom_b*3:atom_b*3+3]
            vvec    = flatcoords[atom_d*3:atom_d*3+3] - flatcoords[atom_c*3:atom_c*3+3]
            ulen    = linalg.norm(uvec)
            vlen    = linalg.norm(vvec)
            wlen    = linalg.norm(wvec)
            uvec   /= ulen
            wvec   /= wlen
            vvec   /= vlen
            uwcross = np.cross(uvec, wvec)
            vwcross = np.cross(vvec, wvec)
            cos_u   = np.dot(uvec, wvec)
            cos_v   = -np.dot(vvec, wvec)
            sin2_u   = 1.0 - cos_u**2
            sin2_v   = 1.0 - cos_v**2
            if sin2_u <= 1.0e-12 or sin2_v <= 1.0e-12: # torsion would be 0° or 180°
                curr_coord += 1
                logging.log(logging.VERYVERBOSE, '    Found torsion with linear angle' +
                                                 'for atoms {0},{1},{2},{3}. Not calculating derivative!'.format(atom_a, atom_b,atom_c,atom_d))
                continue
            else:
                newBmat[curr_coord, atom_a*3]   = uwcross[0]/(ulen * sin2_u)
                newBmat[curr_coord, atom_a*3+1] = uwcross[1]/(ulen * sin2_u)
                newBmat[curr_coord, atom_a*3+2] = uwcross[2]/(ulen * sin2_u)
                newBmat[curr_coord, atom_b*3]   = -uwcross[0]/(ulen * sin2_u) + uwcross[0]*cos_u/(sin2_u * wlen) + vwcross[0]*cos_v/(sin2_v * wlen)
                newBmat[curr_coord, atom_b*3+1] = -uwcross[1]/(ulen * sin2_u) + uwcross[1]*cos_u/(sin2_u * wlen) + vwcross[1]*cos_v/(sin2_v * wlen)
                newBmat[curr_coord, atom_b*3+2] = -uwcross[2]/(ulen * sin2_u) + uwcross[2]*cos_u/(sin2_u * wlen) + vwcross[2]*cos_v/(sin2_v * wlen)
                newBmat[curr_coord, atom_c*3]   = vwcross[0]/(vlen * sin2_v) - uwcross[0]*cos_u/(sin2_u * wlen) - vwcross[0]*cos_v/(sin2_v * wlen)
                newBmat[curr_coord, atom_c*3+1] = vwcross[1]/(vlen * sin2_v) - uwcross[1]*cos_u/(sin2_u * wlen) - vwcross[1]*cos_v/(sin2_v * wlen)
                newBmat[curr_coord, atom_c*3+2] = vwcross[2]/(vlen * sin2_v) - uwcross[2]*cos_u/(sin2_u * wlen) - vwcross[2]*cos_v/(sin2_v * wlen)
                newBmat[curr_coord, atom_d*3]   = -vwcross[0]/(vlen * sin2_v)
                newBmat[curr_coord, atom_d*3+1] = -vwcross[1]/(vlen * sin2_v)
                newBmat[curr_coord, atom_d*3+2] = -vwcross[2]/(vlen * sin2_v)
        curr_coord += 1
    # log_Matrix('    New BMatrix:', newBmat)
    return newBmat

def calcKmat(flatcoords = None, coords = None, grad_int = None, cnum = None):
    """ calcKmat() calculates the second derivatives of the primitive internal coordinates with
    respect to the cartesian coordinates.
    """
    assert flatcoords != None
    assert coords != None
    assert grad_int != None
    assert grad_int.shape[0] == coords.coord_count, 'Grad shape: {0}, coord_count: {1}'.format(grad_int.shape, coords.coord_count)

    if cnum != None:
        coord_list  = [coords.get_Coords()[cnum]]
    else:
        coord_list  = coords.get_Coords()
    newKmat     = np.zeros((flatcoords.shape[0], flatcoords.shape[0]), dtype = np.float64)

    for coord_id, intco in enumerate(coord_list):
        tmpmat  = np.zeros((flatcoords.shape[0], flatcoords.shape[0]), dtype = np.float64)
        atoms   = intco.atom_ids
        if intco.type == 'stretch':
            atom_a, atom_b = atoms
            vec = flatcoords[atom_b*3:atom_b*3+3] - flatcoords[atom_a*3:atom_a*3+3]
            if not _norm_is_ok(vec):
                raise ValueError('Norm for Kmat not right.')
            vec_len = linalg.norm(vec)
            vec    /= vec_len

            # INVERSE STRETCH NOT HANDLED? -> Do I need that?
            for atom1 in atoms:
                for xyz1 in range(3):
                    for atom2 in atoms:
                        for xyz2 in range(3):
                            tmpval = (vec[xyz1] * vec[xyz2] - dij(xyz1, xyz2))/vec_len
                            if atom1 == atom2: tmpval = -tmpval
                            if cnum == None: tmpval *= grad_int[coord_id]
                            tmpmat[atom1*3+xyz1, atom2*3+xyz2] = tmpval
        elif intco.type == 'bend':
            atom_a, atom_b, atom_c = atoms
            uvec = flatcoords[atom_a*3:atom_a*3+3] - flatcoords[atom_b*3:atom_b*3+3]
            vvec = flatcoords[atom_c*3:atom_c*3+3] - flatcoords[atom_b*3:atom_b*3+3]
            ulen = linalg.norm(uvec)
            vlen = linalg.norm(vvec)
            uvec /= ulen
            vvec /= vlen

            # Determine wvec
            if not _are_parallel(uvec, vvec):
                # if intco.is_pseudo:
                #     wvec = flatcoords[atom_b*3:atom_b*3+3] - 0.5*(flatcoords[atom_a*3:atom_a*3+3] + flatcoords[atom_c*3:atom_c*3+3])
                # else:
                wvec = np.cross(uvec, vvec)
            else:
                orth_vec = np.array([1.0, -1.0, 1.0])
                orth_len = linalg.norm(orth_vec)
                if not _are_parallel(uvec, orth_vec) and not _are_parallel(vvec, orth_vec):
                    wvec = np.cross(uvec, orth_vec)
                else:
                    wvec = np.cross(uvec, np.array([-1.0, 1.0, 1.0]))
                # if intco.is_pseudo:
                #     wvec = np.cross(wvec, uvec)
            wlen = linalg.norm(wvec)
            wvec /= wlen

            # Computer first derivatives ?
            tmpBmat = np.zeros((3,3))
            uwvec   = np.cross(uvec, wvec)
            wvvec   = np.cross(wvec, vvec)
            for atom in range(3):
                for xyz in range(3):
                    tmpBmat[atom, xyz] = zijk(atom, 0, 1) * uwvec[xyz]/ulen + zijk(atom, 2, 1) * wvvec[xyz]/vlen
            cos_q   = np.dot(uvec, vvec)
            if 1.0 - cos_q**2 <= 1.0e-12: continue
            sin_q   = np.sqrt(1.0 - cos_q**2)

            # And the second derivatives
            for idx1, atom1 in enumerate(atoms):
                for xyz1 in range(3):
                    for idx2, atom2 in enumerate(atoms):
                        for xyz2 in range(3):
                            tmpval = zijk(idx1, 0, 1)*zijk(idx2, 0, 1)*(uvec[xyz1]*vvec[xyz2] + uvec[xyz2]*vvec[xyz1] - 3*uvec[xyz1]*uvec[xyz2]*cos_q + dij(xyz1, xyz2)*cos_q) / (ulen**2 * sin_q)
                            tmpval += zijk(idx1, 2, 1)*zijk(idx2, 2, 1)*(uvec[xyz1]*vvec[xyz2] + uvec[xyz2]*vvec[xyz1] - 3*vvec[xyz1]*vvec[xyz2]*cos_q + dij(xyz1, xyz2)*cos_q) / (vlen**2 * sin_q)
                            tmpval += zijk(idx1, 0, 1)*zijk(idx2, 2, 1)*(uvec[xyz1]*uvec[xyz2] + vvec[xyz2]*vvec[xyz1] - uvec[xyz1]*vvec[xyz2]*cos_q - dij(xyz1, xyz2)) / (ulen*vlen*sin_q)
                            tmpval += zijk(idx1, 2, 1)*zijk(idx2, 0, 1)*(vvec[xyz1]*vvec[xyz2] + uvec[xyz2]*uvec[xyz1] - vvec[xyz1]*uvec[xyz2]*cos_q - dij(xyz1, xyz2)) / (ulen*vlen*sin_q)
                            tmpval -= tmpBmat[idx1, xyz1] * tmpBmat[idx2, xyz2] * cos_q / sin_q
                            if cnum == None: tmpval *= grad_int[coord_id]
                            tmpmat[atom1*3+xyz1, atom2*3+xyz2] = tmpval

        elif intco.type == 'twist':
            atom_a, atom_b, atom_c, atom_d = atoms
            uvec = flatcoords[atom_a*3:atom_a*3+3] - flatcoords[atom_b*3:atom_b*3+3]
            vvec = flatcoords[atom_d*3:atom_d*3+3] - flatcoords[atom_c*3:atom_c*3+3]
            wvec = flatcoords[atom_c*3:atom_c*3+3] - flatcoords[atom_b*3:atom_b*3+3]
            ulen = linalg.norm(uvec)
            vlen = linalg.norm(vvec)
            wlen = linalg.norm(wvec)
            uvec /= ulen
            vvec /= vlen
            wvec /= wlen

            cos_u = np.dot(uvec, wvec)
            cos_v = -np.dot(vvec, wvec)
            if 1.0-cos_u**2 <= 1.0e-12 or 1.0-cos_v**2 <= 1.0e-12: continue
            sin_u = np.sqrt(1.0 - cos_u**2)
            sin_v = np.sqrt(1.0 - cos_v**2)

            uwcross = np.cross(uvec, wvec)
            vwcross = np.cross(vvec, wvec)
            sin4_u  = sin_u**4
            sin4_v  = sin_v**4
            cos3_u  = cos_u**3
            cos3_v  = cos_v**3

            for idx1, atom1 in enumerate(atoms):
                for idx2, atom2 in enumerate(atoms[:idx1+1]):
                    for xyz1 in range(3):
                        for xyz2 in range(3):
                            tmpval = 0
                            if (idx1 == 0 and idx2 == 0) or (idx1 == 1 and idx2 == 0) or (idx1 == 1 and idx2 == 1):
                                tmpval += zijk(idx1,0,1)*zijk(idx2,0,1)*(uwcross[xyz1]*(wvec[xyz2]*cos_u - uvec[xyz2]) + uwcross[xyz2]*(wvec[xyz1]*cos_u - uvec[xyz1]))/(sin4_u*ulen**2)

                            if (idx1 ==3 and idx2 == 3) or (idx1 == 3 and idx2 == 2) or (idx1 == 2 and idx2 == 2):
                                tmpval += zijk(idx1,3,2)*zijk(idx2,3,2)*(vwcross[xyz1]*(wvec[xyz2]*cos_v + vvec[xyz2]) + vwcross[xyz2]*(wvec[xyz1]*cos_v + vvec[xyz1]))/(sin4_v*vlen**2)

                            if (idx1 == 1 and idx2 == 1) or (idx1 == 2 and idx2 == 1) or (idx1 == 2 and idx2 == 0) or (idx1 == 1 and idx2 == 0):
                                tmpval += (zijk(idx1,0,1)*zijk(idx2,1,2) + zijk(idx1,2,1)*zijk(idx2,1,0))*(
                                           uwcross[xyz1] * (wvec[xyz2] - 2*uvec[xyz2]*cos_u + wvec[xyz2]*cos_u**2) +
                                           uwcross[xyz2] * (wvec[xyz1] - 2*uvec[xyz1]*cos_u + wvec[xyz1]*cos_u**2))/(2*ulen*wlen*sin4_u)

                            if (idx1 == 3 and idx2 == 2) or (idx1 == 3 and idx2 == 1) or (idx1 == 2 and idx2 == 2) or (idx1 == 2 and idx2 == 1):
                                tmpval += (zijk(idx1,3,2)*zijk(idx2,2,1) + zijk(idx1,1,2)*zijk(idx2,2,3))*(
                                           vwcross[xyz1] * (wvec[xyz2] + 2*vvec[xyz2]*cos_v + wvec[xyz2]*cos_v**2) +
                                           vwcross[xyz2] * (wvec[xyz1] + 2*vvec[xyz1]*cos_v + wvec[xyz1]*cos_v**2))/(2*vlen*wlen*sin4_v)

                            if (idx1 == 1 and idx2 == 1) or (idx1 == 2 and idx2 == 2) or (idx1 == 2 and idx2 == 1):
                                tmpval += zijk(idx1,1,2)*zijk(idx2,2,1)*(uwcross[xyz1]*(uvec[xyz2] + uvec[xyz2]*cos_u**2 - 3*wvec[xyz2]*cos_u + wvec[xyz2]*cos3_u) +
                                                                         uwcross[xyz2]*(uvec[xyz1] + uvec[xyz1]*cos_u**2 - 3*wvec[xyz1]*cos_u + wvec[xyz1]*cos3_u))/(2*sin4_u*wlen**2)

                            if (idx1 == 2 and idx2 == 1) or (idx1 == 2 and idx2 == 2) or (idx1 == 1 and idx2 == 1): # if redundant?
                                tmpval += zijk(idx1,2,1)*zijk(idx2,1,2)*(vwcross[xyz1]*(-vvec[xyz2] - vvec[xyz2]*cos_v**2 - 3*wvec[xyz2]*cos_v + wvec[xyz2]*cos3_v) +
                                                                         vwcross[xyz2]*(-vvec[xyz1] - vvec[xyz1]*cos_v**2 - 3*wvec[xyz1]*cos_v + wvec[xyz1]*cos3_v))/(2*sin4_v*wlen**2)

                            if (idx1 != idx2) and (xyz1 != xyz2):
                                if xyz1 != 0 and xyz2 != 0:
                                    k = 0
                                elif xyz1 != 1 and xyz2 != 1:
                                    k = 1
                                else:
                                    k = 2

                                tmppow = (-0.5)**np.absolute(xyz2 - xyz1)
                                if idx1 == 1 and idx2 == 1: # not used at all?
                                    tmpval += zijk(idx1,0,1)*zijk(idx2,1,2)*(xyz2 - xyz1)*tmppow*(wvec[k]*cos_u - uvec[k])/(ulen*wlen*sin_u**2)

                                if (idx1 == 3 and idx2 == 2) or (idx1 == 3 and idx2 == 1) or (idx1 == 2 and idx2 == 2) or (idx1 == 2 and idx2 == 1):
                                    tmpval += zijk(idx1,3,2)*zijk(idx2,2,1)*(xyz2 - xyz1)*tmppow*(-wvec[k]*cos_v - vvec[k])/(vlen*wlen*sin_v**2)

                                if (idx1 == 2 and idx2 == 1) or (idx1 == 2 and idx2 == 0) or (idx1 == 1 and idx2 == 1) or (idx1 == 1 and idx2 == 0):
                                    tmpval += zijk(idx1,2,1)*zijk(idx2,1,0)*(xyz2 - xyz1)*tmppow*(-wvec[k]*cos_u + uvec[k])/(ulen*wlen*sin_u**2)

                                if (idx1 == 2 and idx2 == 2): # not used either?
                                    tmpval += zijk(idx1,1,2)*zijk(idx2,2,3)*(xyz2 - xyz1)*tmppow*(wvec[k]*cos_v + vvec[k])/(vlen*wlen*sin_v**2)
                            if cnum == None: tmpval *= grad_int[coord_id]
                            tmpmat[atom1*3+xyz1, atom2*3+xyz2]  = tmpval
                            tmpmat[atom2*3+xyz2, atom1*3+xyz1]  = tmpmat[atom1*3+xyz1, atom2*3+xyz2]
        newKmat += tmpmat
    return newKmat

def _kmat_numderiv(flatcoords, coords, int_grad, diff = 0.001, cnum = None, umat = None):
    tmpmat = np.zeros((flatcoords.shape[0], flatcoords.shape[0]))
    for xk in range(tmpmat.shape[0]):
        bla         = flatcoords.copy()
        bla[xk]    += diff
        bmat_p      = calcBmat(bla, coords)
        bla[xk]    -= 2.0*diff
        bmat_m      = calcBmat(bla, coords)
        if umat != None:
            bmat_p = np.dot(umat.transpose(), bmat_p)
            bmat_m = np.dot(umat.transpose(), bmat_m)
        if cnum == None:
            tb      = (bmat_p-bmat_m)/(2.0*diff)
            dBdxk   = np.dot(tb.transpose(),int_grad)
        else:
            dBdxk   = (bmat_p[cnum,:]-bmat_m[cnum,:])/(2.0*diff)
        #kmat_row    = dBdxk.sum(axis=0)
        tmpmat[xk,:]= dBdxk
    return tmpmat
