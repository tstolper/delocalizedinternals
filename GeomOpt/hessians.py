
# -*- coding: utf-8 -*-

############################################################
#                                                          #
# Module providing approximations and updates to the       #
# Hessian matrix.                                          #
#                                                          #
# Currently provides:                                      #
#                                                          #
# Handling self-defined Hessian                            #
#                                                          #
############################################################
import time
import logging
import scipy as np
import scipy.linalg as linalg
from numpy import arccos, exp, dot, pi
from numpy.linalg import norm
from numba import jit

import PyOpt.Potentials
from PyOpt.GeomOpt.atomprops import get_Period, Elements, Bohr2Ang
from PyOpt.GeomOpt.intcoord_funcs import calcBmat, _kmat_numderiv
from PyOpt.GeomOpt.primitives import Stretch, Bend, Twist, Internals
from PyOpt.GeomOpt.utility_funcs import logverbose, log_Matrix, log_Vector, log_Vectors_stacked, symm_mat_inv, _calcAngle, log_Progress
from PyOpt.GeomOpt.coordinates import Cartesians, PrimitiveInternals, NormalCoordinates

@jit(nopython=True)
def get_Lindh_coords(cart_coords, atom_periods, lindh_k, lindh_aij, lindh_rij):
    #Constants for the creation of Lindh's Hessian
    thresh_zero = 1.0e-6
    tmpstretch = []
    tmpbend = []
    tmptwist = []
    tmpks = []
    natoms = len(atom_periods)
    # stime = time.time()
    # cit = 0
    # cmax = (natoms*(natoms-1))/2
    for a in range(natoms):
        for b in range(a+1, natoms):
            #tmpstr = Stretch(a, b)
            #if tmpstr.get_Val(coords) < 8.0: # Some value used by PSI4. Lindh doesn't use it I guess.
            p1  = atom_periods[a]-1
            p2  = atom_periods[b]-1
            val = norm(cart_coords[b*3:b*3+3] - cart_coords[a*3:a*3+3])
            k   = lindh_k[0] * exp( -lindh_aij[p1,p2] * (val**2 - lindh_rij[p1,p2]**2))
            if k > thresh_zero:
                tmpstretch.append((a, b))
                tmpks.append(k)
            # else:
            #     logverbose('Skipping stretch '+repr((a,b))+' because of small k: '+repr(k))
            # cit += 1
            # ctime = time.time()
            #log_Progress('Str: after {:8.2f} minutes finished '.format((ctime-stime)/60.0), cit, cmax)
    # Bends
    # stime = time.time()
    # cit = 0
    # cmax = (natoms*(natoms-1)*(natoms-2))/2
    for a in range(natoms):
        for b in range(natoms):
            if a != b:
                for c in range(a+1, natoms):
                    if b != c:
                        v12 = cart_coords[a*3:a*3+3] - cart_coords[b*3:b*3+3]
                        v23 = cart_coords[c*3:c*3+3] - cart_coords[b*3:b*3+3]
                        dist12 = norm(v12)
                        dist23 = norm(v23)
                        angle = arccos(dot(v12,v23)/(dist12*dist23))*180.0/pi
                        #if angle < 175.0 and angle > 5.0:
                        p1  = atom_periods[a]-1
                        p2  = atom_periods[b]-1
                        p3  = atom_periods[c]-1
                        k   = lindh_k[1] * exp(-lindh_aij[p1,p2] * (dist12**2 - lindh_rij[p1,p2]**2)) *\
                                           exp(-lindh_aij[p2,p3] * (dist23**2 - lindh_rij[p2,p3]**2))
                        if k > thresh_zero:
                            tmpbend.append((a,b,c))
                            tmpks.append(k)
                            # else:
                            #     logverbose('Skipping bend '+repr((a,b,c))+' because of small k: '+repr(k))
                        # else:
                        #     logverbose('avoiding linear bend for '+repr((a,b,c)))
                        # cit += 1
                        # ctime = time.time()
                        # log_Progress('Bend: after {:8.2f} minutes finished '.format((ctime-stime)/60.0), cit, cmax)
    # Twists
    # stime = time.time()
    # cit = 0
    # cmax = (natoms*(natoms-1)*(natoms-2)*(natoms-3))/4
    for a in range(natoms):
        for b in range(natoms):
            if a != b:
                for c in range(natoms):
                    if a != c and b != c:
                        for d in range(a+1, natoms):
                            if d != b and d != c:
                                v12 = cart_coords[a*3:a*3+3] - cart_coords[b*3:b*3+3]
                                v23 = cart_coords[c*3:c*3+3] - cart_coords[b*3:b*3+3]
                                v34 = cart_coords[d*3:d*3+3] - cart_coords[c*3:c*3+3]
                                dist12 = norm(v12)
                                dist23 = norm(v23)
                                dist34 = norm(v34)
                                angle123 = arccos(dot(v12,v23)/(dist12*dist23))*180.0/pi
                                angle234 = arccos(dot(v23,v34)/(dist23*dist34))*180.0/pi
                                if angle123 < 175.0 and angle123 > 5.0 and angle234 < 175.0 and angle234 > 5.0:
                                    p1  = atom_periods[a]-1
                                    p2  = atom_periods[b]-1
                                    p3  = atom_periods[c]-1
                                    p4  = atom_periods[d]-1
                                    k   = lindh_k[2] * exp(-lindh_aij[p1,p2] * (dist12**2 - lindh_rij[p1,p2]**2)) *\
                                                       exp(-lindh_aij[p2,p3] * (dist23**2 - lindh_rij[p2,p3]**2)) *\
                                                       exp(-lindh_aij[p3,p4] * (dist34**2 - lindh_rij[p3,p4]**2))
                                    if k > thresh_zero:
                                        tmptwist.append((a, b, c, d))
                                        tmpks.append(k)
                                    # else:
                                    #     logverbose('Skipping twist '+repr((a,b,c,d))+' because of small k: '+repr(k))
                                    #logging.debug('Saving twist '+repr((a,b,c,d)))
                                    # cit += 1
                                    # ctime = time.time()
                                    # log_Progress('Twist: after {:8.2f} minutes finished '.format((ctime-stime)/60.0), cit, cmax)
    return (tmpstretch, tmpbend, tmptwist, tmpks)

class Hessian(object):
    """
    Main class for storing and using the Hessian matrix. Options that should once be choosable from:
    saveLastNum     integer                                      Defines the number of Hessians to save.
    """
    def __init__(self, optref = None, approx = 'EXACT', callback = None, update_type = 'None', update_steps = 1, **kwargs):
        self.OptParent      = optref
        self.OptHist        = optref.OptHist
        self.pushLog        = optref.OptHist.pushNew_Hess
        self.getHess        = callback
        self.hessapp        = approx.upper()
        self.update_type    = update_type.upper()
        self.update_steps   = update_steps
        self.current        = None
        self.current_int    = None
        self.current_cart   = None

        if 'exact_steps' in kwargs:
            self.exact_steps = kwargs['exact_steps']
        else:
            self.exact_steps = 0

    def rebuild_Internal_Hessians(self, coords=None, optsteps=None):
        """ Rebuilds the Hessian and Hessians of the optimization history
        if the internal coordinates changed.
        """
        assert coords is not None
        assert optsteps is not None
        for i in xrange(len(optsteps)):
            isint, hess = self.get_Hessian(coords=coords, incart=False, step_hist=optsteps, for_step=i+1)
            optsteps[i].hess_int = hess

    def get_nonred_Basis(self, hess_mat = None, evthres = 1.0E-7):
        """
        Convenience function to transform the hessian into a new basis, in which
        it is diagonal. Also gets rid of redundant eigenvalues/-vectors, effectively
        transforming out translation and hopefully also rotation.

        Returns:

        (diagHess, newHessBasis)    tuple of diagonal Hessian and eigenvector matrix
        """
        if hess_mat is not None:
            hessevals, hessevecs = linalg.eigh(hess_mat)
        else:
            hessevals, hessevecs = linalg.eigh(self.currhess)
        veccols         = np.where(np.absolute(hessevals) > evthres)[0]
        nonredEvals     = hessevals[veccols]
        newHessBasis    = hessevecs[:,veccols] #advanced slicing is veeeery practical. C
        diagHess        = np.diag(nonredEvals) # Λ
        #Aaaand the debug stuff.
        logging.debug('--- Hessian Diagonalisation ---')
        log_Vector('Eigenvalues of the Hessian:', nonredEvals)
        log_Matrix('Eigenvectors/LNCs:', newHessBasis)
        return (diagHess, newHessBasis)

    def get_Soler_Hess(self, coords_cart=None, asymbs=None, curr_step=None):

        natoms  = len(coords_cart)//3
        cov_R   = tuple(getattr(Elements, A).cov_radius for A in asymbs)
        max_R   = max(cov_R)
        A = 3.0e+5*Bohr2Ang**2/27.211385 # Eh/Bohr^2
        B = 0.1
        tmpcoords   = {}
        coordlist   = []
        tmpks       = {}
        tmpdiag     = []
        for atm_i in range(natoms):
            for atm_j in range(atm_i+1, natoms):
                stretch = Stretch(atm_i, atm_j)
                if stretch.get_Val(coords_cart) < 6.0*max_R:
                    tmpcoords[atm_i, atm_j] = stretch
                    coordlist.append(stretch)
                    ks_ij = A*np.power((cov_R[atm_i] + cov_R[atm_j])/stretch.get_Val(coords_cart), 8)
                    tmpdiag.append(ks_ij)
                    tmpks[atm_i, atm_j] = ks_ij
                    # print 'Adding stretch between '+repr((atm_i, atm_j))+' with ks='+repr(ks_ij)
        for atm_i, atm_j in tmpcoords.keys():
            for atm_k in range(natoms):
                if( atm_k not in (atm_i, atm_j)
                    and (atm_j, atm_i, atm_k) not in tmpcoords
                    and (atm_k, atm_j, atm_i) not in tmpcoords ):
                    bend = None
                    b1ids= (atm_i, atm_j)
                    b2ids= None
                    if (atm_k, atm_j) in tmpcoords:
                        b2ids = (atm_k, atm_j)
                    elif (atm_j, atm_k) in tmpcoords:
                        b2ids = (atm_j, atm_k)
                    if b2ids is not None:
                        bend = Bend(atm_i, atm_j, atm_k)
                        if bend.get_Val(coords_cart)*180.0/np.pi < 175.0:
                            tmpcoords[(atm_i, atm_j, atm_k)] = bend
                            coordlist.append(bend)
                            ks_ij   = tmpks[b1ids]
                            ks_jk   = tmpks[b2ids]
                            kb_ijk = B*np.sqrt(ks_ij*ks_jk)*tmpcoords[b1ids].get_Val(coords_cart)*tmpcoords[b2ids].get_Val(coords_cart)
                            tmpdiag.append(kb_ijk)
                            # print 'adding bend between '+repr((atm_i, atm_j, atm_k))+' with kb='+repr(kb_ijk)
                    b2ids= None
                    if (atm_k, atm_i) in tmpcoords:
                        b2ids = (atm_k, atm_i)
                    elif (atm_i, atm_k) in tmpcoords:
                        b2ids = (atm_i, atm_k)
                    if b2ids is not None:
                        bend = Bend(atm_j, atm_i, atm_k)
                        if bend.get_Val(coords_cart)*180.0/np.pi < 175.0:
                            tmpcoords[(atm_j, atm_i, atm_k)] = bend
                            coordlist.append(bend)
                            ks_ij   = tmpks[b1ids]
                            ks_jk   = tmpks[b2ids]
                            kb_ijk = B*np.sqrt(ks_ij*ks_jk)*tmpcoords[b1ids].get_Val(coords_cart)*tmpcoords[b2ids].get_Val(coords_cart)
                            tmpdiag.append(kb_ijk)
                            # print 'adding bend between '+repr((atm_j, atm_i, atm_k))+' with kb='+repr(kb_ijk)
        inthess = np.diag(tmpdiag)
        log_Matrix('Soler Hessian guess in internal coordinates:', inthess)

        tmpbmat = calcBmat(coords_cart, coordlist)
        gminv   = symm_mat_inv(np.dot(tmpbmat, tmpbmat.T), redundant=True)
        gmb     = np.dot(gminv, tmpbmat)
        pmat    = np.dot(np.dot(tmpbmat, tmpbmat.T), gminv)
        grad_int= np.dot(gmb, curr_step.grad_cart)
        tmpkmat = _kmat_numderiv(flatcoords=coords_cart, coords=coordlist, int_grad=grad_int)
        log_Matrix('tmpBmat:', tmpbmat)
        log_Matrix('tmpKmat:', tmpkmat)
        carthess= np.dot(tmpbmat.T, np.dot(inthess, tmpbmat)) + tmpkmat
        log_Matrix('Soler Hessian guess in cartesian coordinates:', carthess)
        return carthess

    def get_Lindh_Hess(self, coords = None, asymbs = None, coordlist = None, **kwargs):
        """
        Creates and assignes the Hessian as defined by Lindh. Uses a basic definition in which all possible stretches, bends and twists
        except the redundant inverse are used to create the internal coordinates. These coordinates are then used to directly transform
        to a cartesian Hessian.
        NOTE: Bakken and Helgaker actually used the actual coordinate set used in the optimization to determine Lindh's Hessian.

        coordref    flat numpy array        coordinates of the atoms
        atomord     integer array           atomic number of the atoms
        """
        logging.debug('------------- Lindh model Hessian -----------------')
        lindh_k = np.array((0.45, 0.15, 0.005)) #kr, kφ, kτ
        lindh_aij = np.array(((1.0000, 0.3949, 0.3949, 0.3949), #Too redundant for a tuple?
                          (0.3949, 0.2800, 0.2800, 2.800),
                          (0.3949, 0.2800, 0.2800, 2.500),
                          (0.3949, 0.2800, 0.2500, 2.500)))
        lindh_rij = np.array(((1.35, 2.10, 2.53, 2.76), #And of course symmetric
                              (2.10, 2.87, 3.40, 3.71),
                              (2.53, 3.40, 3.40, 3.71),
                              (2.76, 3.71, 3.71, 3.80)))
        natoms      = len(coords) // 3
        # carthess    = np.zeros((natoms*3, natoms*3))
        curr_step   = kwargs.pop('curr_step', None)
        inint       = kwargs.pop('inint', False)
        periods = np.zeros((natoms, ), dtype=np.int32)
        for i in range(natoms):
            periods[i] = get_Period(asymbs[i])
        # Old definition
        if coordlist is None or not inint: # More or less Lindh's definition
            # Stretches
            
            # # Get the derivatives
            # if coordlist is None:
            #     tmpPICs     = PrimitiveInternals(startcart=coords, atomlabels=asymbs, dotest=True, getextrared=True)
            #     coordlist   = tmpPICs.coords.get_Coords()
            stime = time.time()
            tmps, tmpb, tmpt, tmpks = get_Lindh_coords(coords, periods, lindh_k, lindh_aij, lindh_rij)
            etime = time.time()
            logging.debug('Time needed for calculating coords: {:8.2f}'.format((etime-stime)/60.0))
            coordlist = tmps + tmpb + tmpt
            logging.debug('Number of coords: '+repr(len(coordlist)))
            # gminv   = symm_mat_inv(np.dot(tmpbmat, tmpbmat.T), redundant=True)
            # logging.debug('Getting gmb')
            # gmb     = np.dot(gminv, tmpbmat)
            # grad_int= np.dot(gmb, curr_step.grad_cart)
            #tmpkmat = _kmat_numderiv(flatcoords=coords, coords=coordlist, int_grad=grad_int)
        else:
            if coordlist is not None:
                tmpks = []
                for co in coordlist:
                    if co.type == 'stretch':
                        a, b = co.atom_ids
                        p1  = periods[a]-1
                        p2  = periods[b]-1
                        val = norm(coords[b*3:b*3+3] - coords[a*3:a*3+3])
                        k   = lindh_k[0] * exp( -lindh_aij[p1,p2] * (val**2 - lindh_rij[p1,p2]**2))
                    elif co.type in ('bend', 'LinBendCart'):
                        a, b, c = co.atom_ids
                        v12 = coords[a*3:a*3+3] - coords[b*3:b*3+3]
                        v23 = coords[c*3:c*3+3] - coords[b*3:b*3+3]
                        dist12 = norm(v12)
                        dist23 = norm(v23)
                        angle = arccos(dot(v12,v23)/(dist12*dist23))*180.0/pi
                        #if angle < 175.0 and angle > 5.0:
                        p1  = periods[a]-1
                        p2  = periods[b]-1
                        p3  = periods[c]-1
                        k   = lindh_k[1] * exp(-lindh_aij[p1,p2] * (dist12**2 - lindh_rij[p1,p2]**2)) *\
                                           exp(-lindh_aij[p2,p3] * (dist23**2 - lindh_rij[p2,p3]**2))
                    elif co.type == 'twist':
                        a, b, c, d = co.atom_ids
                        v12 = coords[a*3:a*3+3] - coords[b*3:b*3+3]
                        v23 = coords[c*3:c*3+3] - coords[b*3:b*3+3]
                        v34 = coords[d*3:d*3+3] - coords[c*3:c*3+3]
                        dist12 = norm(v12)
                        dist23 = norm(v23)
                        dist34 = norm(v34)
                        angle123 = arccos(dot(v12,v23)/(dist12*dist23))*180.0/pi
                        angle234 = arccos(dot(v23,v34)/(dist23*dist34))*180.0/pi
                        p1  = periods[a]-1
                        p2  = periods[b]-1
                        p3  = periods[c]-1
                        p4  = periods[d]-1
                        k   = lindh_k[2] * exp(-lindh_aij[p1,p2] * (dist12**2 - lindh_rij[p1,p2]**2)) *\
                                        exp(-lindh_aij[p2,p3] * (dist23**2 - lindh_rij[p2,p3]**2)) *\
                                        exp(-lindh_aij[p3,p4] * (dist34**2 - lindh_rij[p3,p4]**2))
                    tmpks.append(k)
        #tmpkmat = _kmat_numderiv(coords, coordlist, )
        rethess  = np.diag(tmpks)
        log_Vector('Coordinates:', coordlist, sform = '{!r}')
        log_Vector('Lindh Hessian guess in internal coordinates:', tmpks)
        if not inint:
            stime = time.time()
            tmpbmat = calcBmat(coords, coordlist)
            logging.debug('Size of tmpbmat: {:15}'.format(repr(tmpbmat.shape)))
            log_Matrix('Bmat:', tmpbmat)
            rethess = np.dot(tmpbmat.T, np.dot(rethess, tmpbmat)) #+ tmpkmat
            log_Matrix('Lindh Hessian guess in cartesian coordinates:', rethess)
            etime = time.time()
            logging.debug('Total time for transformtion to cartesian: {:8.2f} min'.format((etime-stime)/60.0))
        return rethess

    def get_Almloef_Hess(self, coords=None, asymbs=None, coordlist=None, **kwargs):
        assert coords is not None
        assert asymbs is not None

        ret_inint   = kwargs.pop('inint', False)
        cov_R       = tuple(getattr(Elements, A).cov_radius for A in asymbs)
        # Get all stretches, bends, torsions and out-of-plane bends
        # Basically they say one should use all possible coordinates according to bonded atoms,
        # i.e. basically the normal redundant coordinates (without extra)
        # and not just every possible combination of atoms.
        tmpint      = PrimitiveInternals(startcart=coords, atomlabels=asymbs, dotest=True, getextrared=False)
        stretches   = tmpint.coords.get_Stretches()
        bends       = tmpint.coords.get_Bends()
        torsions    = tmpint.coords.get_Twists()
        # tmpint = Internals(has_extrared_bonds=False)
        # for aid, asym in enumerate(asymbs):
        #     for aid2, asym2 in enumerate(asymbs, start=aid+1):
        #         tmpint.add_Stretch(ids=(aid, aid2), frag_id=0)
        tmpks = []
        for cstr in stretches:
            hab = 0.3601*np.exp(-1.944*(cstr.get_Val(flatcoords=coords) - (cov_R[cstr.atom_ids[0]] + cov_R[cstr.atom_ids[1]])))
            tmpks.append(hab)
        for cbend in bends:
            atb, ata, atc   = cbend.atom_ids
            rab_cov, rac_cov= cov_R[atb] + cov_R[ata], cov_R[ata] + cov_R[atc]
            rab, rac        = linalg.norm(coords[atb*3:atb*3+3] - coords[ata*3:ata*3+3]), linalg.norm(coords[atc*3:atc*3+3] - coords[ata*3:ata*3+3])
            hbend           = 0.089 + 0.11*np.exp(-0.44*(rab + rac - rab_cov - rac_cov))/np.power(rab_cov*rac_cov, -0.42)
            tmpks.append(hbend)
        for ctors in torsions:
            atc, ata, atb, atd  = ctors.atom_ids
            rab, rab_cov        = linalg.norm(coords[ata*3:ata*3+3] - coords[atb*3:atb*3+3]), cov_R[ata] + cov_R[atb]
            nstra = sum(1 for __s in stretches if ata in __s.atom_ids and not atb in __s.atom_ids)
            nstrb = sum(1 for __s in stretches if atb in __s.atom_ids and not ata in __s.atom_ids)
            htors = 0.0015 + 14.0*np.power(nstra + nstrb, 0.57)*np.exp(-2.85*(rab - rab_cov))/np.power(rab*rab_cov, 4.0)
            tmpks.append(htors)
        tmpint_Bmat = calcBmat(coords, stretches + bends + torsions)
        finalhess   = np.dot(tmpint_Bmat.T, np.dot(np.diag(tmpks), tmpint_Bmat))
        if ret_inint:
            finalint_Bmat   = calcBmat(coords, coordlist)
            btpinv          = linalg.pinv2(finalint_Bmat.T)
            bpinv           = linalg.pinv2(finalint_Bmat)
            finalhess       = np.dot(btpinv, np.dot(finalhess, bpinv))
        log_Matrix('Almlöf Hessian guess in internal coordinates:', np.diag(tmpks))
        log_Matrix('Almlöf Hessian guess in current coordinates:', finalhess)
        return finalhess

    def get_Schlegel_Hess(self, coords = None, asymbs = None, coordlist = None, getcart = False): pass

    def get_diag_Hess(self, coords = None, asymbs = None, coordlist = None, getcart = False):
        """ Diagonal Hessian in the internal coordinate frame. Bonds, angles and dihedrals get
        0.5, 0.2 and 0.1 respectively.
        """
        natoms      = len(asymbs)
        new_hess    = None
        if getcart:
            new_hess = np.identity(len(coords))
        else:
            ncoords     = len(coordlist)
            new_hess    = np.zeros((ncoords, ncoords))
            currcoord   = 0
            for coord in coordlist:
                coordtype = coord.type
                if coordtype == 'stretch':
                    new_hess[currcoord, currcoord] = 0.5
                elif coordtype == 'bend' or coordtype == 'LinBend':
                    new_hess[currcoord, currcoord] = 0.2
                elif coordtype == 'twist':
                    new_hess[currcoord, currcoord] = 0.1
                elif coordtype == 'LinBendCart':
                    new_hess[currcoord, currcoord] = 1.0# 0.05
                currcoord += 1
        return new_hess

    def get_Hessian_guess(self, coords_cart = None, asymbs = None, coordlist = None, incart = False, curr_step = None, **kwargs):
        new_hess = None
        isinint  = not incart
        isexact  = False

        approx   = kwargs.pop('hessapp', self.hessapp)[:4]

        if approx == 'SCHL':
            logging.debug('    Getting Schlegel\'s Hessian.')
            new_hess    = self.get_Schlegel_Hess(coords = coords_cart, asymbs = asymbs, coordlist = coordlist, getcart = incart)
            isinint     = False
        elif approx == 'LIND':
            logging.debug('    Getting Lindh\'s Hessian.')
            new_hess    = self.get_Lindh_Hess(coords = coords_cart, asymbs = asymbs, coordlist = coordlist, curr_step=curr_step, inint=isinint)
            # isinint     = False
        elif approx == 'ALML':
            logging.debug('    Getting Almlöfs model Hessian.')
            new_hess    = self.get_Almloef_Hess(coords = coords_cart, asymbs = asymbs, coordlist = coordlist, inint=isinint)
        elif approx == 'SOLE':
            logging.debug('    Getting Soler Hessian model.')
            new_hess    = self.get_Soler_Hess(coords_cart=coords_cart, asymbs=asymbs, curr_step=curr_step)
            isinint     = False
        elif approx == 'DIAG':
            logging.debug('    Getting a diagonal Hessian.')
            new_hess = self.get_diag_Hess(coords = coords_cart, asymbs = asymbs, coordlist = coordlist, getcart = incart)
        elif approx == 'EXACT':
            logging.debug('    Getting the supplied Hessian.')
            # this should actually return a cartesian hessian,
            # but that's the coordinate's doSync()s business.
            if curr_step == None:
                raise Exception('If using an exact hessian, the current structure needs to be supplied!')
            new_hess, isinint = self.getHess(curr_step, coordlist) # supply coordinates?!
            curr_step.exact_hess = True
            log_Matrix('new_hess:', new_hess)
            #print 'Got exact Hessian!'
        else:
            raise ValueError('Hessian {0} not known!'.format(self.hessapp))
        return (new_hess, isinint)

    def get_Hessian(self, coords=None, incart=False, step_hist=None, **kwargs):
        """ Wrapper to 'automatically' get a new, updated Hessian.

        coords              Coordinate class. It's annoying to shift the lists back and forth.
        grad_curr           Flat array with gradient values. Only used for update.
        """
        assert coords is not None
        logging.debug('--- Hessian evaluation')
        new_hess    = None
        stepnum     = None
        steps_noc   = None
        isinint     = not incart
        asymbs      = coords.atom_elems
        if isinstance(coords, Cartesians):
            coordlist = None
        else:
            coordlist   = coords.coords.get_Coords()
        update_steps = kwargs.pop('update_steps', self.update_steps)
        update_type  = kwargs.pop('update_type', self.update_type).upper()
        hess_approx  = kwargs.pop('hess_approx', self.hessapp)
        if step_hist is not None:
            optsteps = step_hist
        else:
            optsteps = self.OptParent.optstep_hist
        hist_start= kwargs.pop('start', 0)
        stepnum   = kwargs.pop('for_step', len(optsteps))
        if self.exact_steps > 0:
            steps_noc   = stepnum%self.exact_steps # steps without calculation of the Hessian
        else:
            steps_noc   = stepnum - 1
        curr_step = optsteps[stepnum-1]
        curr_cart = curr_step.struct.get_flat_Array(units='BOHR')
        logging.debug('    Using the last step ({:8d}) as current.'.format(stepnum))

        # Then let's update with the appropriate steps.
        if update_steps == 0 or stepnum - hist_start == 1:
            new_hess, isinint = self.get_Hessian_guess(curr_cart, asymbs, coordlist, incart, curr_step, hessapp=hess_approx)
        elif update_type != 'NONE' and steps_noc > 0:
            if update_steps > 999 or update_steps >= stepnum - hist_start:
                first_step = hist_start
            elif update_steps < 0:
                first_step = stepnum - 2
            else:
                first_step = (stepnum - hist_start) - (stepnum - hist_start)%(update_steps+1) - 1
            logging.debug('    Updating Hessian using {:8d} steps.'.format(stepnum-first_step-hist_start))
            if update_steps < 0:
                if not incart:
                    new_hess = optsteps[first_step].hess_int
                else:
                    new_hess = optsteps[first_step].hess_cart
            else:
                first_cart = optsteps[first_step].struct.get_flat_Array(units='BOHR')
                new_hess, isinint = self.get_Hessian_guess(first_cart, asymbs, coordlist, incart, optsteps[first_step], hessapp=hess_approx)
                if isinint is incart:
                    #raise ValueError('REALLY, you should handle the coordinates for the Hessian properly, Kumo!')
                    bmat        = calcBmat(first_cart, coords.coords)
                    gmat        = np.dot(bmat, bmat.transpose())
                    gminv_mat   = symm_mat_inv(gmat, redundant = True)
                    gmb_mat     = np.dot(gminv_mat, bmat)
                    #pmat        = np.dot(gmat, gminv_mat)
                    tmpih       = coords.conv_Hess_to_int(hess_mat=new_hess, isinint=False, gmb_mat=gmb_mat, coords_cart=first_cart,
                                                            internals=coords.coords, grad_int=optsteps[first_step].grad_int)
                    new_hess    = tmpih #np.dot(pmat, np.dot(tmpih, pmat))
                    logging.debug('Converting cartesian Hessian to internals.')
                    isinint = True
            # if isinint:
            #     # Also, trying to add the potentials to the Hessian
            #     if Potentials.optimization is not None and len(Potentials.optimization) > 0:
            #         for pot in Potentials.optimization:
            #             uatoms, vatoms  = pot.settings['uatoms'], pot.settings['vatoms']
            #             has_ustretch, ubond = coords.coords.contains_Stretch(*uatoms)
            #             has_vstretch, vbond = coords.coords.contains_Stretch(*vatoms)
            #             if has_ustretch and has_vstretch:
            #                 uidx = coordlist.index(ubond)
            #                 vidx = coordlist.index(vbond)
            #                 new_hess[uidx, uidx] += pot.settings['k']
            #                 new_hess[vidx, vidx] += pot.settings['k']
            #                 new_hess[uidx, vidx] -= pot.settings['k']
            #                 new_hess[vidx, uidx] -= pot.settings['k']
            #                 print 'Added potential between bond ', repr(uidx), ' and ', repr(vidx)
            #             else:
            #                 print 'Oy, cannot find potential distance in internal coordinates!'
            for cstep in xrange(first_step, stepnum-1):
                # To use mutliple steps, we just update successively from the last analytical calculation.
                oldhess = new_hess
                logging.debug('    Updating Hessian using steps {:8d} and {:8d}.'.format(cstep, cstep+1))
                ccart_old = optsteps[cstep].struct.get_flat_Array(units='BOHR')
                ccart_new = optsteps[cstep+1].struct.get_flat_Array(units='BOHR')
                gcart_old = optsteps[cstep].grad_cart
                gcart_new = optsteps[cstep+1].grad_cart
                # # Add potentials if necessary <---- IS NOT! Already done in minimizer.
                # if Potentials.optimization is not None and len(Potentials.optimization) > 0:
                #     for pot in Potentials.optimization:
                #         gcart_old += pot.get_Gradient(flatcoords=ccart_old)
                #         gcart_new += pot.get_Gradient(flatcoords=ccart_new)
                if isinint:
                    #isinint = True
                    # Can we expect the coordinates to always be the same?
                    # I don't think so, therefore we should take care to always transform from cartesians.
                    oldhess = coords.get_working_Hess(hess_int=oldhess, recalc=True, coords_cart=ccart_old)
                    dq      = coords.get_Internals(ccart_new) - coords.get_Internals(ccart_old)
                    dg_curr = coords.conv_Grad_to_int(gcart_new, recalc_matrices=True, coords_cart=ccart_new, project=True)\
                            - coords.conv_Grad_to_int(gcart_old, recalc_matrices=True, coords_cart=ccart_old, project=True)
                else:
                    #isinint = False
                    # TODO: Convert to LNC anew, so that 
                    dq      = ccart_new - ccart_old
                    dg_curr = gcart_new - gcart_old
                    logging.debug('    Updating Hessian in cartesians')
                if update_type == 'BFGS':
                    logging.debug('    Updating Hessian using BFGS.')
                    new_hess = oldhess + self.get_BFGS_correction(oldhess, dg_curr, dq)
                elif update_type == 'FARKASUPDATE':
                    logging.debug('    Doing Update according to Farkas et al.')
                    factor = np.sqrt(self.get_phiBofill(oldhess, dg_curr, dq))
                    corr_bfgs = self.get_BFGS_correction(oldhess, dg_curr, dq)
                    corr_sr1 = self.get_SR1_correction(oldhess, dg_curr, dq)
                    new_hess = oldhess + factor * corr_sr1 + (1.0 - factor) * corr_bfgs
                else:
                    logging.error('    Hessian update type {0} not known!'.format(self.update_type))
        else:
            raise ValueError('ERROR: Something unexpected happened. update_steps: {:5d}\
                            update_type: {:20s} steps_noc: {:5d} self.current != None: {scurr}'\
                            .format(update_steps, update_type, steps_noc, scurr=(self.current != None)))
            #print 'Updated Hessian with {0} steps.'.format(first_step)
        #print 'new_hess:',repr(new_hess)
        # TODO: Maybe just let the saving of the Hessian be done in the coordinates doSync method.
        return (isinint, new_hess)

    def get_BFGS_correction(self, hess_old = None, dgrad = None, dx_curr = None):
        """ Applies the Broyden-Fletcher-Goldfarb-Shanno rank 2 update to the Hessian (not to its inverse).
        Also according to Bakken and Helgaker (as nearly always).
        """
        #print(repr(hess_old))
        print(repr(dgrad))
        print(repr(dx_curr))
        ggt         = np.outer(dgrad, dgrad.transpose()) # transposing actually shouldn't make a difference for vectors
        gtx         = np.inner(dgrad.transpose(), dx_curr)
        GxxtG       = np.dot(hess_old, np.dot(np.outer(dx_curr, dx_curr.transpose()), hess_old))
        xtGx        = np.dot(dx_curr.transpose(), np.dot(hess_old, dx_curr))
        correction  = ggt/gtx - GxxtG/xtGx
        if np.amax(np.absolute(correction)) > 5.0:
            logging.debug('    WARNING: Unreasonable correction size!')
            # log_Vectors_stacked(('dgrad', 'dx_curr'), (dgrad, dx_curr))
            # log_Matrix('    ggt:', ggt, logging.debug)
            # log_Matrix('    GxxtG:', GxxtG, logging.debug)
            logging.debug('    gtx: {:8.2e} xtGx: {:8.2e}'.format(gtx, xtGx))
            logging.debug('    Not updating Hessian!')
            correction = 0.0
        return correction

    def get_SR1_correction(self, hess_old = None, dgrad = None, dx_curr = None):
        """
        dx_curr: last step, i.e. x(k) - x(k-1)
        """
        assert hess_old is not None
        assert dgrad is not None
        assert dx_curr is not None
        dfpHs = -dgrad + np.dot(hess_old, dx_curr)
        correction = - np.outer(dfpHs, dfpHs)/np.dot(dfpHs, dx_curr)
        return correction

    def get_phiBofill(self, hess_old = None, dgrad = None, dx_curr = None):
        assert hess_old is not None
        assert dgrad is not None
        assert dx_curr is not None
        dfpHs = -dgrad + np.dot(hess_old, dx_curr)
        phi = np.power(np.dot(dfpHs, dx_curr), 2)/(np.dot(dfpHs, dfpHs) * np.dot(dx_curr, dx_curr))
        return phi

    def do_Update(self, hess_cart = None, hess_int = None, hess_int_tolog = None):
        """ hess_int_tolog is a Convenience option in the case that we don't want the actually used Hessian
        in the history. This is the case if redundant internals are used, for example. Then the redundant
        part is scaled in the hessian, which shouldn't be used for updates.
        """
        self.current_cart   = hess_cart
        self.current_int    = hess_int
        if hess_int != None:
            self.current = self.current_int
        else:
            self.current = self.current_cart
        if hess_int_tolog != None:
            self.pushLog(hess_cart, hess_int_tolog)
        else:
            self.pushLog(hess_cart, hess_int)
