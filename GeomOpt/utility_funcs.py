
# -*- coding: utf-8 -*-

import logging
import scipy as np
import scipy.linalg as linalg

def dij(a = None, b = None):
    assert a != None
    assert b != None
    retval = 0
    if a == b: retval = 1
    return retval

def zijk(i = None, j = None, k = None):
    assert i != None
    assert j != None
    assert k != None
    retval = 0
    if i == j:
        retval = 1
    elif i == k:
        retval = -1
    return retval

def symm_mat_inv(mat = None, redundant = False):
    #print 'symm_mat_inv'
    #print repr(mat)
    det = 1.0
    evals, evecs = linalg.eigh(mat)
    log_Vector('    Eigenvalues of to be inverted matrix:', evals)

    for val in evals:
      det *= val

    if not redundant and np.absolute(det) < 1.0e-10:
      raise ValueError('Couldn\'t inverse non-generalized matrix!')

    tmpmat = np.zeros(mat.shape)
    for i, e in enumerate(evals):
      if not redundant or np.absolute(e) > 1.0e-10:
        tmpmat[i,i] = 1.0/e
    log_Vector('    Eigenvalues of inverted matrix:', evals)

    retmat = np.dot(evecs, np.dot(tmpmat, evecs.transpose()))
    return retmat

def _are_parallel(uvec = None, vvec = None, thresh = 1.0e-10):
    assert uvec != None
    assert vvec != None
    retval  = True
    a= uvec/np.linalg.norm(uvec)
    b= vvec/np.linalg.norm(vvec)
    if np.absolute(np.absolute(np.dot(a,b)) - 1.0) > thresh:
        logverbose('    Vectors not parallel. a.b={:12.8f}'.format(np.dot(a,b)))
        retval = False
    return retval

def _norm_is_ok(*args, **kwargs):
    if 'min' in kwargs:
        normmin = kwargs['min']
    else:
        normmin = 1.0e-8
    if 'max' in kwargs:
        normmax = kwargs['max']
    else:
        normmax = 1.0e+15
    retval = True
    for vec in args:
        norm = linalg.norm(vec)
        if norm < normmin or norm > normmax:
            retval = False
            break
    return retval

def _calcDist(flatcoords=None, fatom=None, iatom=None):
    return linalg.norm(flatcoords[fatom*3:fatom*3+3] - flatcoords[iatom*3:iatom*3+3])

def _calcAngle(flatcoords = None, latom = None, catom = None, ratom = None):
    lvec        = flatcoords[latom*3:latom*3+3] - flatcoords[catom*3:catom*3+3]
    rvec        = flatcoords[ratom*3:ratom*3+3] - flatcoords[catom*3:catom*3+3]
    try:
        angle_rad = _calc_Angle_of_Vects(lvec, rvec)
        angle_cos = np.cos(angle_rad)
        ldist     = linalg.norm(lvec)
        rdist     = linalg.norm(rvec)
    except ValueError, e:
        print 'Angle: {0}'.format(repr((latom, catom, ratom)))
        raise e
    return ((latom, catom, ratom), angle_cos, angle_rad), (lvec, rvec, ldist, rdist)

def _calc_Angle_of_Vects(vec1 = None, vec2 = None):
    assert vec1 != None
    assert vec2 != None
    angle_rad = None
    v1len   = linalg.norm(vec1)
    v2len   = linalg.norm(vec2)
    if not _norm_is_ok(vec1, vec2):
        raise ValueError('A norm for an angle is either too large or small: v1len = {0}, v2len = {1}'.format(v1len, v2len))
    else:
        angle_cos = np.dot(vec1/v1len, vec2/v2len)
        if angle_cos >= 1.0 - 1.0e-14:
            angle_rad = 0.0
        elif angle_cos <= -1.0 + 1.0e-14:
            angle_rad = np.pi
        else:
            angle_rad = np.arccos(angle_cos)
    return angle_rad

def _calcDihedral(flatcoords = None, matom = None, oatom = None, patom = None, natom = None):
    """
    See: http://math.stackexchange.com/questions/47059/how-do-i-calculate-a-dihedral-angle-given-cartesian-coordinates
    """
    uvec    = flatcoords[oatom*3:oatom*3+3] - flatcoords[matom*3:matom*3+3] # b1
    wvec    = flatcoords[patom*3:patom*3+3] - flatcoords[oatom*3:oatom*3+3] # b2
    vvec    = flatcoords[natom*3:natom*3+3] - flatcoords[patom*3:patom*3+3] # b3
    ulen    = linalg.norm(uvec)
    wlen    = linalg.norm(wvec)
    vlen    = linalg.norm(vvec)
    if not _norm_is_ok(uvec, wvec, vvec):
        raise ValueError('A norm for dihedral {0} is either too large or small: ulen = {1}, wlen = {2}, vlen = {3}'\
                         .format(repr((matom, oatom, patom, natom)), ulen, wlen, vlen))
    ucross  = np.cross(uvec/ulen, wvec/wlen) # n1
    vcross  = np.cross(wvec/wlen, vvec/vlen) # n2
    phiu_sin = linalg.norm(ucross)
    phiv_sin = linalg.norm(vcross)
    # sensibility check if not done beforehand
    # if phiu_sin * phiv_sin < 2.0e-3:
    #     print 'dihedral:', repr((matom, oatom, patom, natom))
    #     print 'phiu_sin:', repr(phiu_sin)
    #     print 'phiv_sin:', repr(phiv_sin)
    #     raise ValueError('At least one angle in the dihedral is ~0. Stopping the calculation.')
    # adjust the sign
    n1          = ucross/linalg.norm(ucross)
    n2          = vcross/linalg.norm(vcross)
    mvec        = np.cross(n1, wvec/wlen)
    tors_rad    = -np.arctan2(np.dot(mvec, n2), np.dot(n1, n2))
    return ((matom, oatom, patom, natom), None, tors_rad), (uvec, wvec, vvec, ulen, wlen, vlen, phiu_sin, phiv_sin)

def _rotateVec_x(vec = None, angle = None):
    assert vec is not None
    assert angle is not None
    rotmat = np.array([[1, 0, 0],
                       [0, np.cos(angle), -np.sin(angle)],
                       [0, np.sin(angle), np.cos(angle)]])
    return np.dot(rotmat, vec)

def _rotateVec_y(vec = None, angle = None):
    assert vec is not None
    assert angle is not None
    rotmat = np.array([[np.cos(angle), 0, np.sin(angle)],
                        [0, 1, 0],
                        [-np.sin(angle), 0, np.cos(angle)]])
    return np.dot(rotmat, vec)

def _rotateVec_z(vec = None, angle = None):
    assert vec is not None
    assert angle is not None
    rotmat = np.array([[np.cos(angle), -np.sin(angle), 0],
                        [np.sin(angle), np.cos(angle), 0],
                        [0 , 0, 1]])
    return np.dot(rotmat, vec)

######################## Logging #######################
logverbose = lambda t: logging.log(logging.VERYVERBOSE, t)

def log_Vector(descr = None, vec = None, func = logverbose, sform = '{:12.8f}'):
    assert descr != None
    assert vec != None
    assert func != None

    func(descr)
    for idx,v in enumerate(vec):
        func(('{:5d} '+sform).format(idx,v))

def log_Vectors_stacked(descr = None, vecs = None, func = logverbose):
    assert descr is not None
    assert vecs is not None
    assert len(descr) == len(vecs)

    func(' '.join('{:20s}'.format(t) for t in descr))
    for idx, vs in enumerate(zip(*vecs)):
        func(' '.join('{:5d} {:14.8f}'.format(idx, v) for v in vs))


def log_Matrix(descr = None, mat = None, func = logverbose):
    assert descr != None
    assert mat != None
    assert func != None

    func(descr)
    for idx, l in enumerate(mat):
        lstr = [ '{0:12.8f}'.format(v) for v in l ]
        func('{0:10d}'.format(idx) + '\n          '.join(' '.join(lstr[i*10:i*10+10]) for i in range(int(np.ceil(len(lstr)/10.0)))))

def log_Debug_files(opt_step = 0, fext='.log', **kwargs):
    import os

    dpath = 'optstep_{:03d}'.format(opt_step)
    if not os.path.isdir(dpath):
        os.mkdir(dpath)
    for key, val in kwargs.iteritems():
        with open(dpath+'/'+key+fext, 'a') as ofile:
            write = lambda txt: ofile.write(txt + '\n')
            try:
                dim = len(val.shape)
                if dim > 1:
                    log_Matrix(descr = '', mat = val, func = write)
                else:
                    log_Vector(descr = '', vec = val, func = write)
            except AttributeError:
                ofile.write(val + '\n')

def log_Progress(text=None, curr=None, cmax=None, steps=10, func=logging.debug):
    percent = 100.0*float(curr)/float(cmax)
    if percent%steps == 0:
        func(text+str(percent)+'%')

##################### Things from papers ###################
def solve_linEq_ON2(y = None, M = None, mbarhook = None, maxit = 20, **kwargs):
    """ O(N2) method to solve y=(M+lS)x
    According to: O. Farkas, H. B. Schlegel, JCP 1998, 109, 7100.
    - y: Solution vector = f
    - M: Hessian
    - First guess: l = f^T * f
    """
    logging.debug('----Solving system of linear equations in O(N2)')
    # initialization
    x = np.zeros(y.shape[0])
    dy = y # error in vector y
    # first guess for the
    mbar = M
    mbarinv = linalg.pinv(M).real
    for k in range(maxit):
        # (2)
        dxtilde = np.dot(mbarinv, dy)
        dytilde = np.dot(mbar, dxtilde)
        sigma = np.dot(dy, dytilde)/np.dot(dytilde, dytilde)
        if np.absolute(sigma) < 1.0e-3:
            dxtilde = dy
        # (3)
        x = x + sigma*dxtilde
        # (4)
        tmpcorr = dxtilde-np.dot(mbarinv, dytilde)
        denom   = np.dot(tmpcorr, dytilde)
        if np.absolute(denom) > 1.0e-5:
            mbarinv = mbarinv + np.outer(tmpcorr, tmpcorr)/denom
        else:
            logverbose('    Skipping mbarinv update because of low denominator: denom = {:12.8e}'.format(denom))
        # (5) Not in the general paper, but used to update lambda for RFO step.
        if mbarhook is not None:
            mbar = mbarhook(x, y, mbar, **kwargs)
        # (6)
        dy = y - np.dot(mbar, x)
        logging.debug('    Step {:5d} sigma={:12.5e} denom={:12.5e} norm(dy)={:12.5e}, dy max={:12.5e}'.format(k, sigma, denom, linalg.norm(dy), dy.max()))
        if linalg.norm(dy) < 1.0e-6 and dy.max() < 1.0e-6:
            break
    return x
