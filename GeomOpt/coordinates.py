
# --* encoding: utf-8 *--

#########################################################
#                                                       #
# Coordinate class. Saves and transforms coordinates    #
# according to the chosen system.                       #
#                                                       #
# Currently offers:                                     #
#                                                       #
# Nothing                                               #
#                                                       #
#########################################################

from __future__ import division
import sys
import copy
import logging

import numpy as np
import scipy.linalg as linalg

from atomprops import Elements
from atomprops import Bohr2Ang

from intcoord_funcs import getBondIDs, get_total_Connection
from intcoord_funcs import getAngleIDs
from intcoord_funcs import getTorsIDs
from intcoord_funcs import calcBmat
from intcoord_funcs import calcKmat
from intcoord_funcs import _kmat_numderiv
from intcoord_funcs import _bmat_numderiv

from utility_funcs import symm_mat_inv, log_Matrix, log_Vector, log_Vectors_stacked, logverbose, log_Debug_files
from PyOpt.Interface.Utils import get_Kwarg

class Coordinates(object):
    """
    Class to store, convert and deliver molecular coordinates.
    The constructor needs the coordinates as input. One may give either the XYZ array
    directly (best would be numpy array with dimension (n, 3)), or provide a string to
    the XYZ-file. If you think that's a bad way to do it, send me an email.
    """
    def __init__(self, optref = None, startcart = None, atomlabels = None, dotest = False, **kwargs):
        if optref == None and not dotest:
            raise KeyError('ERROR: optref not existent!')
        elif not dotest:
            self.OptRef     = optref
            self.pushLog    = optref.OptHist.pushNew_Struct
        self.atomcount      = 0
        self.atom_elems     = None
        self.atom_masses    = None
        self.current         = None         #Link to either current_cart or current_int.
        self.current_cart    = None
        self.current_int     = None

        if startcart == None:
            raise ValueError('Please supply the input coordinates.')
        elif isinstance(startcart, basestring):
            atomlabels, startcart  = self.loadXYZ(startcart)

        if atomlabels != None:
            self.atomcount  = len(atomlabels)
            self.atom_elems = atomlabels
            #Get the masses of the atoms
            masses = []
            for atom in self.atom_elems:
                masses.append(getattr(Elements, atom).mass)
            self.atom_masses = np.array(masses)
            self.current_cart = startcart.flatten()
        else:
            raise ValueError('No atom labels defined!')
        if not dotest:
            self.pushLog(self.current_cart, self.current_int)

    def loadXYZ(self, filename):
        """
        Does what it says. Takes the path to a standard XYZ without any fancy connections or so
        and saves it as a 1-D  numpy array in coordlist.
        Created by Filip Savić
        """
        fp = open(filename, 'r')
        lines = fp.readlines()
        self.atomcount = int(lines[0])

        atom_cords  = []
        atom_elem   = []
        for i in lines[-self.atomcount:]:
            line = i.split()
            atom_elem.append(str(line[0]))
            atom_cords.append([float(line[1]), float(line[2]), float(line[3])])
        fp.close()
        return (atom_elem, atom_cords)

    def update_coord(self, deltax):
        """ Just a placeholder for children classes. Not needed actually, but serves for clarification.
        See: https://groups.google.com/forum/#!topic/comp.lang.python/Ym9PRErGi_s

        Name might be a bit misleading. Adds 'deltax' to self.current_cart, but should also update
        the internal coordinates since they should always refer to the same structure (as much as possible).
        """
        raise NotImplementedError('update_cart has to be overridden by a children\'s class!')

    def determine_Internals(self, coords_cart = None):
        """ Also to be overridden. Normally should convert the self.current_cart coordinates to internal coordinates
        in self.current_int. However, if an array of cartesian coordinates (coords_cart; 3N) is given the transformed
        coordinates shouldn't be saved but returned.
        """
        raise NotImplementedError('determine_Internals has to be overridden by a children\'s class!')

    def doSync(self, hess_ref = None, grad = None):
        """ I just can't decide on a better way to sync the coordinates with the Hessian and stuff.
        E.g. this is responsible to convert the coordinates and Hessian to the right frame.
        """
        raise NotImplementedError('doSync has to be overridden by a children\'s class!')


class Cartesians(Coordinates):
    """ Plain cartesian coordinates. """

    def __init__(self, useBodyFrame = True, **kwargs):
        #Call the parent's own __init__ function with the unpacked 'name = value' options not parsed here.
        super(Cartesians, self).__init__(**kwargs)

        self.useFrame   = useBodyFrame
        self.current    = self.current_cart
        self.hessproj   = None #Hessian projector P to get rid of translation and rotation
        if useBodyFrame:
            self.current_cart, self.hessproj = self.toBodyFrame(self.current_cart, self.atom_masses)
            self.current = self.current_cart

    def update_coord(self, dx):
        #Adding a flat array to a flat array should add element-wise
        self.current_cart = self.current_cart + dx
        #print 'old cartesians:\n'+repr(self.current_cart)
        #Rebuild the body frame and projector (we really need to keep things in sync)
        #self.current_cart, self.hessproj = self.toBodyFrame(self.current_cart, self.atom_masses)
        self.current = self.current_cart
        #print 'new cartesians:\n'+repr(self.current_cart)

    def determine_Internals(self): pass

    def toBodyFrame(self, coords_cart = None, masses = None):
        """ Function to translate and rotate the structure to a new frame so that the origin is in the
        center of mass of the system and the inertia tensor is constant and diagonal (body frame).

        It uses stuff from:
        S. Wilson, Molecular Vibration, 1955, p. 8.
        http://en.wikipedia.org/wiki/Moment_of_inertia#The_inertia_matrix_in_different_reference_frames
        W. H. Miller, N. C. Handy, J. E. Adams, JCP 1980, 72, 99.
        """
        assert coords_cart != None and masses != None
        assert coords_cart.shape[0] == masses.shape[0]*3
        #1. Calulate the center of mass coordinates
        num_atoms   = len(masses)
        mol_mass    = np.sum(masses)
        r_masscen   = masses[0]*coords_cart[:3]
        assert r_masscen.shape[0] == 3
        for idx, mass in enumerate(masses[1:], start=1):
            r_masscen += mass*coords_cart[idx*3:(idx+1)*3]
        r_masscen   = np.divide(r_masscen, mol_mass)
        #2. Move that to origin (body frame)
        body_coords = coords_cart.reshape(num_atoms, 3) - r_masscen #Array broadcasting. Maybe rather loop?
        # print 'body_coords: \n'+repr(body_coords)
        #3. Create inertia tensor (it's symmetric, thus we only need 6 values).
        #   See Wilson page 15, eq. 58--59.
        inert_tensor    = np.zeros((3,3)) #Is kept a lower triangular matrix
        for idx, atom in enumerate(body_coords):
            inert_tensor[0,0] += masses[idx]*(atom[1]**2 + atom[2]**2)  #Σ m*(y²+z²); A
            inert_tensor[1,0] -= masses[idx]*atom[0]*atom[1]    #Σ m*x*y; -F
            inert_tensor[2,0] -= masses[idx]*atom[0]*atom[2]    #Σ m*x*z; -E
            inert_tensor[1,1] += masses[idx]*(atom[0]**2 + atom[2]**2)  #Σ m*(x²+z²); B
            inert_tensor[2,1] -= masses[idx]*atom[1]*atom[2]    #Σ m*y*z; -D
            inert_tensor[2,2] += masses[idx]*(atom[0]**2 + atom[1]**2)  #Σ m*(x²+y²); C
        #   But we still need the upper triangle
        inert_tensor[0,1] = inert_tensor[1,0]
        inert_tensor[0,2] = inert_tensor[2,0]
        inert_tensor[1,2] = inert_tensor[2,1]
        #4. Diagonalize the inertia tensor and rotate molecule to principal frame
        inert_vals, inert_vecs = linalg.eigh(inert_tensor)
        # print 'inert_vals:\n'+repr(inert_vals)
        # print 'inert_vecs:\n'+repr(inert_vecs)
        idx_sort        = np.argsort(inert_vals)[::-1]
        if inert_vals[idx_sort[-1]] < 1.0e-5:
            n_rot       = 2
            #idx_sort    = idx_sort[:n_rot]
        else:
            n_rot       = 3
        inert_mat       = np.diag(inert_vals[idx_sort])
        rot_mat         = inert_vecs[:,idx_sort]
        princip_coords  = np.dot(rot_mat.T, body_coords.T).T
        # print 'idx_sort:\n'+repr(idx_sort)
        # print 'rot_mat: \n'+repr(rot_mat)
        # print 'princip_coords:\n'+repr(princip_coords)
        inv_inert       = symm_mat_inv(inert_mat, redundant = True)
        #print 'inv_inert:\n'+repr(inv_inert)
        #5. Build the matrix with torsional and rotational eigenvectors
        #   This is directly translated from Molpro's rotaway subroutine
        r_vec   = np.zeros((num_atoms*3)*(n_rot+3)).reshape((num_atoms*3, n_rot+3))
        big     = np.zeros(3)
        for i, mass in enumerate(masses):
            sqrt_mass   = np.sqrt(mass)
            idx         = i*3
            cxyz        = princip_coords[i]
            for coord in range(3):
                r_vec[idx+coord, coord] = sqrt_mass #eigenvectors in columns (Molpro in rows)
            for rotvec in range(n_rot):
                big[rotvec]        = 0.0
                big[(rotvec+1)%3]  = -1.0*cxyz[(rotvec+2)%3]
                big[(rotvec+2)%3]  = cxyz[(rotvec+1)%3]
                bigo            = np.dot(rot_mat, big)
                #print 'bigo: '+repr(bigo)
                r_vec[idx:idx+3, 3+rotvec] = sqrt_mass*bigo
                #print 'rot_vec: '+repr(r_vec[idx:idx+3, 3+rotvec])
        #print 'r_vec:\n'+repr(r_vec)
        #   This then needs (according to Molpro's code) to be normalized
        #   But this is automatically done by the QR factorization below
        # for vec in range(3+n_rot):
        #     vnorm = linalg.norm(r_vec[:, vec])
        #     if vnorm > 1.0e-7:
        #         vnorm = 1.0/vnorm
        #     else:
        #         vnorm = 0.0
        #     r_vec[:, vec] *= vnorm
        # r_vec_ortho = r_vec
        # print 'normalized r_vec:\n'+repr(r_vec)
        #   And check for orthogonality (QQt = I)
        #   No, actually just orthogonalize.
        #print '---- tobodyfrmae ---'
        r_vec_ortho, scal_prods = linalg.qr(r_vec)
        r_vec_ortho = r_vec_ortho[:,:3+n_rot]
        #print repr(r_vec)
        #print repr(r_vec_ortho), repr(np.dot(r_vec_ortho.T, r_vec_ortho))
        #print repr(r_vec_ortho), scal_prods
        #print 'orthogonalized r_vec:\n'+repr(r_vec_ortho)
        #print 'R:\n'+repr(scal_prods)
        #6. Build the projector P = 1 - RRt, where R is the orthonormal r_vec
        proj_matrix = np.dot(r_vec_ortho, r_vec_ortho.T)
        #print 'proj_matrix:', repr(proj_matrix)
        #proj_matrix = np.outer(r_vec_ortho, r_vec_ortho)
        #print 'proj_matrix:\n'+repr(proj_matrix)
        proj_matrix = np.identity(num_atoms*3) - proj_matrix
        #print 'final projector\n'+repr(proj_matrix)
        return (body_coords.flatten(), proj_matrix)

    def projectHessian(self, coords_cart=None, carthess = None, masses = None, wantWeight = False):
        """ Applies the previously calculated projector P to the mass-weighted Hessian as in:
                                    K' = P K P^T
        Uses self.atomcount and the locally stored projector matrix self.hessproj. Maybe this should
        also be given as options.

        Input:
        carthess        3N*3N cartesian Hessian. Not mass-weighted.
        masses          N array of atomic masses, in the same order of the Hessian of course.
        wantWeight      Boolean stating that we want the mass-weighted projected Hessian returned.

        Returns:
        projhess        Hessian without translation and rotation.
        """
        #Some sanity checks
        assert carthess is not None
        assert coords_cart is not None
        if masses is None:
            masses = self.atom_masses
        num_atoms = len(masses)
        assert carthess.shape == (3*num_atoms, 3*num_atoms)
        tmphess = np.copy(carthess)
        #1. Mass-weight the Hessian
        for i in range(carthess.shape[0]):
            for j in range(i+1):
                tmphess[i,j] /= np.sqrt(masses[i//3]*masses[j//3])
                tmphess[j,i]  = tmphess[i,j]
        #2. Apply projector
        __, hessproj = self.toBodyFrame(coords_cart, masses)
        # print 'carts:', repr(coords_cart)
        # print 'bodycoords:', repr(__)
        # print '--- tmphess:', repr(tmphess)
        # print '--- hessproj:', repr(hessproj)
        phess = np.dot(hessproj, np.dot(tmphess, hessproj.T))
        #3. Un-mass-weight
        if not wantWeight:
            for i in range(carthess.shape[0]):
                for j in range(i+1):
                    phess[i,j]  *= np.sqrt(masses[i//3]*masses[j//3])
                    phess[j,i]   = phess[i,j] #'assumes' a symmetric Hessian *cough*
        return __, phess

    def _projectRot(self, hess_mat = None):
        #We should have changed coordinates, so rebuild the projector
        #body_coords, hessproj    = self.toBodyFrame(self.current_cart, self.atom_masses)
        proj_hessian                        = self.projectHessian(self.coords_cart, hess_mat, self.atom_masses)
        #self.current                        = self.current_cart
        return proj_hessian

    def doSync(self, HessRef = None, curr_step = None, **kwargs):
        """ If things like translation and rotation needs to be projected out of the Hessian,
        then this function does exactly that. In general, it is supposed to transform the bare
        cartesian coordinates and its derivatives to the working space, if not already done.

        Returns:
        coords, grad, hess              Tuple with these contents (in this order). Dimensions as usual.
                                        (All flat except for hess.) Gradient is only returned if it differs
                                        from the cartesian one though. Otherwise it's None.
        """
        assert None not in (HessRef, curr_step)
        isint, hess_mat    = HessRef.get_Hessian(self, incart = True)
        if not isint:
            # proj_hess   = self._projectRot(hess_mat)
            proj_hess = hess_mat #self.projectHessian(hess_mat, self.atom_masses)
            #mass_array= np.array([self.atom_masses[m//3] for m in range(len(self.atom_masses)*3)])
            proj_grad = curr_step.grad_cart #np.dot(self.hessproj, curr_step.grad_cart * mass_array) / mass_array
        else:
            # complain!
            logging.error('I cannot work with an internal Hessian, since we are in cartesian coordinates!')
        # And update the Hessian here.
        # self.pushLog(self.current_cart, self.current_int)
        curr_step.hess_cart = proj_hess
        self.OptRef.OptHist.pushNew_Grad(curr_step.grad_cart, None)
        HessRef.do_Update(hess_cart = hess_mat, hess_int = proj_hess)
        return (self.current, proj_grad, proj_hess)


class NormalCoordinates(Cartesians):

    normal_basis = None

    def __init__(self, **kwargs):
        super(NormalCoordinates, self).__init__(**kwargs)

    def update_coord(self, dx):
        """ We expect a Δx in normal coordinates, i.e. we need to transform back.
        """
        super(NormalCoordinates, self).update_coord(np.dot(self.normal_basis, dx))
        self.current_int    = np.dot(self.normal_basis.T, self.current_cart)
        self.current        = self.current_int

    def doSync(self, HessRef = None, curr_step = None, **kwargs):
        assert HessRef is not None and curr_step is not None
        #bla, hess_mat           = HessRef.getHess() #TODO: Handle different Hessians
        isint, hess_mat = HessRef.get_Hessian(coords = self, incart = True)
        if not isint:
            carts, proj_hess               = self.projectHessian(self.current_cart, hess_mat, self.atom_masses)
            #print repr(proj_hess)
            diag_hess, norm_basis   = HessRef.get_nonred_Basis(proj_hess)
            active_grad             = np.dot(norm_basis.T, curr_step.grad_cart)
            self.current_int        = np.dot(norm_basis.T, self.current_cart)
            self.current            = self.current_int
            self.normal_basis       = norm_basis
        else:
            logging.error('I cannot work with an internal Hessian, since we are in cartesian (local normal) coordinates!')
        #And some updating/logging
        #print repr(hess_mat)
        #print repr(diag_hess)
        #print curr_step.grad_cart
        curr_step.coords_int = self.current
        curr_step.grad_int = active_grad
        curr_step.hess_int = diag_hess
        curr_step.hess_cart = hess_mat
        self.pushLog(self.current_cart, self.current_int)
        self.OptRef.OptHist.pushNew_Grad(curr_step.grad_cart, active_grad)
        HessRef.do_Update(hess_cart = hess_mat, hess_int = diag_hess)
        return (self.current, active_grad, diag_hess)


class PrimitiveInternals(Coordinates):
    """ Class to solely construct primitive internals, i.e. bonds, angles and dihedrals.
    This is done according to:

    Bakken, Helgaker, J. Chem. Phys., 117, 20, 2002, 9160.

    The other internal coordinate classes should derive from this.
    Parameters, which probably shouldn't need any change:

    covbond_fact    1.3         Factor applied to the sum of the covalent radii. If a distance is lower than the result, it's a bond.
    hbond_fact      0.9         Factor applied to the sum of the van der Waals radii, to decide about hydrogen bonds.
    auxinter_ang    2.0         Maximum distance between atoms (in Ångstrøm) for creating auxiliary interfragment bonds.
    auxinter_fact   1.3         Auxiliary interfragment bond must also be lower than the interfr. bond scaled by this factor.
    auxbond_fact    2.5         Bakken and Helgaker's auxiliary bonds are created if the distance is lower than auxinter_fact
                                times the covalent radii.
    """
    def __init__(self, getextrared = True, *args, **kwargs):
        super(PrimitiveInternals, self).__init__(*args, **kwargs)
        self.coords = None
        self.total_conn     = False
        if 'options' in kwargs:
            self.total_conn = kwargs['options'].pop('total_connection', False)

        self.current_int    = self.determine_Internals(self.current_cart, self.atom_elems, getextrared = getextrared)
        self.current        = self.current_int
        self.extrared       = getextrared
        #print 'Bmat:', repr(self.Bmat)
        #print 'coordlist:',self.list_of_coords

    def rebuild_Internals(self):
        logging.debug('  Rebuilding variables dependent on internal coordinates!')
        logging.debug('    Sorting coordinates.')
        self.coord.sort_Coords()
        logging.debug('    Evaluating coordinate values.')
        self.current_int    = self.coords.get_Coord_Vals(self.current_cart)
        self.current        = self.current_int

    def reset_Internals(self, coords_cart=None, atomlist=None, *args, **kwargs):
        # save the previous twist states
        self.current_int = self.determine_Internals(coords_cart, atomlist, *args, getextrared=self.extrared, **kwargs)
        self.current     = self.current_int
        # make sure the states of twists that are contained in old and new coordinates are retained

    def determine_Internals(self, coords_cart = None, atomlist = None, *args, **kwargs):
        assert coords_cart is not None
        assert atomlist is not None

        if self.coords is not None:
            tstates = self.coords.get_TwistStates()

        intcoords   = None

        logging.debug('    Determining primitive coordinates.')
        if self.total_conn:
            self.coords= get_total_Connection(coords_cart, atomlist)
        else:
            self.coords = getBondIDs(coords_cart, atomlist, *args, **kwargs)
            getAngleIDs(coords_cart, atomlist, self.coords)
            getTorsIDs(coords_cart, atomlist, self.coords)
        self.coords.sort_Coords()
        if 'tstates' in locals():
            self.coords.set_TwistStates(states=tstates)
        
        intcoords = self.coords.get_Coord_Vals(coords_cart)
        logging.debug('    Coordinate search done. Resulting primitive coordinate system:')
        self.coords.print_Coords(flatcoords = coords_cart)
        logging.debug('    Coord stats: {:3d} fragments, {:5d} stretches, {:8d} bends, {:10d} twists.'.format(
                        len(self.coords.list_of_fragments), len(self.coords.get_Stretches()), len(self.coords.get_Bends()), len(self.coords.get_Twists())))

        return intcoords

    def get_Internals(self,coords_cart = None):
        assert coords_cart != None
        return self.coords.get_Coord_Vals(coords_cart)

    def transform_back(self, cint_old = None, ccart_old = None, cint_step = None, bmat = None, **kwargs):
        assert cint_old != None
        assert ccart_old != None
        assert cint_step != None

        logging.debug('-- Back-Transforming Structure')
        allow_noconv    = get_Kwarg(kwargs, 'allow_noconv', True)
        max_iter        = get_Kwarg(kwargs, 'maxit', 25)
        recalc_bmat     = get_Kwarg(kwargs, 'recalc_bmat', False)
        logverbose('allow_noconv={!r:8} recalc_bmat={!r:8} max_iter={:8d}'.format(allow_noconv, recalc_bmat, max_iter))

        converged   = False
        xcart_new   = ccart_old.copy()
        last_rms    = None
        curr_rms    = None
        if recalc_bmat:
            bmatrix= self.get_Bmat(ccart_old)
        else:
            assert bmat != None
            bmatrix= bmat
        btpinvt     = np.linalg.pinv(bmatrix.transpose(), rcond=1.0e-5).transpose()

        self.coords.fix_tors_180(ccart_old)

        qint_target = cint_old + cint_step
        # check if a step would flip an angle
        for i,c in enumerate(self.coords.get_Coords()):
            if c.type == 'bend' and qint_target[i] > np.pi:
                #cint_step[i]= np.pi-cint_old[i]
                logging.debug('    Step inverts Angle! '+repr(c.atom_ids))
            elif c.type == 'twist' and np.absolute(qint_target[i]) > np.pi:
                logging.debug('    WARNING: twist is inverted! '+repr(c.atom_ids))
        dxcart_new  = np.dot(btpinvt, cint_step)
        xcart_guess = ccart_old + dxcart_new
        xcart_new  += dxcart_new
        qint_guess  = self.get_Internals(xcart_guess)
        dqint_new   = qint_target - qint_guess
        # log_Vectors_stacked(('Starting structure', 'Target structure', 'Step'), (cint_old, qint_target, cint_step), logverbose)
        logging.debug('    lastrms currrms diff')
        # log_Debug_files(opt_step=len(self.OptRef.optstep_hist), backtrafo_cintold=cint_old, backtrafo_cintstep=cint_step,
        #                         backtrafo_ccartold=ccart_old, backtrafo_qinttarget=qint_target, backtrafo_qintguess=qint_guess)
        # log_Debug_files(opt_step=len(self.OptRef.optstep_hist), fext='.xyz',
        #                     bt_structures=str(self.atomcount)+'\n\n'
        #                     +'\n'.join('{:5s} {:12.5f} {:12.5f} {:12.5}'.format(a, *v) for a, v in zip(self.atom_elems, (ccart_old*Bohr2Ang).reshape(self.atomcount,3))))
        qint_new = qint_guess
        for i in range(max_iter):
            # if np.iscomplexobj(bmatrix):
            #     with open('complex_debug.log', 'a') as cdfl:
            #         cdfl.write('#### Found complex object in bmatrix!\n')
            # if np.iscomplexobj(btpinvt):
            #     with open('complex_debug.log', 'a') as cdfl:
            #         cdfl.write('#### Found complex object in btpinvt!\n')
            # if np.iscomplexobj(dxcart_new):
            #     with open('complex_debug.log', 'a') as cdfl:
            #         cdfl.write('#### Found complex object in step!\n')
            #         for z,c in enumerate(dxcart_new):
            #             cdfl.write('{:5d} {:20.15f}\n'.format(z, c))
            # tmpdict = {}
            # btit = 'backtrafo_it{:02d}'.format(i)
            # tmpdict[btit+'_bmatrix'], tmpdict[btit+'_btpinvt'] = bmatrix, btpinvt
            # tmpdict[btit+'_xcartnew'], tmpdict[btit+'_dxcartnew'] = xcart_new, dxcart_new
            # tmpdict[btit+'_qintnew'], tmpdict[btit+'_dqintnew'] = qint_new, dqint_new
            # log_Debug_files(opt_step=len(self.OptRef.optstep_hist), **tmpdict)
            # log_Debug_files(opt_step=len(self.OptRef.optstep_hist), fext='.xyz',
            #                 bt_structures=str(self.atomcount)+'\n\n'
            #                 +'\n'.join('{:5s} {:12.5f} {:12.5f} {:12.5}'.format(a, *v) for a, v in zip(self.atom_elems, (xcart_new*Bohr2Ang).reshape(self.atomcount,3))))
            # log_Vectors_stacked(('Internal step','Last dq'), (cint_step, dqint_new), logverbose)
            if recalc_bmat:
                newbmatrix= self.get_Bmat(xcart_new)
                btpinvt= np.linalg.pinv(bmatrix.transpose(), rcond=1.0e-5).transpose()
                # log_Matrix('----btpinvt:', btpinvt)
                # log_Matrix('----New B-matrix:', newbmatrix)
                # log_Matrix('----Difference between old and new B-matrix:', newbmatrix-bmatrix)
                bmatrix = newbmatrix
            # log_Vector('----dxcart_new:', dxcart_new)
            # log_Vector('----dqint_new:', dqint_new)
            last_rms    = np.sqrt(np.mean(np.power(dxcart_new, 2)))
            dxcart_new  = np.dot(btpinvt, dqint_new)
            curr_rms    = np.sqrt(np.mean(np.power(dxcart_new, 2)))
            xcart_new  += dxcart_new
            qint_new    = self.get_Internals(xcart_new)
            dqint_new   = qint_target - qint_new
            logging.debug('{3:5d} {0:12.8e} {1:12.8e} {2:12.8e}'.format(last_rms, curr_rms, np.absolute(curr_rms - last_rms), i))
            if curr_rms < 1.0e-6 or np.absolute(curr_rms - last_rms) < 1.0e-12:
                converged = True
                xcart_final = xcart_new
                qint_final  = qint_new
                break
        else:
            logging.debug('    Back-transformation did not succeed. Taking first guess.')
            if not allow_noconv:
                raise Exception('No convergence in {0} steps of the back-transformation.'.format(max_iter))
            #else:
                #print 'Warning: No convergence in {0} steps of the back-transformation.'.format(max_iter)
            xcart_final = xcart_guess
            qint_final  = qint_guess
        return (converged, curr_rms, xcart_final, qint_final)

class DelocalizedInternals(PrimitiveInternals):

    def __init__(self, **kwargs):
        super(DelocalizedInternals, self).__init__(**kwargs)
        self.Gmat = None
        self.Umat = None
        self.Bmat = None
        self.get_nonred_Space()
        self.is_hess_deloc = False # this is somewhat not the right place

    def get_Bmat(self, coords_cart = None):
        assert coords_cart != None

        if self.Umat == None:
            raise ValueError('Nonredundant space was not initialized yet!')

        bmat_prim   = calcBmat(coords_cart, self.coords)
        bmat_deloc  = np.dot(self.Umat.transpose(), bmat_prim)
        return bmat_deloc

    def get_nonred_Space(self):
        """ Internal one-shot method (according to Baker's procedure) to set up the coordinate space. """
        bmat_prim       = calcBmat(self.current_cart, self.coords)
        self.Gmat       = np.dot(bmat_prim, bmat_prim.transpose())
        evals, evecs    = np.linalg.eigh(self.Gmat)
        colidxs         = [i for i in range(len(evals)) if evals[i] >= 1.0e-10]
        self.Umat       = evecs[:, colidxs[:3*self.atomcount-6]]
        if self.Umat.shape[1] < self.current_cart.shape[0]-6:
            raise ValueError('Only {0} internal coordinates! 3N-6 = {1}'.format(self.Umat.shape[1], self.current_cart.shape[0]-6))
        elif self.Umat.shape[1] > self.current_cart.shape[0]-6:
            raise ValueError('Too many internal coordinates? int: {0}, 3N-6: {1}'.format(self.Umat.shape[1], self.current_cart.shape[0]-6))
        self.Bmat       = np.dot(self.Umat.transpose(), bmat_prim)
        self.current_int= np.dot(self.Umat.transpose(), self.current_int)
        self.current    = self.current_int

    def get_Internals(self, coords_cart = None):
        coords_int      = super(DelocalizedInternals, self).get_Internals(coords_cart)
        coords_deloc    = np.dot(self.Umat.transpose(), coords_int)
        return coords_deloc

    def update_coord(self, dx = None, *args, **kwargs):
        assert dx != None
        res                 = self.transform_back(self.current_int, self.current_cart, dx, self.Bmat)
        self.current_int    = res[3]
        self.current_cart   = res[2]
        self.current        = self.current_int
        return res

    def iterate_Prim_to_Deloc(self, prim_old = None, deloc_old = None, prim_step = None, umat = None, **kwargs):
        assert prim_old != None
        assert deloc_old != None
        assert prim_step != None
        assert umat != None

        allow_noconv    = get_Kwarg(kwargs, 'allow_noconv', True)
        max_iter        = get_Kwarg(kwargs, 'maxit', 25)

        converged   = False
        deloc_new   = deloc_old.copy()
        last_rms    = None
        curr_rms    = None

        prim_target = prim_old + prim_step
        ddeloc_new  = np.dot(umat.transpose(), prim_step)
        deloc_guess = deloc_old + ddeloc_new
        deloc_new  += ddeloc_new
        prim_guess  = np.dot(umat, deloc_guess)
        dprim_new   = prim_target - prim_guess
        for i in range(max_iter):
            last_rms    = np.sqrt(np.mean(np.power(ddeloc_new, 2)))
            ddeloc_new  = np.dot(umat.transpose(), dprim_new)
            curr_rms    = np.sqrt(np.mean(np.power(ddeloc_new, 2)))
            deloc_new  += ddeloc_new
            prim_new    = np.dot(umat, deloc_new)
            dprim_new   = prim_target - prim_new
            print 'Umat back: lastrms={0}, currrms={1}, it={2}'.format(last_rms, curr_rms, i)
            if curr_rms < 1.0e-6 or np.absolute(curr_rms - last_rms) < 1.0e-12:
                converged = True
                deloc_final = deloc_new
                prim_final  = prim_new
                break
        else:
            if not allow_noconv:
                raise Exception('No convergence in {0} steps of the back-transformation.'.format(max_iter))
            #else:
                #print 'Warning: No convergence in {0} steps of the back-transformation.'.format(max_iter)
            deloc_final = deloc_guess
            prim_final  = prim_guess
        return (converged, curr_rms, deloc_final, prim_final)

    def doSync(self, HessRef = None, curr_step = None, doBmatUpdate = True, **kwargs):
        assert HessRef != None
        assert curr_step != None

        # update coordinates in step. Current XYZ is already correct. Done in the 'methods' module.
        curr_step.coords_int = self.current_int

        if doBmatUpdate:
            self.Bmat   = self.get_Bmat(self.current_cart)

        btpinv              = np.linalg.pinv(self.Bmat.transpose())
        # gradient
        curr_step.grad_int  = np.dot(btpinv, curr_step.grad_cart)
        # Hessian
        isint, hess_mat     = HessRef.get_Hessian(coords = self, grad_curr = curr_step.grad_int, asymbs = self.atom_elems,
                                                 coordlist = self.coords.get_Coords(), incart = False, curr_step = curr_step)
        if not isint:
            curr_step.hess_int = np.dot(btpinv, np.dot(hess_mat, btpinv.transpose()))
            self.is_hess_deloc = True
        else: # convert to delocalized ones
            if self.is_hess_deloc:
                curr_step.hess_int = hess_mat
            else:
                curr_step.hess_int = np.dot(self.Umat.transpose(), np.dot(hess_mat, self.Umat)) # IS THAT CORRECT?????
                self.is_hess_deloc = True
            hess_mat = np.dot(self.Bmat.transpose(), np.dot(curr_step.hess_int, self.Bmat))
        # and update the logging
        self.pushLog(self.current_cart, self.current_int)
        self.OptRef.OptHist.pushNew_Grad(curr_step.grad_cart, curr_step.grad_int)
        HessRef.do_Update(hess_cart = hess_mat, hess_int = curr_step.hess_int)

        return (self.current, curr_step.grad_int, curr_step.hess_int)

class RedundantInternals(PrimitiveInternals):

    def __init__(self, **kwargs):
        super(RedundantInternals, self).__init__(**kwargs)
        self.Bmat       = self.get_Bmat(self.current_cart)
        self.Gmat       = np.dot(self.Bmat, self.Bmat.transpose())
        self.Gmat_pinv  = symm_mat_inv(self.Gmat, redundant = True)
        self.Pmat       = np.dot(self.Gmat, self.Gmat_pinv) # projector to make changes physical
        self.Kmat       = None

    def rebuild_Grad_History(self, step_hist=None, recalc_matrices=False):
        assert step_hist is not None
        for i in xrange(len(step_hist)):
            ccart = step_hist[i].struct.get_flat_Array(units='BOHR')
            step_hist[i].coords_int = self.get_Internals(ccart)
            step_hist[i].grad_int = self.conv_Grad_to_int(grad_cart=step_hist[i].grad_cart, recalc_matrices=recalc_matrices,
                                        coords_cart=ccart, project=True)

    def rebuild_Internals(self):
        super(RedundantInternals, self).rebuild_Internals()
        self.Bmat       = self.get_Bmat(self.current_cart)
        self.Gmat       = np.dot(self.Bmat, self.Bmat.transpose())
        self.Gmat_pinv  = symm_mat_inv(self.Gmat, redundant = True)
        self.Pmat       = np.dot(self.Gmat, self.Gmat_pinv) # projector to make changes physical
        self.Kmat       = None
        self.rebuild_Grad_History(step_hist=self.OptRef.optstep_hist, recalc_matrices=True)

    def reset_Internals(self, coords_cart=None, *args, **kwargs):
        super(RedundantInternals, self).reset_Internals(coords_cart, self.atom_elems, *args, **kwargs)
        self.Bmat       = self.get_Bmat(coords_cart)
        self.Gmat       = np.dot(self.Bmat, self.Bmat.transpose())
        self.Gmat_pinv  = symm_mat_inv(self.Gmat, redundant = True)
        self.Pmat       = np.dot(self.Gmat, self.Gmat_pinv) # projector to make changes physical
        self.rebuild_Grad_History(step_hist=self.OptRef.optstep_hist, recalc_matrices=True)
        self.Kmat       = _kmat_numderiv(coords_cart, self.coords, self.OptRef.optstep_hist[-1].grad_int)

    def get_Bmat(self, coords_cart = None):
        assert coords_cart is not None
        bmat = calcBmat(coords_cart, self.coords)
        #numbmat= _bmat_numderiv(coords_cart, self.coords)
        #log_Matrix('----bmat-numbmat:', bmat-numbmat, logging.debug)
        # if 'optstep_hist' in self.OptRef.__dict__:
        #     log_Debug_files(opt_step=len(self.OptRef.optstep_hist), bmat_svd=linalg.svd(bmat, compute_uv=False), numbmat_svd=linalg.svd(numbmat, compute_uv=False),
        #                     bmats_all=bmat, numbmats_all=numbmat, bmat_numdiff_all=(bmat-numbmat))
        # print 'Rank of Bmat:', linalg.matrix_rank(bmat)
        # if linalg.matrix_rank(bmat) < 3*self.num_atoms-6:
        #     print 'WARNING: rank of B is too small:'
        return bmat

    def conv_Hess_to_cart(self, hess_mat = None, coords_cart = None, grad_int = None):
        assert hess_mat is not None
        assert coords_cart is not None
        assert grad_int is not None
        
        bmat = self.get_Bmat(coords_cart)
        kmat = calcKmat(coords_cart, self.coords, grad_int)
        numkmat = _kmat_numderiv(coords_cart, self.coords, grad_int)
        diff = np.absolute(kmat - numkmat)
        # log_Matrix('    conv_Hess_to_cart: Kmat difference (max = {:12.5e})'.format(diff.max()), diff)
        assert bmat.shape[0] == hess_mat.shape[0]
        return np.dot(bmat.transpose(),np.dot(hess_mat, bmat)) + kmat


    def conv_Hess_to_int(self, hess_mat = None, isinint = False, gmb_mat = None, **kwargs):
        assert hess_mat is not None

        do_recalc = kwargs.pop('recalc_matrices', False)
        do_project= kwargs.pop('project', False)

        if gmb_mat is None or do_recalc:
            assert 'coords_cart' in kwargs
            bmat    = self.get_Bmat(kwargs['coords_cart'])
            gmat    = np.dot(bmat, bmat.T)
            gminv   = symm_mat_inv(gmat, redundant=True)
            gmb_mat = np.dot(gminv, bmat)
        if do_recalc and do_project:
            pmat    = np.dot(gmat, gminv)
        elif 'pmat' in kwargs:
            pmat    = kwargs['pmat']
        else:
            pmat    = None

        if not isinint:
            if 'kmat' in kwargs:
                kmat = kwargs['kmat']
            elif 'grad_int' in kwargs and 'coords_cart' in kwargs and 'internals' in kwargs:
                kmat = _kmat_numderiv(kwargs['coords_cart'], kwargs['internals'], kwargs['grad_int'])
            else:
                kmat = self.Kmat
            hess_int = np.dot(gmb_mat, np.dot(hess_mat, gmb_mat.T))
        else:
            hess_int = hess_mat
        if do_project:
            hess_int = np.dot(pmat, np.dot(hess_int, pmat))
        #hess_phys = np.dot(self.Pmat, np.dot(hess_int, self.Pmat))
        return hess_int

    def conv_Grad_to_int(self, grad_cart = None, gmb_mat = None, recalc_matrices = False, coords_cart = None, project = False):
        assert grad_cart is not None
        pmat = self.Pmat
        if recalc_matrices:
            assert coords_cart is not None
            assert gmb_mat is None
            bmat = self.get_Bmat(coords_cart)
            gmat = np.dot(bmat, bmat.transpose())
            gminv_mat = symm_mat_inv(gmat, redundant = True)
            gmb_mat = np.dot(gminv_mat, bmat)
            if project:
                pmat = np.dot(gmat, gminv_mat)
        elif gmb_mat is None:
            gmb_mat = np.dot(self.Gmat_pinv, self.Bmat)
        grad_int = np.dot(gmb_mat, grad_cart)
        if project:
            grad_int = np.dot(pmat, grad_int)
        return grad_int

    def get_working_Grad(self, grad_int=None, **kwargs):
        assert grad_int is not None
        assert grad_int.shape[0] == self.current.shape[0]
        do_recalc = kwargs.pop('recalc', True)
        if do_recalc:
            assert 'coords_cart' in kwargs
            bmat    = self.get_Bmat(kwargs['coords_cart'])
            gmat    = np.dot(bmat, bmat.T)
            gminv   = symm_mat_inv(gmat, redundant=True)
            pmat    = np.dot(gmat, gminv)
        else:
            pmat    = self.Pmat
        return np.dot(pmat, grad_int)

    def get_working_Hess(self, hess_int=None, **kwargs):
        assert hess_int is not None
        assert hess_int.shape[0] == self.current.shape[0]
        do_recalc = kwargs.pop('recalc', True)
        if do_recalc:
            assert 'coords_cart' in kwargs
            bmat    = self.get_Bmat(kwargs['coords_cart'])
            gmat    = np.dot(bmat, bmat.T)
            gminv   = symm_mat_inv(gmat, redundant=True)
            pmat    = np.dot(gmat, gminv)
        else:
            pmat    = self.Pmat
        return np.dot(pmat, np.dot(hess_int, pmat)) #+ 1000.0*(np.identity(hess_int.shape[0])-self.Pmat)

    def update_coord(self, dx = None, *args, **kwargs):
        """ We're getting internal coordinate steps here. So this is what
        we should do:

        Don't calculate the new internal coordinates by q_old + dq!
        1.) Iteratively transform the step back to cartesian coordinates
        1.a) Create a first guess for the cartesian coords using the step
             in internal coordinates (dx).
        1.b) Get new internal coordinates and calculate the difference of
             the step (dx) to the step calculated from the new int. coords.
        1.c) Use this difference as the new step for a better guess.
        1.d) Do 1.b and 1.c again until it converges, the iterations exceed
             25 steps or use the first guess from 1.a if the last step from
             1.b is larger than the first.
        2.) Calculate new internal coordinates
        3.) Build B matrix, its inverse and the projector <-- in doSync? YES!
        """
        logging.debug('*** Doing back-transformation')
        logging.debug('    {0:4s} {1:12s} {2:12s} {3:12s}'.format('Its', 'RMS(new)', 'RMS(old)', 'RMSDIFF'))
        logging.debug('Current step:\n{0}'.format('\n'.join('{0:12.8f}'.format(v) for v in dx)))
        logging.debug('Internal coordinates before transformation:\n{0}'.format('\n'.join('{0:12.8f}'.format(v) for v in self.current_int)))

        assert dx != None
        if 'step_cart' not in kwargs:
            res                 = self.transform_back(self.current_int, self.current_cart, dx, self.Bmat, recalc_bmat=False)
            self.current_int    = res[3]
            self.current_cart   = res[2]
            self.current        = self.current_int
        else:
            # expect dx to be internal step
            res = None
            self.current_int += dx
            self.current_cart+= kwargs['step_cart']
            self.current      = self.current_int
        logging.debug('    Internal coordinates after transformation:\n{0}'.format('\n'.join('{0:12.8f}'.format(v) for v in self.current_int)))
        return res

    def doSync(self, HessRef = None, curr_step = None, doBmatUpdate = True, **kwargs):
        """
        1.) Recalc the B and P matrix if necessary
        2.) Get the current Hessian
        3.) Convert Hessian and gradient to internal coordinates
        4.) Project Hessian and gradient to a physical meaning ;)
        """
        assert HessRef is not None
        assert curr_step is not None

        curr_step.coords_int = self.current
        hess_hist_start      = kwargs.pop('hist_start', 0)

        if doBmatUpdate:
            self.Bmat       = self.get_Bmat(self.current_cart)
            self.Gmat       = np.dot(self.Bmat, self.Bmat.transpose())
            self.Gmat_pinv  = symm_mat_inv(self.Gmat, redundant = True)
            self.Pmat       = np.dot(self.Gmat, self.Gmat_pinv)
            # bpinv = linalg.pinv2(self.Bmat)
            # opmat = np.dot(self.Bmat, bpinv)
            # log_Matrix('Difference between Orca P and Bakken P:', self.Pmat-opmat, logging.debug)
            #print 'Bmat:', repr(self.Bmat)
            #print 'Projector:',repr(self.Pmat)
        # for the conversion we need the transposed pseudo-inverse of B
        GmB = np.dot(self.Gmat_pinv, self.Bmat)
        # btpinv = linalg.pinv2(self.Bmat.T)
        # log_Matrix('GmB', GmB, logging.debug)
        # log_Matrix('btpinv', btpinv, logging.debug)
        # log_Matrix('Difference between GmB and btpinv:', GmB-btpinv, logging.debug)
        #BGm         = np.dot(self.Bmat.transpose(), self.Gmat_pinv) # the same as BGm.T !!!
        curr_step.grad_int = self.conv_Grad_to_int(curr_step.grad_cart, GmB, project=True)
        if doBmatUpdate:
            # self.Kmat = calcKmat(self.current_cart, self.coords, curr_step.grad_int)
            self.Kmat = _kmat_numderiv(self.current_cart, self.coords, curr_step.grad_int)
        isint, hess_mat = HessRef.get_Hessian(coords=self, incart=False, start=hess_hist_start)
        #print 'Hessian:\n', repr(hess_mat)
        hess_int = self.conv_Hess_to_int(hess_mat, isint, GmB, project=True, recalc_matrices=True, coords_cart=self.current_cart)
        #hess_int = np.dot(self.Pmat, np.dot(hess_int, self.Pmat)) + 1000.0*(np.identity(hess_int.shape[0])-self.Pmat)
        curr_step.hess_int = hess_int
        if isint:
            hess_mat    = None
        self.pushLog(self.current_cart, self.current_int)
        self.OptRef.OptHist.pushNew_Grad(curr_step.grad_cart, curr_step.grad_int)
        HessRef.do_Update(hess_cart = hess_mat, hess_int = hess_int)
        return (self.current, curr_step.grad_int, hess_int)
