
# -*- coding: utf-8 -*-

OBSOLETE!!!!!!!
-> I think this is too much for reading numerical restart files. Could use this just as a configuration
file to start calculations. But not for reading and writing massive numerical data.

import os, ConfigParser
import scipy as np

class RestartFile(object):
    """ A class for a restart file, obviously. Luckily Python always has something up its sleeve,
    so this class makes use of Python's 'ConfigParser'. That means that the restart file should
    look like a Windows .ini file with sections and its corresponding key=value pairs. Of course
    things like matrices contain loads of values and the ConfigParser does not support those types,
    especially not numpy's arrays. Therefore an array should be handled by defining the number of
    dimensions, the dimension sizes for each dimension and the values should be comma seperated.

    Some values which are going to be saved during an optimization:

    - The last hessian
    - The last gradient
    - All structures
    - All energies
    - Configurations (method, Hessian, etc.)
    """

    def __init__(self, fpath = 'pyopt_geomopt_restart.dat'):
        self.saved_data = ConfigParser.ConfigParser()
        self.arr_char   = ','
        self.fpath      = fpath
        self.readconf   = False

        if os.path.isfile(fpath):
            self.saved_data.read(fpath)
            self.readconf = True

    def restart_Opt(self):
        restart_dict = {}
        if self.readconf:
            # get the configuration
            restart_dict['method']  = self.saved_data.get('Settings', 'Method')
            restart_dict['hessian'] = self.saved_data.get('Settings', 'Hessian')
            restart_dict['coords']  = self.saved_data.get('Settings', 'Coords')
            # and the important stuff
            restart_dict['num_steps']   = self.saved_data.getint('Settings', 'stepcount')
            restart_dict['atoms_list']  = self.saved_data.get('Settings', 'atoms').split(self.arr_char)
            restart_dict['num_atoms']   = len(atoms_list)
            restart_dict['last_struct'] = np.fromstring(self.saved_data.get('OptStep {0}'.format(num_steps-1), 'structure'), sep = self.arr_char)
            try:
                restart_dict['last_grad']   = np.fromstring(self.saved_data.get('OptStep {0}'.format(num_steps-1), 'gradient'), sep = self.arr_char)
                restart_dict['last_hess']   = np.fromstring(self.saved_data.get('OptStep {0}'.format(num_steps-1), 'hessian'), sep = self.arr_char).reshape((num_atoms*3, num_atoms*3))
            except ConfigParser.NoOptionError:
                if last_grad == None and self.saved_data.has_option('In Progress', 'grad_vals'):
                    # this gradient didn't finish in time. Let's restore it by loading the values and the corresponding indices.
                    restart_dict['tmpgrad_idxs'] = np.fromstring(self.saved_data.get('In Progress', 'grad_idxs'), dtype = int, sep = self.arr_char)
                    restart_dict['tmpgrad_vals'] = np.fromstring(self.saved_data.get('In Progress', 'grad_vals'), sep = self.arr_char)
                if last_hess == None and self.saved_data.has_option('In Progress', 'hess_vals'): pass

        return restart_dict

    def write_IP(self, arrtype = 'grad', vallist = None):
        """ Takes the type that's saved and a list of (index, value) tuples.
        """
        assert vallist != None

        curridxs = ''
        currvals = ''
        addidxs = ','.join(str(i) for i, v in vallist)
        addvals = ','.join('{0:12.8f}'.format(v) for i, v in vallist)

        if not self.saved_data.has_section('In Progress'):
            self.saved_data.add_section('In Progress')

        if self.saved_data.has_option('In Progress', arrtype + '_idxs'):
            curridxs = self.saved_data.get('In Progress', arrtype + '_idxs') + ','
        self.saved_data.set('In Progress', arrtype + '_idxs', curridxs + addidxs)

        if self.saved_data.has_option('In Progress', arrtype + '_vals'):
            currvals = self.saved_data.get('In Progress', arrtype + '_vals') + ','
        self.saved_data.set('In Progress', arrtype + '_vals', currvals + addvals)

        with open(self.fpath, 'wb') as conf:
            self.saved_data.write(conf)