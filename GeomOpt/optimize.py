
# -*- coding: utf-8 -*-
#############################################
#											#
# Implements the top-level optimization		#
# class. This offers the interface or all	#
# the offered optimization algorithms.		#
#											#
#############################################

import os
import logging
import scipy as np
import scipy.linalg as linalg
import PyOpt.Potentials as Potentials
from PyOpt.GeomOpt.methods import *
from PyOpt.GeomOpt.hessians import *
from PyOpt.GeomOpt.coordinates import *
from PyOpt.GeomOpt.history import *
from PyOpt.GeomOpt.Structure import Molecule
from PyOpt.GeomOpt.utility_funcs import log_Debug_files, log_Vector, _calcAngle, _calc_Angle_of_Vects

class OptStep(object):

    def __init__(self):
        self.struct     = None
        self.coords_int = None
        self.energy     = None
        self.energy_real= None
        self.grad_real  = None # cartesian
        self.grad_cart  = None
        self.grad_int   = None
        self.hess_cart  = None
        self.hess_int   = None
        self.exact_hess = False

coord_classes = { 'CART': Cartesians, 'NORM': NormalCoordinates, 'REDU': RedundantInternals, 'DELO': DelocalizedInternals }
method_classes = { 'RF': RationalFunction, 'AHRS': AHRS, 'RSRFO': RSRFO, 'FarkasRFO': FarkasRFO, 'GDIIS': SimpleDIIS, 'FarkasGDIIS': GDIIS, 'GEDIIS': GEDIIS, 'Hybrid': HybridOpt }

class Optimizer(object):
    """
    """
    def __init__(self, molfile = None, method = 'RF', hess = 'DIAG', hessupd = 'BFGS', coordsys = 'DELOC',
                getEner = None, getGrad = None, getHess = None, opt_options=None,
                    **kwargs):
        """
        Loads coordinates, creates initial Hessian.
        molfile         Structure as a Molecule class.
        method          Optimization algorithm. Currently: RF
        hess            Approximation to the Hessian. Currently: own (supply own with getHess)
        getEner         Callback to retrieve energy. Should accept cartesian coordinates as n, 3 array and an n size array of atom labels.
        getGrad         Callback to retrieve gradient in 1-D array. In cartesian coordinates.
        getHess         Callback to retrieve Hessian in 2-D array. In cartesian coordinates.
        """
        np.set_printoptions(linewidth=200,precision=8,suppress=True)
        self.options    = opt_options
        self.start_mol  = molfile
        self.coordsys   = coordsys.upper()
        self.hessapp    = hess.upper()
        self.getEnergy  = getEner
        self.getGrad    = getGrad
        self.getHess    = getHess
        self.OptHist    = OptimizationHistory(self, system = self.coordsys)
        if self.coordsys[:4] not in coord_classes:
            raise ValueError('There is no coordinate system for {0}. Please check the options.'.format(coordsys))
        else:
            self.Coords = coord_classes[self.coordsys[:4]](optref = self, startcart = molfile.get_flat_Array(units = 'BOHR'), atomlabels = molfile.get_Elem_List(),
                            getextrared = self.options['extrared'], options=self.options, **kwargs)
            #self.Coords.coords.print_Coords(logging.info, self.Coords.current_cart)
        self.Hessian    = Hessian(self, approx = hess, callback = getHess, update_type = hessupd, **kwargs)
        #if hess == 'own':
        #    self.Hessian.set_own_Hess()
        self.Method = method_classes[method](self, smax=self.options['smax'], **kwargs)

        self.optstep_hist            = [OptStep()]
        self.optstep_hist[-1].struct = molfile
        # Do some logging here
        logging.info('Initialized Optimizer. Doing the optimization with the following options:')
        logging.info('Method:       {0}'.format(method))
        logging.info('Coordinates:  {0}'.format(coordsys[:4]))
        logging.info('Hessian:      {0}'.format(hess))
        logging.info('Update:       {0}'.format(hessupd))

    def _apply_Potentials(self, coords_cart=None, **kwargs):
        assert coords_cart is not None

        energy  = kwargs.pop('energy', None)
        grad    = kwargs.pop('gradient', None)
        if Potentials.optimization is not None:
            for pot in Potentials.optimization:
                if energy is not None:  energy = energy + pot.get_Energy(flatcoords=coords_cart)
                if grad is not None:    grad   = grad + pot.get_Gradient(flatcoords=coords_cart)
        return energy, grad

    def minimize(self, enthresh = 1.0e-6, gradthresh = 3.0e-4, stepsthresh = 3.0e-4, maxsteps = 100):
        """
        """
        logging.info('----------------------- STARTING MINIMIZATION ------------------------')
        deltaen     = None
        deltastep   = None
        isconverged = False
        nsteps      = 0
        last_reset  = 0
        gradmax     = None
        gradnorm    = None
        curr_sp     = self.optstep_hist[0]
        #Initial calculation
        curr_sp.energy_real = self.getEnergy(self.start_mol)
        curr_sp.grad_real   = self.getGrad(self.start_mol)
        curr_sp.energy, curr_sp.grad_cart = self._apply_Potentials(coords_cart=self.start_mol.get_flat_Array(units='BOHR'),
                                            energy=curr_sp.energy_real, gradient=curr_sp.grad_real)
        curr_sp.coords_int, curr_sp.grad_int, curr_sp.hess_int = self.Coords.doSync(HessRef = self.Hessian, curr_step = curr_sp)
        # and the usual logging
        logging.debug('*** Starting XYZ in Angstrøm\n{0}'.format(curr_sp.struct.get_String(units = 'ANG')))
        logging.debug('Current Energy: {0:15.8f}'.format(curr_sp.energy))
        logging.debug('Current Cartesian Gradient:')
        for a, c in zip(self.start_mol.get_Elem_List(), curr_sp.grad_cart.reshape((self.start_mol.natoms, 3))):
            logging.debug('{0:5s} {1:12.8f} {2:12.8f} {3:12.8f}'.format(a, c[0], c[1], c[2]))
        logging.info('{0:10} {1:16} {2:12} {3:12} {4:12} {5:12} {6:12} {7:8} {8:12} {9}'.format(
                     'Iteration', 'Energy/Eh', 'dE/a.u.', 'max(g)', 'max(gint)', '|g|',
                                    'RMS dx', '|dx|', 'rigidity', 'isConverged'))
        if self.options['write_trj']:
            # remove old file if it exists
            trjfile=open('optimization_trajectory.trj', 'w+')
            trjfile.write(curr_sp.struct.get_String(units='ANG', nogroup=True, noheader=False))
        dbgfile = open('optimization_debug.log', 'w+')

        while not isconverged and nsteps < maxsteps:
            logging.debug('###########              Step {0:6d}                   ###########'.format(nsteps))
            #We already have full information: coordinates, energy, gradient, Hessian
            #Therefore we first need a step
            oldcart    = self.Coords.current_cart
            oldint     = curr_sp.coords_int
            good_step  = False
            activehess = curr_sp.hess_int # self.Coords.get_working_Hess(curr_sp.hess_int)
            activegrad = curr_sp.grad_int # self.Coords.get_working_Grad(curr_sp.grad_int)
            while not good_step: # Let's search for an appropriate step!
                newintstep = self.Method.doStep(curr_hess = activehess, curr_grad = activegrad, curr_vars = self.Coords.current)
                # log_Debug_files(opt_step = nsteps, start_struct=curr_sp.struct.get_String(units='ANG', nogroup=True),
                #                                     hess_active=activehess, grad_active=activegrad, internal_step=newintstep,
                #                                     grad_cart=curr_sp.grad_cart)
                # if not isinstance(self.Coords, Cartesians):
                #     ### scale the step until we're out of linear regions
                #     tmpscales, doscale = [], False
                #     for c, v in zip(self.Coords.coords.get_Coords(), curr_sp.coords_int + newintstep):
                #         if c.type == 'bend' and v > np.pi-0.1:
                #             tmpscales.append((v-np.pi+0.1)/v)
                #             doscale = True
                #     if doscale:
                #         log_Vector('    Step scales:', tmpscales)
                #         logging.debug('    Scaling step with {:10.5f} because of linear regions.'.format(min(tmpscales)))
                #         newintstep *= min(tmpscales)
                sd_deviation = _calc_Angle_of_Vects(newintstep, activegrad)*180.0/np.pi
                # logging.debug('   Angle between step and redu. gradient: {:12.6f}'.format(_calc_Angle_of_Vects(newintstep, curr_sp.grad_int)*180.0/np.pi))
                logging.debug('   Angle between step and active gradient: {:12.6f}'.format(sd_deviation))
                # if sd_deviation < 100.0: # prevent near-orthogonal steps
                #     # if last_reset < nsteps:
                #     #     logging.debug('   Too large deviation from gradient direction. Resetting Hessian.')
                #     #     isinint, tmphess = self.Hessian.get_Hessian(coords=self.Coords, incart=False, start=nsteps)
                #     #     # check and treat if Hessian is in cartesians <-- TODO!
                #     #     activehess = self.Coords.conv_Hess_to_int(tmphess, isinint, gmb_mat=None, project=True, recalc_matrices=True, coords_cart=oldcart)
                #     # else:
                #     logging.debug('   Resetting Hessian did not work. Taking steepest descent.')
                #     activehess = np.identity(activegrad.shape[0])
                #     last_reset = nsteps
                # else:
                #     logging.debug('   Step for iteration {:3d} should be ok.'.format(nsteps))
                good_step = True

            self.Coords.update_coord(newintstep)
            newcart = self.Coords.current_cart
            #In case of elimrot == True, the gradient is transormed, so we should evaluate it /after/ the step. Weird, I agree.
            logging.debug('The latest (internal) gradient:\n{0}'.format(curr_sp.grad_int))
            gradmax     = np.absolute(curr_sp.grad_cart).max()
            gradmax_int = np.absolute(activegrad).max()
            gradnorm    = linalg.norm(curr_sp.grad_cart)
            nsteps += 1
            # except ValueError as e:
            #     logging.error('!!! ERROR: There was a problem in the step calculation! {0}'.format(e))
            #     break
            #If it was successful (it was, if we arrived here), get values for the new structure.
            deltastep   = newcart - oldcart
            newmol      = Molecule(copyfrom = self.start_mol)
            # print 'bli', repr(self.start_mol.natoms), repr(newmol.natoms)
            newmol.set_coords(newcart.reshape(self.Coords.atomcount, 3), units = 'BOHR')
            self.optstep_hist.append(OptStep())
            curr_sp = self.optstep_hist[-1]
            curr_sp.struct      = newmol
            curr_sp.energy_real = self.getEnergy(newmol)
            curr_sp.energy, __  = self._apply_Potentials(coords_cart=newcart, energy=curr_sp.energy_real)
            deltaen     = curr_sp.energy - self.optstep_hist[-2].energy
            if self.options['write_trj']:
                trjfile.write(curr_sp.struct.get_String(units='ANG', nogroup=True, noheader=False))
                trjfile.flush()
            logging.debug('   Angle between step and cart gradient: {:12.6f}'.format(_calc_Angle_of_Vects(deltastep, self.optstep_hist[-2].grad_cart)*180.0/np.pi))
            logging.debug('   Cartesian step length: {:12.8f}'.format(linalg.norm(deltastep)))
            # log_Debug_files(opt_step=nsteps, new_struct=curr_sp.struct.get_String(units='ANG', nogroup=True),
            #                                 cartesian_step=deltastep)
            # let's update the trust radius
            if self.options['trust_update']:
                self.Method.smax = update_TrustRadius(curr_t = self.Method.smax, dE = deltaen, curr_step = newintstep, curr_grad = activegrad, curr_hess = activehess)
            #Now, according to F. Eckert (J. Comp. Chem., 18, 1473-1483 (1997)), this is the point
            #at which Molpro is checking convergence.
            isconverged = self.checkConvergence(deltaen, deltastep, self.optstep_hist[-2].grad_cart, newcart, enthresh, gradthresh, stepsthresh)
            # General logging
            rigidity = linalg.norm(oldint + newintstep - self.Coords.current)/linalg.norm(newintstep)
            logging.debug('************ Current Convergence (optimization values) **************')
            logging.info('{0:10d} {1:16.8f} {2:12.8f} {3:12.8f} {4:12.8f} {5:12.8f} {6:12.8f} {7:8.5f} {8:12.6f} {9!r}'.format(
                        nsteps, curr_sp.energy, deltaen, gradmax, gradmax_int, gradnorm,
                        np.sqrt(np.sum(np.power(np.mean(deltastep) - deltastep, 2))/deltastep.shape[0]), linalg.norm(deltastep), rigidity, isconverged))
            #Everything cleared to update the other stuff. This is also the point to stop if maxsteps is reached.
            curr_sp.grad_real       = self.getGrad(newmol)
            __, curr_sp.grad_cart   = self._apply_Potentials(coords_cart=newcart, gradient=curr_sp.grad_real)
            #################### We need to check if the step is useful, otherwise restart?
            if not isinstance(self.Coords, Cartesians):
                ### now check if we need to rebuild the coordinate system
                # reset_coords = False
                # for coord, val in zip(self.Coords.coords.get_Coords(), self.Coords.current_int):
                #     if coord.type == 'bend' and val >= np.pi-0.2:
                #         reset_coords = True
                #         break
                #     elif coord.type == 'LinBendCart':
                #         res = _calcAngle(newcart, *coord.atom_ids)
                #         if res[0][2] <= np.pi-0.8:
                #             reset_coords = True
                #             break
                if self.options['reset_coords'] and nsteps%self.options['reset_coords_steps'] == 0:
                    logging.debug('    WARNING: Resetting internals in step {:3d}'.format(nsteps))
                    #logging.debug('   WARNING: Step flips bend! Switching to linear bend coordinates.')
                    self.Coords.reset_Internals(coords_cart=newcart)
                    self.Hessian.rebuild_Internal_Hessians(coords=self.Coords, optsteps=self.optstep_hist)
            # Debug logging
            logging.debug('*** Starting XYZ in Angstrøm\n{0}'.format(curr_sp.struct.get_String(units = 'ANG')))
            logging.debug('Current Energy (active): {0:15.8f}'.format(curr_sp.energy))
            logging.debug('Current Cartesian Gradient (active):')
            for a, c in zip(self.start_mol.get_Elem_List(), curr_sp.grad_cart.reshape((self.start_mol.natoms, 3))):
                logging.debug('{0:5s} {1:12.8f} {2:12.8f} {3:12.8f}'.format(a, c[0], c[1], c[2]))
            logging.debug('Current Energy (real): {0:15.8f}'.format(curr_sp.energy_real))
            logging.debug('Current Cartesian Gradient (real):')
            for a, c in zip(self.start_mol.get_Elem_List(), curr_sp.grad_real.reshape((self.start_mol.natoms, 3))):
                logging.debug('{0:5s} {1:12.8f} {2:12.8f} {3:12.8f}'.format(a, c[0], c[1], c[2]))
            curr_sp.coords_int, curr_sp.grad_int, curr_sp.hess_int = self.Coords.doSync(HessRef = self.Hessian, curr_step = curr_sp, hess_hist_start=last_reset)
            dbgfile.write('#cold cnew cstep gold gnew for step {:5d}\n'.format(nsteps))
            for cold, cnew, cstep, gold, gnew in zip(self.optstep_hist[-2].coords_int, curr_sp.coords_int, newintstep, self.optstep_hist[-2].grad_int, curr_sp.grad_int):
                dbgfile.write('{:12.8f} {:12.8f} {:12.8f} {:12.8f} {:12.8f}\n'.format(cold, cnew, cstep, gold, gnew))
            #self.opthist.pushNew(self.coords.xyzlist[-1], self.currgrad_cart, self.hessian.CurrHess, self.curren)
        logging.info('*** Optimization done. ***')
        if isconverged:
            logging.info('Optimization converged in {0:5d} steps.'.format(nsteps))
        else:
            logging.info('Optimization did not converge!')
        logging.info('*** Final XYZ in Angstrom\n{0}'.format(curr_sp.struct.get_String(units = 'ANG')))
        if self.options['write_trj']:
            trjfile.close()
        dbgfile.close()
        return (nsteps, isconverged, curr_sp.struct)

    def checkConvergence(self, de, dx, curr_grad, curr_cart, dethresh, gthresh, dxthresh):
        """
        Function to check convergence as proposed by Eckert and Werner, or very similarly by Baker, et al.
        """
        converged = False
        #if isinstance(self.Coords, Cartesians):
        gmax = np.absolute(curr_grad).max()
        if gmax <= gthresh:
            if np.absolute(de) < dethresh:
                converged = True
                logging.debug('Signaling convergence because of dE and max of gradient.')
            elif np.absolute(dx).max() < dxthresh:
                converged = True
                logging.debug('Signaling convergence because of grad and small displacement.')
        if not converged:
            logging.debug('No convergence yet. dE={0:18g}, Gmax={1:18g}, dXmax={2:18g}'.format(de, float(gmax), float(np.absolute(dx).max())))
        # else:
        #     logging.debug('Checking convergence in internal coordinates!')
        #     # convert cartesian gradient thresholds to internal gradient thresholds
        #     threshs     = { 'stretch': gthresh, 'bend': np.arctan(gthresh/3.0), 'LinBend': gthresh, 'LinBendCart': gthresh, 'twist': np.arctan(gthresh/3.0) }
        #     coord_list  = self.Coords.coords.get_Coords()
        #     int_thresh  = np.array([ threshs[c.type] for c in coord_list ])
        #     if np.all(np.absolute(curr_grad)-np.absolute(int_thresh) < 0.0) and np.absolute(de) < dethresh:
        #         converged = True
        #     else:
        #         logging.debug('   Not converged yet.')
        #     logging.debug('   Difference to convergence: '+' '.join('{:12.8f}'.format(v) for v in np.absolute(curr_grad)-np.absolute(int_thresh)))
        #     logging.debug('   Internal thresholds: '+' '.join('{:12.8f}'.format(v) for v in int_thresh))
        # elif np.absolute(de) < dethresh and dx < 6.0e-4 and gmax < 1.0e-3: # dx threshold taken from ORCA (TightOpt, RMS value).
        #     converged = True
        #     logging.debug('Signaling convergence because of dE and small displacement.')
        return converged

    def get_NumGrad(self, coords_cart = None, diff = 1.0e-2):
        """ Tries to calculate the numerical (central differences) gradient from delocalized coordinates.
        """
        assert coords_cart != None

        tmpcoords   = DelocalizedInternals(optref = self, startcart = coords_cart, atomlabels = self.start_mol.get_Elem_List(), getextrared = False)
        tmpdx       = np.zeros(tmpcoords.current.shape)
        tmpgrad_int = np.zeros(tmpcoords.current.shape)
        tmpgrad_cart= None
        tmpmol      = Molecule(copyfrom = self.start_mol)

        for i in range(len(tmpgrad_int)):
            tmpdx[i-1]  = 0.0
            # positive
            tmpdx[i]    = diff
            converged, rms, ccart, cint = tmpcoords.transform_back(tmpcoords.current_int, tmpcoords.current_cart, tmpdx, tmpcoords.Bmat)
            tmpmol.set_coords(ccart.reshape(tmpcoords.atomcount, 3), units = 'BOHR')
            en_plus     = self.getEnergy(tmpmol)
            # negative
            tmpdx[i]        = -diff
            converged, rms, ccart, cint = tmpcoords.transform_back(tmpcoords.current_int, tmpcoords.current_cart, tmpdx, tmpcoords.Bmat)
            tmpmol.set_coords(ccart.reshape(tmpcoords.atomcount, 3), units = 'BOHR')
            en_minus        = self.getEnergy(tmpmol)
            tmpgrad_int[i]  = (en_plus - en_minus)/(2.0 * diff)
        tmpgrad_cart    = np.dot(tmpcoords.Bmat.transpose(), tmpgrad_int)
        return (tmpgrad_cart, tmpgrad_int)

