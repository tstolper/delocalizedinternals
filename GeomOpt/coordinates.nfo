########## Coordinate Module Information #############

In general, coordinate classes need at least three methods:

- '__init__(self, optref, startcart, atomlabels, dotest, **kwargs)'
    Actually only the **kwargs need to be caught, because all
    coordinate classes are supposed to inherit from the main 'Coordinates'
    class, which handles the other options.

    'optref' is a back-reference to the optimization class for
    interoperability with other classes like the Hessian and method.

    'startcart' is a (flat) list of cartesian coordinates of the
    starting structure.

    'atomlabels' is a list of the corresponding labels of the
    elements in startcart.

    'dotest' is just a convenience option to circumvent errors
    while testing just the coordinate classes.

- 'update_coord(self, dx)'
    Method to update the coordinates after an optimization step.
    'dx' is the step in INTERNAL (coordinate frame of the class)
    coordinates!

- 'doSync(self, hessref, gradvec, **kwargs)'
    Should do everything that's necessary to keep coordinates, hessian
    and gradient in sync after an optimization step. The Hessian update
    should be called here (so that it can be transformed accordingly).
    Have a look at the 'NormalCoordinates' class if in doubt.