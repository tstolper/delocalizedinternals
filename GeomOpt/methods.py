# -*- coding: utf-8 -*-


#############################################
#                                           #
# Method library. Provides optimization     #
# methods suitable for geometry             #
# optimization.                             #
#                                           #
# Currently offers:                         #
#                                           #
# Rational Function                         #
#                                           #
#############################################

import logging
import scipy as np
import scipy.linalg as linalg
import scipy.optimize as spopt
import utility_funcs as utils

def do_1D_quartic_Search(opt_steps = None, laststep_idx = None):
    """ Tries do to a one dimensional quartic fit to two points (x,g,E) and (hopefully)
    adhering to the constraint the there is only one point with d^2E/dx^2 = 0 (Schlegel 1982).
    This means the following equations are thrown in:
    a*x^4 + b*x^3 + c*x^2 + d*x + e = E for every coordinate point
    4ax^3 + 3bx^2 + 2cx + d = g        same here
    12ax^2 + 6bx + 2c >= 0              should be zero only at one point. 
    """
    assert opt_steps is not None
    assert laststep_idx is not None

    if laststep_idx > 0:
        logging.debug('----Quartic 1D fit using steps {:5d} and {:5d}.'.format(laststep_idx, laststep_idx-1))
        cen, ccoord, cgrad = opt_steps[laststep_idx].energy, opt_steps[laststep_idx].coords_int, opt_steps[laststep_idx].grad_int
        oen, ocoord, ograd = opt_steps[laststep_idx-1].energy, opt_steps[laststep_idx-1].coords_int, opt_steps[laststep_idx-1].grad_int
        dx = ccoord-ocoord
        dxlen = linalg.norm(dx)
        clen = linalg.norm(ccoord)
        olen = linalg.norm(ocoord)
        # use the norm of the gradient as the value for fitting
        # check the sign by the angle between the gradients (first one (ocoord) should be negative)
        ga0 = np.dot(ograd, dx)
        ga1 = np.dot(cgrad, dx)
        utils.log_Vector('dX:', dx, logging.debug)
        # hello
        ncoords = len(ccoord)
        amat = np.ones((4,4))
        bvec = np.zeros((4,))
        amat[0, 0] = 0.0
        amat[0, 1] = 0.0
        amat[0, 2] = 0.0
        amat[0, 3] = 1.0
        bvec[0] = oen
        amat[1, 0] = 1.0
        amat[1, 1] = 1.0
        amat[1, 2] = 1.0
        amat[1, 3] = 1.0
        bvec[1] = cen
        amat[2, 0] = 0.0
        amat[2, 1] = 0.0
        amat[2, 2] = 1.0
        amat[2, 3] = 0.0
        bvec[2] = ga0
        amat[3, 0] = 3.0
        amat[3, 1] = 2.0
        amat[3, 2] = 1.0
        amat[3, 3] = 0
        bvec[3] = ga1
        # for i in range(4):
        #     for j in range(ncoords):
        #         if i%2 == 0:
        #             cx = ccoord[j]
        #             en = cen
        #             grad = cgrad
        #         else:
        #             cx = ocoord[j]
        #             en = oen
        #             grad = ograd
        #         if i < 2:
        #             fact1, fact2, fact3, fact4, fact5 = 1.0, 1.0, 1.0, 1.0, 1.0
        #             pow1, pow2, pow3, pow4, pow5 = 4, 3, 2, 1, 0
        #             bv = en
        #         else:
        #             fact1, fact2, fact3, fact4, fact5 = 4.0, 3.0, 2.0, 1.0, 0.0
        #             pow1, pow2, pow3, pow4, pow5 = 3, 2, 1, 0, 0
        #             bv = grad[j]
        #         amat[ncoords*i+j,0] = fact1*pow(cx,pow1)
        #         amat[ncoords*i+j,1] = fact2*pow(cx,pow2)
        #         amat[ncoords*i+j,2] = fact3*pow(cx,pow3)
        #         amat[ncoords*i+j,3] = fact4*pow(cx,pow4)
        #         amat[ncoords*i+j,4] = fact5
        #         bvec[ncoords*i+j] = bv
        print 'HELLO!'
        avals, res, rank, sing = linalg.lstsq(amat, bvec)
        logging.debug('    ga0 = {:12.5f}, ga1 = {:12.5f}'.format(ga0,ga1))
        logging.debug('    Rank of amat: {:5d}'.format(rank))
        logging.debug('    Residual: '+repr(res))
        alpha1 = -2.0*avals[1]/(6.0*avals[0]) + np.sqrt(np.power(2.0*avals[1]/(6.0*avals[0]), 2)-avals[2]/(3.0*avals[0]))
        alpha2 = -2.0*avals[1]/(6.0*avals[0]) - np.sqrt(np.power(2.0*avals[1]/(6.0*avals[0]), 2)-avals[2]/(3.0*avals[0]))
        logging.debug('    x1, x2: {:12.5f}, {:12.5f}'.format(alpha1, alpha2))
        df2alpha1 = 6.0*avals[0]*alpha1+2.0*avals[1]
        df2alpha2 = 6.0*avals[0]*alpha2+2.0*avals[1]
        logging.debug('    2nd deriv at x1, x2: {:12.5f}, {:12.5f}'.format(df2alpha1, df2alpha2))
        if df2alpha1 > 0.0 and df2alpha2 <= 0.0:
            xnum = 1
            epredict = avals[0]*pow(alpha1, 3) + avals[1]*pow(alpha1, 2) + avals[2]*alpha1 + avals[3]
            retstep = (alpha1-1.0)*dx
            steplen = linalg.norm((alpha1-1.0)*dx)
        elif df2alpha2 > 0.0 and df2alpha1 <= 0.0:
            xnum = 2
            epredict = avals[0]*pow(alpha2, 3) + avals[1]*pow(alpha2, 2) + avals[2]*alpha2 + avals[3]
            retstep = (alpha2-1.0)*dx
            steplen = linalg.norm((alpha2-1.0)*dx)
        else:
            raise ValueError('Something wrong here.')
        logging.debug('    Predicted energy and step length at x{:1d}: {:12.6f} {:12.5f}'.format(xnum, epredict, steplen))
        utils.log_Vector('Koefficient vector:', avals, logging.debug)
        utils.log_Vector('Singular values:', sing, logging.debug)
        return steplen, epredict, retstep

def update_TrustRadius(curr_t = None, dE = None, curr_step = None, curr_grad = None, curr_hess = None):
    assert curr_t is not None
    assert dE is not None
    assert curr_step is not None
    assert curr_grad is not None
    assert curr_hess is not None
    logging.debug('----Updating Trust radius')
    new_t = curr_t
    rho = dE/(np.dot(curr_grad, curr_step) + np.dot(curr_step, np.dot(curr_hess, curr_step))/2.0)
    step_len = linalg.norm(curr_step)
    sqrtn = np.sqrt(curr_step.shape[0]/3.0)
    if rho > 0.75 and 5.0*step_len/4.0 > curr_t:
        new_t = 2.0 * curr_t
    elif rho < 0.25:
        new_t = 0.25 * curr_t
    if new_t < sqrtn/10.0:
        new_t = sqrtn/10.0
        logging.debug('Limiting trust radius to tmin.')
    elif new_t > sqrtn:
        new_t = sqrtn
        logging.debug('Limiting trust radius to tmax.')
    logging.debug('    New trust radius: {:12.5f}'.format(new_t))
    return new_t


class RationalFunction(object):
    """
    The Rational Function algorithm uses a function to restrict the step length.
    Thus it's one of the many (quasi) Newton methods. By default it should be built
    to resemble Molpro's RF optimizer.

    Values:

    hessref              Reference to a Hessian class.
    startcoords          Coordinates in whatever system everything (gradient, Hessian) is provided.
                         Assumes reference to Coordinates class.
    smax                 Float. Maximum value the step may have.
    usenorm              Apply smax to the norm of the step instead of the individual values.
    """
    def __init__(self, optref = None, smax = 0.3, doscale = True, usenorm = True, **kwargs):
        if optref == None:
            raise ValueError('At least one Hessian or Coordinates equals None. Please supply.')
        else:
            self.OptParent     = optref
            self.OptHist     = optref.OptHist
            self.Coords     = optref.Coords
            self.Hessian    = optref.Hessian
            self.smax         = smax
            self.doscale    = doscale
            self.usenorm     = usenorm


    def doStep(self, curr_hess = None, curr_grad = None, curr_vars = None):
        """
        Function doing the actual step of the optimization. Just one though, because convergence
        is not part of the method.
        Options:

        Returns:

        coords         Coordinates in default system and the actual step (∆x).
        """
        assert curr_hess is not None
        assert curr_grad is not None

        #First build augmented Hessian matrix
        lastrow = np.append(curr_grad, 0.0) #returns a copy of the gradient extended with a zero
        aughess = np.vstack((np.column_stack((curr_hess, curr_grad)), lastrow)) #hstack/vstack take tuples as input.
        #assert augHess.__array_interface__['data'][0] % 16 == 0 #check if the array is in 16 byte long blocks
        eigenvals, eigenvecs = linalg.eigh(aughess)
        utils.log_Matrix('RFO Matrix:', aughess)
        utils.log_Vector('Eigenvalues:', eigenvals, logging.debug)
        #The step is now the eigenvector (last element scaled to 1) of the lowest eigenvalue
        lowestev = np.argmin(eigenvals)
        if eigenvals[lowestev] > 1.0E-4:
            raise ValueError('ERROR: I don\'t want to go up the PES!')
        stepvec = eigenvecs[:, lowestev]
        if stepvec[-1] == 0.0:
            scaledstepvec = stepvec[:-1]
            logging.debug('    WARNING: Step should be devided by 0! No guarantee for the step size!')
        else:
            scaledstepvec = stepvec[:-1]/stepvec[-1]
            logging.debug('    Scaling eigenvector of RFO matrix by last element: {:8.5e}'.format(stepvec[-1]))
        logging.debug('    Length of RFO step vector: {:12.8f}'.format(linalg.norm(stepvec[:-1])))
        #Time to check and scale the chosen step
        if self.doscale:
            realstep_int = self.scaleStep(scaledstepvec)
        else:
            realstep_int = scaledstepvec
        logging.debug('    Length of scaled step vector: {:12.8f}'.format(linalg.norm(realstep_int)))
        logging.debug('    Predicted energy change: {:12.8f}'.format(0.5*eigenvals[lowestev]/(stepvec[-1])**2))
        # compare with the quartic fit
        # if len(self.OptParent.optstep_hist) > 1:
        #     slen, sen, svec = do_1D_quartic_Search(self.OptParent.optstep_hist, self.OptParent.optstep_hist.index(curr_step))
        #     #Just do the step. No need to update the internals, since that should be done in optimize loop.
        #     self.Coords.update_coord(svec)
        # else:
        #     self.Coords.update_coord(realstep_int)
        # newXYZ = self.Coords.current_cart #should always contain newest coordinates in XYZ, regardless of coordinate system used.
        #Aaaand debugging!
        utils.log_Vector('Step vector:', scaledstepvec, logging.debug)
        utils.log_Vector('Real step (restricted):', realstep_int, logging.debug)
        return realstep_int

    def scaleStep(self, step):
        """
        Function to ensure that the step doesn't get too large. This only got its own method so that the
        AH class can later easily replace this. Scaling in delocalized internals doesn't really work (and
        also doesn't make too much sense), so we're now trying this on primitive internals.
        Returns the step.
        """
        newstep = None
        if self.usenorm:
            stepnorm = linalg.norm(step)
            logging.debug('Step norm: {0:12.8g}'.format(stepnorm))
                #if stepnorm > 10.0:
                #    print 'Very large step. actual step-array:', repr(step)
            if stepnorm > self.smax:
                newstep = step*self.smax/stepnorm
                logging.debug('Scaling step by {0:12.4g}'.format(self.smax/stepnorm))
            else:
                newstep = step
                logging.debug('Not scaling RFO step.')
        else:
            # Scaling like in PSI4
            # first determine largest scaling
            scale = 1.0
            for c in step:
                if scale * np.absolute(c) > self.smax:
                    scale = self.smax / np.absolute(c)
            newstep = step * scale
            logging.debug('Scaled steb by: {0:12.4g}'.format(scale))
        #last but not least, Molpro checks the current step against the last step
        #supposedly, reducing the coordinates that don't change much between two steps
        #helps avoiding oscillation
        # if len(self.OptHist.xyzstructs) > 1:
        #     dXold = self.OptHist.xyzstructs[-1]-self.OptHist.xyzstructs[-2]
        #     for idx, val in np.ndenumerate(newstep):
        #         if np.absolute(dXold[idx] + val) < 1.0e-8:
        #             newstep[idx] *= 0.7
        #             if self.OptParent.dodebug: print 'Modifying coordinate because of oscillation. Sum: '+str(dXold[idx] + val)
        return newstep

class AHRS(RationalFunction):

    def Hebden_procedure(self, Rk=None, Nuk0=None, EVals=None, EVecs=None, realgrad=None, maxiter=10):
        logging.debug('--- Hebden procedure')
        logging.debug('    Target R={:12.6f}, µ0={:12.6e}'.format(Rk, Nuk0))
        convergence = 100.0
        step_len    = 100.0
        alphas      = np.dot(EVecs.T, realgrad)
        Nuki        = Nuk0
        niter       = 0
        while step_len > Rk+1.0e-5 and niter < maxiter:
            logging.debug('  - Iteration {:5d}'.format(niter+1))
            step        = -np.sum(alphas[i]*EVecs[:, i]/(EVals[i] + Nuki) for i in range(len(EVals)))
            step_len    = linalg.norm(step)
            len_deriv   = (-np.sum(alphas[i]**2/(EVals[i] + Nuki)**3 for i in range(1,len(EVals))))/step_len
            convergence = (1.0 - step_len/Rk)*step_len/len_deriv
            Nuki       += convergence
            logging.debug('    Current values: µi={:12e}, dµ={:12e}, ||step||={:12e}, d||step||={:12e}'.format(Nuki, convergence, step_len, len_deriv))
            niter      += 1
        logging.debug('--- Hebden procedure finished')
        return Nuki

    def doStep(self, curr_hess = None, curr_grad = None, curr_vars = None):
        logging.debug('--- AHRS step')
        max_step= self.smax
        h0, s0 = linalg.eigh(curr_hess)
        h0_min = np.argmin(h0)
        rotgrad = np.dot(s0.T, curr_grad)
        aughess = np.vstack((np.column_stack((np.diag(h0), rotgrad)), np.append(rotgrad, 0.0)))
        lmb, vvecs = linalg.eigh(aughess)
        lmb_min    = np.argmin(lmb)
        step    = vvecs[:, lmb_min]
        step    = step[:-1]/step[-1]
        len2    = np.dot(step, step)
        logging.debug('len2: '+repr(len2)+' lmin: '+repr(lmb[lmb_min]))
        if np.absolute(len2) > max_step**2 or h0[h0_min]-lmb[lmb_min] < 0.0:
            good_step = False
        else:
            good_step = True
        while not good_step: # do microiterations
            # do Hebden procedure
            guess_mu= linalg.norm(curr_grad)/max_step + np.absolute(h0[h0_min])
            mu      = self.Hebden_procedure(Rk=max_step, Nuk0=guess_mu, EVals=h0, EVecs=s0, realgrad=curr_grad)
            aughess = np.vstack((np.column_stack((np.diag(h0) + (-mu - lmb[lmb_min])*np.identity(len(h0)), rotgrad)), np.append(rotgrad, mu*max_step**2)))
            __l, __v= linalg.eigh(aughess)
            lmin    = np.argmin(__l)
            step    = __v[:, lmin]
            step    = step[:-1]/step[-1]
            len2    = np.dot(step, step)
            logging.debug('    Current lowest eigenvalue={:8e}, step length={:8e}, direction={:5.2f}°.'.format(
                            __l[lmin], np.sqrt(len2), utils._calc_Angle_of_Vects(step, curr_grad)*180.0/np.pi))
            rotstep = np.dot(s0.T, step)
            logging.debug('    rotstep len={:8.4e}, direction={:5.2f}°'.format(linalg.norm(rotstep), utils._calc_Angle_of_Vects(rotstep, curr_grad)*180.0/np.pi))
            if np.absolute(len2) > max_step**2 or h0[h0_min] - __l[lmin] + mu < 0.0:
                max_step /= 4.0
                logging.debug('    Step does not show the desired length ({:8.4e} <= {:8.4e}) or inertia ({:8.4e} > 0), lowering it.'.format(
                                    np.sqrt(len2), max_step, h0[h0_min] - __l[lmin] + mu))
            else:
                good_step = True
        logging.debug('--- AHRS done.')
        return step

class RSRFO(RationalFunction):

    def get_new_Alpha(self, hi=None, wi=None, grad=None, step=None, lamb=None, alph=None, Rk=None): # could pass just nu=lamb*alph
        dq2da = 2.0*lamb*np.sum(np.power(np.dot(wi[:, i], grad), 2)/np.power(hi[i] - lamb*alph, 3) for i in range(len(hi)))/(1.0 + np.dot(step, step)*alph)
        correction = 2.0*(Rk*linalg.norm(step) - np.dot(step, step))/dq2da.real
        logging.debug('    Determined new alpha. alpha={:12.8f}, Rk={:12.6f}, dq2da={:8.4e}, correction={:8.4e}'.format(alph + correction, Rk, dq2da, correction))
        logging.debug('    lamb: {:12.8f}, alph: {:12.8f}, Rk: {:12.8f}, step*step: {:12.8f}'.format(lamb, alph, Rk, np.dot(step, step)))
        return alph + correction

    def doStep(self, curr_hess = None, curr_grad = None, curr_vars = None):
        logging.debug('--- RS-RFO step')
        max_step= self.smax
        alpha   = 1.0
        mu      = 0
        good_step   = True
        h0, s0 = linalg.eigh(curr_hess)
        h0_min = np.argmin(h0)
        rotgrad = np.dot(s0.T, curr_grad)
        aughess = np.vstack((np.column_stack((np.diag(h0), rotgrad)), np.append(rotgrad, 0.0)))
        lmb, vvecs = linalg.eigh(aughess)
        lmb_min    = np.argmin(lmb)
        step    = vvecs[:, lmb_min]
        if np.absolute(step[-1]) < 1.0e-8: # I'm such a dirty cheater!
            logging.debug('    Step too large! Going with gradient descent!')
            step = -curr_grad
            slen = linalg.norm(step)
            if slen > max_step:
                step = step / slen
            return step
        else:
            step    = step[:-1]/step[-1]
        step_len= linalg.norm(step)
        logging.debug('    step length={:12.2f}, lowest eigenvalue={:12.8f}'.format(step_len, lmb[lmb_min]))
        if step_len > max_step:
            good_step = False
        while not good_step:
            logging.debug('    Step not accepted, recalculating step. Iteration {:5d}'.format(mu+1))
            alpha_old   = alpha
            alpha       = self.get_new_Alpha(hi=h0, wi=s0, grad=curr_grad, step=step, lamb=lmb[lmb_min], alph=alpha, Rk=max_step)
            # get new step
            aughess = np.vstack((np.column_stack((np.diag(h0/alpha), rotgrad/np.sqrt(alpha))), np.append(rotgrad/np.sqrt(alpha), 0.0)))
            lmb, vvecs  = linalg.eigh(aughess)
            lmb_min     = np.argmin(lmb)
            step        = vvecs[:, lmb_min]
            step        = step[:-1]/(step[-1]*np.sqrt(alpha))
            step_len    = linalg.norm(step)
            if np.absolute(step_len-max_step) > 1.0E-4 or np.absolute(alpha_old-alpha) > 1.0e-5:
                good_step = False
            else:
                good_step = True
            mu += 1
        logging.debug('    Step length: {:12.6f}'.format(step_len))
        logging.debug('    Predicting energy change: {:12.8f} Eh'.format(0.5*lmb[lmb_min]/vvecs[-1, lmb_min]**2))
        logging.debug('    Projecting step back.')
        step = np.dot(s0, step)
        logging.debug('--- RS-RFO step done.')
        return step

class FarkasRFO(object):
    """ Another flavour of rational function optimization with improvements from Ö. Farkas, H. B. Schlegel, JCP 1999, 111, 10806.
    """
    def __init__(self, optref = None, **kwargs):
        if optref is None:
            raise ValueError('Need the reference of the parent optimization object.')
        else:
            self.OptParent  = optref
            self.OptHist    = optref.OptHist
            self.Coords     = optref.Coords
            self.Hessian    = optref.Hessian
            #self.mbar_hist  = []
            self.lmbd_hist  = []
            self.alpha_hist = []
            self.bet      = None

    def update_Mbar(self, xkp1 = None, y = None, M = None, chess = None, Smat = None, elow = None):
        """
        """
        assert xkp1 is not None
        assert y is not None
        assert M is not None
        assert chess is not None
        assert Smat is not None
        assert elow is not None
        scaler = np.sqrt(M.shape[0])
        lmbmin = -elow + min(-elow/2.0, 1.0E-4)
        if self.bet is None:
            self.bet = lmbmin
        # Calculate lambda_RFO
        yx = np.dot(y, xkp1)
        x2 = np.dot(xkp1, xkp1)
        Mx = np.dot(M, xkp1)
        P = np.dot(xkp1, Mx) * scaler / (x2*yx)
        Q = -yx * scaler / (x2*yx)
        alpha = -P/2.0 + np.sqrt(np.power(P, 2)/4.0 - Q).real # this is alpha_k ?!
        lrfo = alpha.real * np.dot(xkp1, y)
        # do some things that could be ltrm
        muk = self.lmbd_hist[-1]/scaler
        ltrm = ((1.0-alpha)*np.dot(xkp1, np.dot(chess, xkp1)) + muk*x2)/(alpha*x2)
        ltrm2 = 0.0
        ltrm3 = 0.0
        ltrm4 = 0.0
        if len(self.alpha_hist) > 0 and alpha > 1.0 and self.alpha_hist[-1] < 1.0:
            mukm1 = self.lmbd_hist[-2]/scaler
            ltrm2 = ((1.0-alpha)*mukm1 + (self.alpha_hist[-1]-1.0)*muk)/(self.alpha_hist[-1]-alpha)
            utils.logverbose('    Doing a linear interpolation.')
        if elow < 0.0:
            ltrm3 = (muk + elow)/alpha + elow
        if yx < 0.0:
            self.bet += yx/scaler
            ltrm4 = self.bet * scaler
        if self.bet*scaler > lrfo:
            utils.logverbose('    Bet*S WOULD BE LARGER!')
        lkp1 = max(lrfo, ltrm, ltrm2, ltrm3)
        utils.logverbose('    lkp1: {:12.5e}'.format(lkp1))
        self.alpha_hist.append(alpha)
        self.lmbd_hist.append(lkp1)
        # get the new mbar matrix
        mbarkp1 = chess - lkp1*Smat/scaler
        utils.logverbose('    Alpha: {:12.5f}, LambdaRFO={:12.5f}'.format(alpha, lrfo))
        utils.logverbose('    ltrm: {:12.5f}, ltrm2: {:12.5f}, ltrm3: {:12.5f}, ltrm4: {:12.5f}'.format(ltrm, ltrm2, ltrm3, ltrm4))
        return mbarkp1


    def doStep(self, curr_step = None):
        assert curr_step is not None
        # use cartesians for the step to avoid redundancy problems
        intgrad = curr_step.grad_int
        if curr_step.hess_int is None:
            inthess = self.Coords.conv_Hess_to_int(curr_step.hess_cart)
        else:
            inthess = curr_step.hess_int
        # if curr_step.hess_cart is None:
        #     print 'Converting internal Hessian to cartesians'
        #     chess = self.Coords.conv_Hess_to_cart(curr_step.hess_int, curr_step.struct.get_flat_Array(units = 'BOHR'), curr_step.grad_int)
        # else:
        #     chess = curr_step.hess_cart
        # use the iterative method to solve the linear equations f = (H + lambda*S)s
        lmb_guess = np.dot(intgrad, intgrad)/np.sqrt(inthess.shape[0])
        mbar_guess = inthess - lmb_guess*self.Coords.Gmat_pinv
        lowest_eig = np.amin(linalg.eigvalsh(inthess))
        self.lmbd_hist.append(lmb_guess)
        logging.debug('    Guess Lambda: {:12.5f}'.format(lmb_guess))
        #self.lmbd_hist.append(lmb_guess)
        #self.mbar_hist.append(mbar_guess)
        step = utils.solve_linEq_ON2(y=-intgrad, M=mbar_guess, mbarhook=self.update_Mbar, chess=inthess, Smat = self.Coords.Gmat_pinv, elow = lowest_eig)
        self.Coords.update_coord(step)
        return self.Coords.current_cart

###############       New type of optimizers which are hopefully cleaner, simpler and at least as functional.         #################

def get_RFOStep(curr_hess = None, curr_grad = None, curr_vars = None):
    assert curr_hess is not None
    assert curr_grad is not None
    assert curr_vars is not None

    utils.logverbose('################# RFO step calculation ###################')
    #First build augmented Hessian matrix
    lastrow = np.append(curr_grad, 0.0)
    aughess = np.vstack((np.column_stack((curr_hess, curr_grad)), lastrow))
    eigenvals, eigenvecs = linalg.eigh(aughess) # <---- Could use a Davidson routine maybe.
    #utils.log_Matrix('RFO Matrix:', aughess, logging.debug)
    utils.log_Vector('Eigenvalues:', eigenvals, utils.logverbose)
    # scale eigenvector
    lowestev = np.argmin(eigenvals)
    if eigenvals[lowestev] > 1.0E-4:
        raise ValueError('ERROR: I don\'t want to go up the PES!')
    else:
        stepvec = eigenvecs[:, lowestev]
        utils.log_Vector('    RFO step vector before scaling:', stepvec, utils.logverbose)
        if stepvec[-1] == 0.0:
            scaledstepvec = stepvec[:-1]
            logging.debug('    WARNING: Step should be devided by 0! No guarantee for the step size!')
        else:
            scaledstepvec = stepvec[:-1]/stepvec[-1]
    #utils.log_Vector('SimpleRFO Step vector:', scaledstepvec, logging.debug)
    logging.debug('    Length of the RFO step: {:15.8f}'.format(linalg.norm(scaledstepvec)))
    logging.debug('    Maximum component of the step: {:15.8f}'.format(np.amax(np.absolute(scaledstepvec))))
    if linalg.norm(scaledstepvec) > 10:
        logging.debug('    WARNING: Step is weirdly large!')
    # slen, smax = linalg.norm(scaledstepvec), np.amax(np.absolute(scaledstepvec))
    # if slen > 0.3 or smax > 0.3:
    #         logging.debug('    RFO Step too large, scaling it.')
    #         if slen > smax:
    #             logging.debug('    Scaling by length. Factor: {:12.6f}'.format(0.3/slen))
    #             scaledstepvec *= 0.3/slen
    #         else:
    #             logging.debug('    Scaling by maximum component. Factor: {:12.6f}'.format(0.3/smax))
    #             scaledstepvec *= 0.3/smax
    return scaledstepvec

class SimpleDIIS(object):
    """ KISS style DIIS
    """

    def __init__(self, optref = None, **kwargs):
        if optref is None:
            raise ValueError('Need the reference of the parent optimization object.')
        else:
            self.OptParent  = optref
            self.old_points = [] # Might give problems with coordinate resets!
            self.max_points = kwargs.pop('max_steps', 10)
            self.smax       = 0.3
            self.space = kwargs.pop('space', 'grad').upper()

    def _get_gigj(self, i = None, j = None, points = None, *args):
        return np.dot(points[i][1], points[j][1])

    def _get_amat(self, curr_hess = None, start = 0):
        if self.space == 'GRAD':
            calc_Aij = self._get_gigj
        else:
            raise ValueError('Cannot handle space {}'.format(self.space))
        points = self.old_points[start:]
        num_points = len(points)

        logging.debug('  Building Amat. Using {:d} steps.'.format(num_points))

        Amat = np.zeros((num_points+1, num_points+1))
        for i in range(num_points+1):
            for j in range(i, num_points+1):
                if j == num_points or i == num_points:
                    Amat[i, j] = 1.0
                else:
                    Amat[i, j] = calc_Aij(i, j, points, curr_hess)
                Amat[j, i] = Amat[i, j]
        Amat[:-1, :-1] /= Amat[-2, -2]
        Amat[num_points, num_points] = 0.0

        utils.log_Matrix('  Complete DIIS matrix:', Amat)
        return Amat

    def get_step(self, curr_hess = None):
        x_old, g_old = self.old_points[-1]
        if len(self.old_points) < 2:
            new_step = get_RFOStep(curr_hess, g_old, x_old)
        else:
            # 1. Determine properly conditioned matrix
            start = 0
            is_good = False
            while not is_good:
                amat = self._get_amat(curr_hess, start)
                vals, vecs = np.linalg.eigh(amat)
                amin, amax = np.absolute(vals).min(), np.absolute(vals).max()
                if amin < 1.0e-5 or amax/amin > 100.0:
                    start += 1
                    logging.debug('  Badly conditioned A matrix! amin = {:12.8f}, amax = {:12.8f}'.format(amin, amax))
                else:
                    is_good = True
            # 2. Renormalize coefficient vector
            cvec = vecs[np.argmin(vals)][:-1]
            csum = np.sum(cvec)
            ncs = len(cvec)
            #cvec += (1.0 - csum)*np.ones((ncs,))/ncs
            cvec /= csum
            utils.log_Vector('  Coefficient vector after renormalization:', cvec)
            # 3. Interpolate
            x_intp = np.sum(c*x[0] for c, x in zip(cvec, self.old_points[start:]))
            g_intp = np.sum(c*x[1] for c, x in zip(cvec, self.old_points[start:]))
            # 4. Take (Quasi-)Newton step (or RF)
            relax_step = get_RFOStep(curr_hess, g_intp, x_intp)
            new_step = x_intp + relax_step - x_old
        logging.debug('  Length of relaxed step: {:12.8f}'.format(np.linalg.norm(new_step)))
        return new_step

    def doStep(self, curr_hess = None, curr_grad = None, curr_vars = None):
        logging.debug('##### DIIS #####')
        if len(self.old_points) >= self.max_points:
            self.old_points.pop(0)
        self.old_points.append((curr_vars, curr_grad))
        step = self.get_step(curr_hess)
        if np.linalg.norm(step) > self.smax:
            step *= self.smax/np.linalg.norm(step)
        logging.debug('  DIIS step length: {:12.8f}'.format(np.linalg.norm(step)))
        return step


class DIIS(object):
    """ Standard DIIS as described by Pulay for example.
        References:
        - http://vergil.chemistry.gatech.edu/notes/diis/diis.html
        - P. Császár, P. Pulay, J. Mol. Struct., 114, 1984, 31--34.
        - Ö. Farkas, H. B. Schlegel, PCCP, 4, 2002, 11--15.

        Result: Ok, this became more of a geometry optimization procedure ... but it should still be easily
                appliable to electronic structure methods.
    """

    def __init__(self, optref = None, **kwargs):
        if optref is None:
            raise ValueError('Need the reference of the parent optimization object.')
        else:
            self.OptParent  = optref
            self.old_points = []
            self.max_points = 10
            self.curr_iter  = 0
            self.smax       = 0.3

    def get_Aij(self, i = None, j = None, space = None):
        assert i is not None
        assert j is not None
        assert space is not None
        return np.dot(space[i].T, space[j])

    def solve_SEQ(self, error_vecs = None):
        assert error_vecs is not None

        numsteps = len(error_vecs)
        Amat = np.zeros((numsteps+1, numsteps+1))
        for i in xrange(Amat.shape[0]):
            for j in xrange(i, Amat.shape[1]):
                if j == numsteps:
                    if i != numsteps:
                        Amat[i,j] = 1.0
                else:
                    Amat[i,j] = self.get_Aij(i, j, error_vecs)
                Amat[j,i] = Amat[i,j]
        res  = np.zeros((Amat.shape[0],))
        res[-1] = 1.0
        cvec, residuals, rank, svals = linalg.lstsq(Amat, res)
        #utils.log_Matrix('    A matrix:', Amat, logging.debug)
        logging.debug('    DIIS coefficients: '+' '.join(['{:12.5f}'.format(c) for c in cvec]))
        return cvec[:-1]

    def get_new_Vars(self, curr_hess = None, curr_grad = None, curr_vars = None):
        # just do an RFO step at the beginning.
        curr_hinv = utils.symm_mat_inv(curr_hess, redundant = True)
        ref_step = np.dot(curr_hinv, -curr_grad)
        new_vars  = curr_vars + ref_step
        if self.curr_iter >= 2:
            if self.max_points is None or self.max_points > self.curr_iter:
                maxsteps = self.curr_iter
            else:
                maxsteps = self.max_points
            utils.log_Matrix('    Hinv:', curr_hinv, logging.debug)
            tmparr = [] #[ np.dot(curr_hinv, -self.old_points[-maxsteps+point][1]) for point in range(maxsteps) ]
            for point in xrange(maxsteps):
                utils.log_Vector('    gradient of step {:5d}:'.format(point+1), self.old_points[-maxsteps+point][1], logging.debug)
                # if point < maxsteps-1:
                #     dx = self.old_points[-maxsteps+point+1][0] - self.old_points[-maxsteps+point][0]
                # else:
                #     dx = np.dot(curr_hinv, -self.old_points[-maxsteps+point][1])
                dx = self.old_points[-maxsteps+point+1][1]
                utils.log_Vector('    dx:', dx, logging.debug)
                tmparr.append(dx)
            cvec = self.solve_SEQ(tmparr)
            # interpolate variables and gradient to get new set.
            curr_gstar = np.sum((cvec[i]*self.old_points[-maxsteps+i][1] for i in xrange(maxsteps)), axis = 0)
            curr_xstar = np.sum((cvec[i]*self.old_points[-maxsteps+i][0] for i in xrange(maxsteps)), axis = 0)
            diis_step  = curr_xstar - np.dot(curr_hinv, curr_gstar) - curr_vars
            cost = np.dot(diis_step, ref_step)/(linalg.norm(diis_step)*linalg.norm(ref_step))
            logging.debug('    cos(t): {:12.5f}'.format(cost))
            if cost > 0.4:
                new_vars = curr_vars + diis_step
            else:
                logging.debug('    GDIIS step not accepted. Taking RFO step.')
        return new_vars

    def doStep(self, curr_hess = None, curr_grad = None, curr_vars = None):
        assert curr_hess is not None
        assert curr_grad is not None
        assert curr_vars is not None

        utils.logverbose('################## DIIS optimization ################')
        # 1. Save error vector and points
        if self.max_points is not None and len(self.old_points) >= self.max_points:
            self.old_points.pop(0)
        self.old_points.append((curr_vars, curr_grad))
        self.curr_iter += 1
        # 2. Get the new step and scale if necessary
        newstep = self.get_new_Vars(curr_hess, curr_grad, curr_vars) - curr_vars
        slen, smax = linalg.norm(newstep), np.amax(np.absolute(newstep))
        logging.debug('    Step length: {:12.5f}'.format(slen))
        if slen > self.smax:
            logging.debug('    DIIS Step too large, scaling it.')
            newstep *= self.smax/slen
        return newstep

class GDIIS(DIIS):
    """ GDIIS optimization with some improvements of Farkas et al.
        References:
        - Ö. Farkas, H. B. Schlegel, PCCP, 4, 2002, 11--15.

        Changes they made to the "default" procedure:
        - Quotation: "We use this effective Hessian not only for the reference step, 
          but also to compute the error vectors." This means using an RFO step throughout.
        - If there are less than 3 points they use either the quartic linear search
          or jus the reference step.
        - They (supposedly) iteratively build up their interpolation space one at a time from the previous steps.
          I.e. they add steps to the list of points until the following tests say the resulting
          step would be unacceptable and the last acceptable step is then used.
          If the resulting step would have an angle greater than 90° to the reference step, however,
          the particular point and all earlier ones are discarded completely.
        - Test the GDIIS step:
            1. Check if the angle of the GDIIS step (real structures, not interpolated, i.e. x* + r)
               is within an acceptable angle to the reference step: cos(t) = dx_gdiis * dx_ref / (|dx_gdiis|*|dx_ref|)
               This depends on the accuracy of the Hessian and the number of steps. In their implementation
               cos(t) has to be larger than: 0.97, 0.84, 0.71, 0.67, 0.62, 0.56, 0.49, 0.41 for 2-9 points
               and 0.00 for more than 10.
            2. |dx_gdiis| <= 10*|dx_ref|
            3. The sum of all positive interpolation coefficients c_i should be smaller than 15.
            4. c/|r|^2 <= 10^8. The size of the error vectors has an influence and thus they scale them
               just for solving the DIIS equations so that the smallest has unit length.
          If any of these is not fulfilled the step is not acceptable.
        - The Hessian is updated using their BFGS+SR1 combination.
    """
    def solve_SEQ(self, error_vecs = None):
        assert error_vecs is not None

        isredundant = False
        nsteps = len(error_vecs)
        Amat = np.zeros((nsteps, nsteps))
        for i in xrange(Amat.shape[0]):
            for j in xrange(i, Amat.shape[1]):
                Amat[i,j] = self.get_Aij(i, j, error_vecs)
                Amat[j,i] = Amat[i,j]
        res = np.ones((nsteps,))
        cvec, residuals, rank, svals = linalg.lstsq(Amat, res)
        # check for redundancy as Farkas does
        csum = np.sum(cvec)
        if csum > 1.0e+8:
            isredundant = True
        else:
            cvec /= csum
        # do some coefficient checks
        maxcoeff = np.argmax(cvec)
        if np.absolute(cvec[-1]) < 1.0e-4 or maxcoeff not in (nsteps-1,nsteps-2): # small coefficient for the latest structure is bad
            isredundant = True
        elif nsteps == 2 and maxcoeff == 0: # force the steps to be interpolated
            if cvec[maxcoeff] > 1.0: # change the step direction
                cvec[-1], cvec[-2] = cvec[-2], cvec[-1]
                logging.debug('    Swapping coefficient direction!')
            else:
                isredundant = True
        utils.logverbose('    CSum: {:8.2e}, maxcoeff: {:5d}'.format(csum, maxcoeff))
        utils.logverbose(' cvec: {0}\nresiduals: {1}\nrank: {2}\n svals: {3}'.format(repr(cvec), repr(residuals), repr(rank), repr(svals)))
        return isredundant, cvec

    def check_Step(self, diis_step = None, ref_step = None, cvec = None):
        """ Returns True if the GDIIS step is ok to be followed.
        """
        utils.logverbose('--- Checking the GDIIS step')
        step_is_ok = True
        min_angle  = (0.97, 0.84, 0.71, 0.67, 0.62, 0.56, 0.49, 0.41)
        num_points = cvec.shape[0]
        # 1. test angle
        diis_len = linalg.norm(diis_step)
        ref_len  = linalg.norm(ref_step)
        cost = np.dot(diis_step, ref_step)/(diis_len * ref_len)
        # 3. sum of positive c_i
        pci_sum = np.sum(i for i in cvec if i > 1.e-10)
        # 4. |c|/|r|^2 <-- this is already done after solving the SEQ
        # assemble results
        if num_points < 10:
            lower_bound = min_angle[num_points-2]
        else:
            lower_bound = 0.0
        if cost < lower_bound or diis_len > 10.0*ref_len or pci_sum > 15.0:
            step_is_ok = False
        # if step is < 90° discard earlier steps
        if cost < 0.0:
            logging.debug('    WARNING: Discarding steps because of large deviating Angle in the GDIIS step!')
            self.old_points = self.old_points[-(num_points-1):]
        utils.logverbose('    cost: {:5.3f} diis_len/ref_len: {:8.2f} pci_sum: {:8.2f}'.format(cost, diis_len/ref_len, pci_sum))
        return step_is_ok
    
    def get_new_Vars(self, curr_hess = None, curr_grad = None, curr_vars = None):
        if self.curr_iter < 2:
            new_vars = curr_vars + get_RFOStep(curr_hess, curr_grad, curr_vars)
        else:
            # iterate up to max number of points or unacceptable step
            if self.max_points is None or self.max_points > len(self.old_points):
                npoints = len(self.old_points)
            else:
                npoints = self.max_points
            errvecs  = [ get_RFOStep(curr_hess, self.old_points[-npoints+i][1], self.old_points[-npoints+i][0]) for i in range(npoints) ]
            new_vars = curr_vars + errvecs[-1]
            for nstep in xrange(2, npoints+1):
                logging.debug('*** GDIIS subspace search. Iteration {:4d}'.format(nstep-1))
                # 0. get and scale the error vectors
                evmin2 = np.amin([linalg.norm(ev) for ev in errvecs[-nstep:]])**2
                tmpspace = [ev/evmin2 for ev in errvecs[-nstep:]]
                # 1. get the step
                isredundant, real_cvec = self.solve_SEQ(tmpspace)
                if isredundant:
                    logging.debug('    Skipping step because of redundancy (or bad coefficients)!')
                    break
                curr_xstar = np.sum((real_cvec[-i]*self.old_points[-i][0] for i in xrange(1, nstep+1)), axis = 0)
                curr_rvec  = np.sum((real_cvec[-i]*errvecs[-i] for i in xrange(1, nstep+1)), axis = 0)
                gdiis_step = curr_xstar + curr_rvec - curr_vars
                # 2. examine the step
                rnorm = linalg.norm(curr_rvec)
                logging.debug('    |dx_gdiis| = {:12.8e}, |r| = {:12.8e}, evmin2 = {:12.8e}'.format(linalg.norm(gdiis_step), rnorm, evmin2))
                if self.check_Step(gdiis_step, errvecs[-1], real_cvec):
                    logging.debug('    GDIIS step accepted.')
                    new_vars = curr_xstar + curr_rvec
                else:
                    logging.debug('    GDIIS step not accepted! Taking RFO step.')
                    break
        return new_vars


class GEDIIS(GDIIS):
    """ Very simple implementation of the energy represented GDIIS according to Li and Frisch.
    References:
    - X. Li, M. J. Frisch, JCTC, 2, 2006, 835--839.
    - H. B. Schlegel, WIREs Comp. Mol. Sci., 1, 2011, 795.
    """

    def get_Aij(self, i = None, j = None, space = None):
        return np.dot(space[i][1]-space[j][1], space[i][0]-space[j][0])

    def solve_SEQ(self, error_vecs = None):
        assert error_vecs is not None

        numsteps = len(error_vecs)
        Amat = np.zeros((numsteps+1, numsteps+1))
        for i in xrange(Amat.shape[0]):
            for j in xrange(i, Amat.shape[1]):
                if j == numsteps:
                    if i != numsteps:
                        Amat[i,j] = -1.0
                else:
                    Amat[i,j] = self.get_Aij(i, j, error_vecs)
                Amat[j,i] = Amat[i,j]
        res  = np.zeros((Amat.shape[0],))
        res[-1] = -1.0
        cvec, rnorm = spopt.nnls(Amat, res)
        logging.debug('    DIIS coefficients: '+' '.join(['{:12.5f}'.format(c) for c in cvec]))
        logging.debug('    Residual norm of nnls: {:12.5f}'.format(rnorm))
        return False, cvec[:-1]

    def get_new_Vars(self, curr_hess = None, curr_grad = None, curr_vars = None):
        if self.curr_iter < 2:
            new_vars = curr_vars + get_RFOStep(curr_hess, curr_grad, curr_vars)
        else:
            # iterate up to max number of points or unacceptable step
            if self.max_points is None or self.max_points > len(self.old_points):
                npoints = len(self.old_points)
            else:
                npoints = self.max_points
            rfostep = get_RFOStep(curr_hess, curr_grad, curr_vars)
            #new_vars = curr_vars + rfostep
            for nstep in xrange(2, npoints+1):
                logging.debug('*** GDIIS subspace search. Iteration {:4d}'.format(nstep-1))
                # 1. get the step
                isredundant, real_cvec = self.solve_SEQ(self.old_points[:-nstep-1:-1])
                if isredundant:
                    logging.debug('    Skipping step becase of redundancy!')
                    break
                curr_xstar = np.sum((real_cvec[i-1]*self.old_points[-i][0] for i in xrange(1, nstep+1)), axis = 0)
                curr_gstar = np.sum((real_cvec[i-1]*self.old_points[-i][1] for i in xrange(1, nstep+1)), axis = 0)
                curr_rvec  = get_RFOStep(curr_hess, curr_gstar, curr_xstar)
                gdiis_step = curr_xstar + curr_rvec - curr_vars
                # 2. examine the step
                rnorm = linalg.norm(curr_rvec)
                logging.debug('    |dx_gdiis| = {:12.8e}, |r| = {:12.8e}'.format(linalg.norm(gdiis_step), rnorm))
                # if self.check_Step(gdiis_step, rfostep, real_cvec):
                #     logging.debug('    GDIIS step accepted.')
                new_vars = curr_xstar + curr_rvec
                # else:
                #     logging.debug('    GDIIS step not accepted! Taking last step.')
                #     break
        return new_vars

class HybridOpt(object):
    """ Very simple implementation of the hybrid optimization scheme according to Li and Frisch.
    It is divided into three parts:
    1.) RFO until root mean square force of the latest point < 10^-2 a.u.
    2.) GEDIIS until root mean square RFO step is < 2.5*10^-3 a.u.
    3.) RFO-DIIS until convergence (cGDIIS of Farkas?)
    References:
    - X. Li, M. J. Frisch, JCTC, 2, 2006, 835--839.
    """
    def __init__(self, optref = None, **kwargs):
        if optref is None:
            raise ValueError('Need the reference of the parent optimization object.')
        else:
            self.OptParent  = optref
            self.old_points = []
            self.max_points = 10
            self.curr_iter  = 0
            self.smax       = 0.1
            self.GEDIIS     = None
            self.GDIIS      = None
            self.method     = 0

    def doStep(self, curr_hess = None, curr_grad = None, curr_vars = None):
        assert curr_hess is not None
        assert curr_grad is not None
        assert curr_vars is not None

        # 1. Save error vector and points
        if self.max_points is not None and len(self.old_points) >= self.max_points:
            self.old_points.pop(0)
        self.old_points.append((curr_vars, curr_grad))
        self.curr_iter += 1
        # 2. Get the new step and scale if necessary
        force_rms   = np.sqrt(np.sum(np.power(self.OptParent.optstep_hist[-1].grad_cart, 2))/self.OptParent.optstep_hist[-1].grad_cart.shape[0])
        rfostep     = get_RFOStep(curr_hess, curr_grad, curr_vars)
        rfostep_rms = np.sqrt(np.sum(np.power(rfostep, 2))/rfostep.shape[0])
        utils.logverbose('    force_rms: {:12.8f} rfostep_rms: {:12.8f}'.format(force_rms, rfostep_rms))
        if force_rms < 1.0e-2 and rfostep_rms < 2.5e-3 and self.method == 0:
            # if rfostep_rms > 2.5e-3 and self.method == 0:
            #     if self.GEDIIS is None:
            #         self.GEDIIS = GEDIIS(self.OptParent)
            #         self.GEDIIS.max_points = self.max_points
            #         self.GEDIIS.smax = self.smax
            #         self.GEDIIS.old_points = self.old_points
            #     self.method = 1
            # elif rfostep_rms < 2.5e-3 and self.method == 1:
                if self.GDIIS is None:
                    self.GDIIS = GDIIS(self.OptParent)
                    self.GDIIS.max_points = self.max_points
                    self.GDIIS.smax = self.smax
                    self.GDIIS.old_points = self.old_points
                self.method = 2
        utils.logverbose('################## Hybrid optimization step {:5d} with method {:2d} ################'.format(self.curr_iter, self.method))
        if self.method == 0:
            utils.logverbose('------------------ Doing RFO step ------------------')
            newstep = rfostep
        # elif self.method == 1:
        #     utils.logverbose('------------------ Doing GEDIIS step ------------------')
        #     utils.logverbose('--- nsteps: {:5d}'.format(len(self.GEDIIS.old_points)))
        #     self.GEDIIS.curr_iter = self.curr_iter
        #     newstep = self.GEDIIS.get_new_Vars(curr_hess, curr_grad, curr_vars) - curr_vars
        elif self.method == 2:
            utils.logverbose('------------------ Doing GDIIS step ------------------')
            utils.logverbose('--- nsteps: {:5d}'.format(len(self.GDIIS.old_points)))
            self.GDIIS.curr_iter = self.curr_iter
            newstep = self.GDIIS.get_new_Vars(curr_hess, curr_grad, curr_vars) - curr_vars
        slen, smax = linalg.norm(newstep), np.amax(np.absolute(newstep))
        logging.debug('    Step length: {:12.5f}'.format(slen))
        if smax > self.smax:
            logging.debug('    DIIS Step too large, scaling it.')
            newstep *= self.smax/smax
        return newstep
