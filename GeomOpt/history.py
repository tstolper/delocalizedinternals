# -*- coding: utf-8 -*-

#####################################
#                                   #
# Class for keeping the history     #
# of about everything, e.g. struc-  #
# ture, energy, gradient, Hessian.  #
# Also serves as a centralized      #
# point for logging.                #
#                                   #
#####################################

import sys

class OptimizationHistory(object):
    """
    Class which saves all the important stuff in lists. The corresponding index should be synched
    between the lists.
    TOTHINK: Maybe it's cleverer to have a class for every optimization point, containing exactly one structure,
    energy, gradient, Hessian, etc. But that'll probably result in a quite large number of objects
    floating about.

    Options:

    autoLog         True            automatically log the history to files?
    logFiles                        dictionary with names for the separate log files.
    """
    def __init__(self, optref = None, system = 'xyz', sizelimit = 0):
        self.OptRef         = optref
        self.listlimit      = sizelimit
        self.energies       = []
        self.xyzstructs     = []
        self.xyzgrads       = []
        self.xyzhess        = []
        self.intstructs     = []
        self.intgrads       = []
        self.inthess        = []
        if system == 'xyz':
            self.workstructs    = self.xyzstructs
            self.workgrads      = self.xyzgrads
            self.workhess       = self.xyzhess
        else:
            self.workstructs    = self.intstructs
            self.workgrads      = self.intgrads
            self.workhess       = self.inthess

    def logLast(self): pass

    def getLast(self, entity = 'ener'):
        loglists = { 'ener': self.energies[-1], 'struct_cart': self.xyzstructs[-1], 'struct_int': self.intstructs[-1], 'grad_cart': self.xyzgrads[-1],\
                        'grad_int': self.intgrads[-1], 'hess_cart': self.xyzhess[-1], 'hess_int': self.inthess[-1] }
        return loglists[entity]

    def pushNew_Hess(self, carthess = None, coordhess = None):
        #check if we need to clean old stuff. xyzhess and inthess are assumed to be the same size.
        if self.listlimit > 0 and len(self.xyzhess) >= self.listlimit:
            self.xyzhess.pop(0)
            self.inthess.pop(0)
        self.xyzhess.append(carthess)
        self.inthess.append(coordhess)

    def pushNew_Grad(self, cartgrad = None, coordgrad = None):
        if cartgrad == None:
            print >> sys.stderr, 'ERROR: There must be at least an XYZ gradient.'
            print cartgrad
            print coordgrad
            sys.exit(1)
        else:
            if self.listlimit > 0 and len(self.xyzgrads) >= self.listlimit:
                self.xyzgrads.pop(0)
                self.intgrads.pop(0)
            self.xyzgrads.append(cartgrad)
            self.intgrads.append(coordgrad)

    def pushNew_Struct(self, cartstruct = None, coordstruct = None):
        if cartstruct == None:
            print >> sys.stderr, 'ERROR: There must be at least an XYZ structure.'
            print cartstruct
            print coordstruct
            sys.exit(1)
        else:
            if self.listlimit > 0 and len(self.xyzstructs) >= self.listlimit:
                self.xyzstructs.pop(0)
                self.intstructs.pop(0)
            self.xyzstructs.append(cartstruct)
            self.intstructs.append(coordstruct)

    def pushNew(self, xyzcoords = None, xyzgrad = None, xyzhess = None, energy = None):
        if None in [xyzcoords, xyzgrad, xyzhess, energy]:
            print >> sys.stderr, 'ERROR: Please supply ALL of xyzcoords, xyzgrad, xyzhess and energy.'
            print xyzcoords
            print xyzgrad
            print xyzhess
            print energy
            sys.exit(1)
        else:
            self.energies.append(energy)
            self.xyzstructs.append(xyzcoords)
            self.xyzgrads.append(xyzgrad)
            self.xyzhess.append(xyzhess)

