
# -*- coding: utf-8 -*-

import argparse
import PyOpt.Control

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Main script to run PyOpt. Parses and runs own input files.')
    parser.add_argument('inpfile', help = 'Path to the input file.')
    args = parser.parse_args()

    PyOpt.Control.parse_Control(args.inpfile)
