
# --* coding: utf-8 *--

import logging
import scipy as np
from scipy import linalg

optimization = None


class Harmonic2Diff(object):
    """ Harmonic potential: V = 0.5*k*(r-r0)^2 where r = ||u||-||v||
        __init__ takes:
            k=1.0           force constant in atomic units (Eh/Bohr^2)
            r0=1.0          "equilibrium" distance of the coordinate
            uatoms=None     atoms vor u-vector which is calculated by u = XYZ[uatoms[0]] - XYZ[uatoms[1]]
            vatoms=None     same as uatoms but for the v-vector

        Coordinates supplied to the methods should be in Bohr. Then the Energy would be
        in Hartree and the Gradient in Eh/Bohr, respectively.
    """
    def __init__(self, k=1.0, r0=1.0, uatoms=None, vatoms=None):
        self.settings = { 'k': k, 'r0': r0, 'uatoms': uatoms, 'vatoms': vatoms }
        logging.debug('Adding potential to bond difference: ||atom{0}-atom{1}||-||atom{2}-atom{3}||'.format(uatoms[0], uatoms[1], vatoms[0], vatoms[1]))
        logging.debug('    Using the following constants: k = {:12.8e} Eh/Bohr**2, r0 = {:12.8f} Bohr'.format(k, r0))

    def _calc_Variables(self, flatcoords=None):
        assert flatcoords is not None
        ua1, ua2    = self.settings['uatoms']
        va1, va2    = self.settings['vatoms']
        uvec = flatcoords[ua1*3:ua1*3+3] - flatcoords[ua2*3:ua2*3+3]
        vvec = flatcoords[va1*3:va1*3+3] - flatcoords[va2*3:va2*3+3]
        ulen = linalg.norm(uvec)
        vlen = linalg.norm(vvec)
        return uvec, ulen, vvec, vlen

    def get_Energy(self, flatcoords=None):
        assert flatcoords is not None
        uvec, ulen, vvec, vlen  = self._calc_Variables(flatcoords=flatcoords)
        settings    = self.settings
        Vpot = settings['k']*(ulen-vlen-settings['r0'])**2/2.0
        return Vpot

    def get_Gradient(self, flatcoords=None):
        assert flatcoords is not None
        retgrad = np.zeros(flatcoords.shape)
        settings= self.settings
        ua1, ua2= settings['uatoms']
        va1, va2= settings['vatoms']
        uvec, ulen, vvec, vlen  = self._calc_Variables(flatcoords=flatcoords)
        retgrad[ua1*3:ua1*3+3] += uvec/ulen
        retgrad[ua2*3:ua2*3+3] -= uvec/ulen
        retgrad[va1*3:va1*3+3] -= vvec/vlen
        retgrad[va2*3:va2*3+3] += vvec/vlen
        retgrad *= settings['k']*(ulen-vlen-settings['r0'])
        return retgrad

    def get_All(self, flatcoords=None):
        uvec, ulen, vvec, vlen = self._calc_Variables(flatcoords=flatcoords)
        dV = self.get_Energy(flatcoords=flatcoords)
        dG = self.get_Gradient(flatcoords=flatcoords)
        dist = ulen-vlen
        return dist, dV, dG
