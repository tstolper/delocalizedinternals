# --* encoding: utf-8 *--

import logging
import scipy as np
from GeomOpt.atomprops import *
from GeomOpt.coordinates import Cartesians


class Frequency(object):

    def __init__(self, molecule = None):
        assert molecule != None
        self.mol            = molecule
        self.atom_masses    = None
        self.hessian        = None
        self.freqs          = None # frequencies in cm^-1
        self.norm_modes     = None # normal modes, mass-weighted and normalized
        self.was_diagonalized = False # are freqs and norm_modes consistent with the hessian?
        self.hess_type  = 'CART'

    def load_cart_Hess(self, carthess = None):
        assert carthess != None
        # 1. mass-weight the Hessian (and project out translation and rotation)
        tmpc            = Cartesians(startcart = self.mol.get_flat_Array(units = 'BOHR'), atomlabels = self.mol.get_Elem_List(), dotest = True)
        self.hessian    = tmpc.projectHessian(carthess = carthess, wantWeight = True)
        self.was_diagonalized = False
        self.hess_type  = 'CART'
        self.atom_masses    = tmpc.atom_masses

    def load_delo_Hess(self, delohess = None): pass

    def get_Frequencies(self):
        assert self.hessian != None
        retfreq = None
        # diagonalize Hessian
        if self.was_diagonalized:
            retfreq = self.freqs
        elif self.hess_type == 'CART':
            assert self.mol.natoms*3 == self.hessian.shape[0]
            evals, evecs    = np.linalg.eigh(self.hessian)
            retfreq         = np.sqrt(evals*EhBohr22Jm2/amu2kg)/(200.0*np.pi*const_c)
            self.freqs      = retfreq
            # mass weight and normalize eigenvectors to obtain normal modes
            weight_vec        = np.zeros((3*self.mol.natoms,))
            for m in range(weight_vec.shape[0]):
                weight_vec[m]   = np.sqrt(self.atom_masses[m//3])
            for i in range(evecs.shape[1]):
                evecs[:, i] /= weight_vec
            self.norm_modes = evecs
            # and log the frequencies
            logmain.info('************** Frequencies (in cm^-1) ************')
            for i in range(retfreq.shape[0]):
                logmain.info('{0:10d} {1:8.2f}'.format(i, retfreq[i]))
            self.was_diagonalized = True

        elif self.hess_type == 'DELO': pass

        return retfreq

    def print_Frequencies(self, outfile = None):
        freqs = self.get_Frequencies()

        if outfile == None:
            print 'Harmonic vibrational frequencies:'
            print '{0:5s} {1:12s}'.format('ID', 'Wavenumber/cm^-1')
            for f in range(freqs.shape[0]):
                print '{0:5d} {1:12.4f}'.format(f, freqs[f])

    def log_Normalmodes(self):
        assert self.was_diagonalized == True
        logdebug.info('*************** Normal Modes ***************')
        elems = self.mol.get_Elem_List()
        for i in range(self.norm_modes.shape[1]):
            logdebug.info('Mode {0:10d}'.format(i))
            m = self.norm_modes[:,i].reshape(len(elems),3)
            for x in range(m.shape[0]):
                logdebug.info('  {0:3} {1:12.8f} {2:12.8f} {3:12.8f}'.format(elems[x], m[x,0], m[x,1], m[x,2]))
            logdebug.info('------------')
        logmain.info('Wrote normal modes to debug file.')

    def write_Vibrations(self, ids = None, basename = 'vib'):
        assert self.was_diagonalized == True
        vibids  = ids
        elems   = self.mol.get_Elem_List()
        refpos  = self.mol.get_flat_Array(units = 'ANG')
        natoms  = len(elems)
        frames  = 20
        displ   = 1.0*Bohr2Ang
        if vibids == None:
            # write all vibrations
            vibids = range(self.norm_modes.shape[1])
        logmain.info('Writing {0} vibrations to files {1}_vib#####.xyz'.format(len(vibids), basename))
        for i in vibids:
            with open('{0}_vib{1:0>5d}.xyz'.format(basename, i), 'w') as vibout:
                mode = self.norm_modes[:,i]
                for f in range(frames):
                    print >> vibout, '{0:d}\n Vibration {1:d} Frame {2:d}'.format(natoms, i, f)
                    for l in range(natoms):
                        print >> vibout, '{0:5} {1:12.8f} {2:12.8f} {3:12.8f}'.format(elems[l], refpos[l*3] + mode[l*3]*displ*float(f)/float(frames),
                                                refpos[l*3+1] + mode[l*3+1]*displ*float(f)/float(frames), refpos[l*3+2] + mode[l*3+2]*displ*float(f)/float(frames))
                for f in range(frames):
                    print >> vibout, '{0:d}\n Vibration {1:d} Frame {2:d}'.format(natoms, i, f+frames)
                    for l in range(natoms):
                        print >> vibout, '{0:5} {1:12.8f} {2:12.8f} {3:12.8f}'.format(elems[l], refpos[l*3] + mode[l*3]*displ*float(frames-f)/float(frames),
                                                refpos[l*3+1] + mode[l*3+1]*displ*float(frames-f)/float(frames), refpos[l*3+2] + mode[l*3+2]*displ*float(frames-f)/float(frames))

        logmain.info('Done writing vibrations.')

logmain     = logging.getLogger('main')
logdebug    = logging.getLogger('debug')